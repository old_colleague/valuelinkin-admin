package com.valuelinkin.admin.dao.entity;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * 〈功能详细描述〉
 *
 * @author Nail.zhang
 * @version [V0.1, Jan 9, 2018]
 */

@Table(name = "vln_answer")
public class Answer implements java.io.Serializable {
    private static final long serialVersionUID = 5454155825314635342L;


    //columns START
    @Id
    @Column(name = "ans_id")
    private Long ansId;
    private Long qsnId;
    private Long usrId;
    private java.lang.String ansContent;
    private Integer ansStatus;
    private Long ansReplier;

    private java.util.Date createdOn;
    private java.util.Date modifiedOn;
    //columns END


    public Answer() {
    }

    public Answer(
            Long ansId
    ) {
        this.ansId = ansId;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Date getModifiedOn() {
        return modifiedOn;
    }

    public void setModifiedOn(Date modifiedOn) {
        this.modifiedOn = modifiedOn;
    }

    public void setAnsId(Long value) {
        this.ansId = value;
    }

    public Long getAnsId() {
        return this.ansId;
    }

    public Long getQsnId() {
        return this.qsnId;
    }

    public void setQsnId(Long value) {
        this.qsnId = value;
    }

    public Long getUsrId() {
        return this.usrId;
    }

    public void setUsrId(Long value) {
        this.usrId = value;
    }

    public java.lang.String getAnsContent() {
        return this.ansContent;
    }

    public void setAnsContent(java.lang.String value) {
        this.ansContent = value;
    }

    public Integer getAnsStatus() {
        return this.ansStatus;
    }

    public void setAnsStatus(Integer value) {
        this.ansStatus = value;
    }

    public Long getAnsReplier() {
        return this.ansReplier;
    }

    public void setAnsReplier(Long value) {
        this.ansReplier = value;
    }


}

