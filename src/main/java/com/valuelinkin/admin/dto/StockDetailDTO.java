package com.valuelinkin.admin.dto;

import java.util.List;

import com.valuelinkin.admin.dao.entity.Group;
import com.valuelinkin.admin.dao.entity.StockFinancialDataDetail;
import com.valuelinkin.admin.dao.entity.StockGroup;
import com.valuelinkin.admin.dao.entity.StockShareholding;

public class StockDetailDTO extends BaseDTO {
    private static final long serialVersionUID = 3148176768559230877L;
    

	private Long stkId;
	private java.lang.String stkCode;
	private java.lang.String stkName;
	private java.lang.String stkLogo;
	private java.lang.String stkKeys;
	private Integer stkShareholders;
	private Double stkCirculatingSharesRate;
	//财务报表标题
	private String sdfDataTitle;
	
	private List<StockFinancialDataDetail> stkFinDataDetails;
	
	private List<StockShareholding> stkShareholdings;	
	
	private java.util.Date createdOn;
	private java.util.Date modifiedOn;
	
	private List<StockGroup> groups;
	
	private List<Group> groupList;

	
	public Long getStkId() {
		return this.stkId;
	}
	
	public void setStkId(Long value) {
		this.stkId = value;
	}
	
	public java.lang.String getStkCode() {
		return this.stkCode;
	}
	
	public void setStkCode(java.lang.String value) {
		this.stkCode = value;
	}
	
	public java.lang.String getStkName() {
		return this.stkName;
	}
	
	public void setStkName(java.lang.String value) {
		this.stkName = value;
	}
	
	public java.lang.String getStkLogo() {
		return this.stkLogo;
	}
	
	public void setStkLogo(java.lang.String value) {
		this.stkLogo = value;
	}
	
	public java.lang.String getStkKeys() {
		return this.stkKeys;
	}
	
	public void setStkKeys(java.lang.String value) {
		this.stkKeys = value;
	}
	
	public Integer getStkShareholders() {
		return this.stkShareholders;
	}
	
	public void setStkShareholders(Integer value) {
		this.stkShareholders = value;
	}
	
	public Double getStkCirculatingSharesRate() {
		return this.stkCirculatingSharesRate;
	}
	
	public void setStkCirculatingSharesRate(Double value) {
		this.stkCirculatingSharesRate = value;
	}
	
	public java.util.Date getCreatedOn() {
		return this.createdOn;
	}
	
	public void setCreatedOn(java.util.Date value) {
		this.createdOn = value;
	}
	
	public java.util.Date getModifiedOn() {
		return this.modifiedOn;
	}
	
	public void setModifiedOn(java.util.Date value) {
		this.modifiedOn = value;
	}

	/**
	 * @return the sdfDataTitle
	 */
	public String getSdfDataTitle() {
		return sdfDataTitle;
	}

	/**
	 * @param sdfDataTitle the sdfDataTitle to set
	 */
	public void setSdfDataTitle(String sdfDataTitle) {
		this.sdfDataTitle = sdfDataTitle;
	}

	
	

	/**
	 * @return the stkFinDataDetails
	 */
	public List<StockFinancialDataDetail> getStkFinDataDetails() {
		return stkFinDataDetails;
	}

	/**
	 * @param stkFinDataDetails the stkFinDataDetails to set
	 */
	public void setStkFinDataDetails(List<StockFinancialDataDetail> stkFinDataDetails) {
		this.stkFinDataDetails = stkFinDataDetails;
	}

	/**
	 * @return the stkShareholdings
	 */
	public List<StockShareholding> getStkShareholdings() {
		return stkShareholdings;
	}

	/**
	 * @param stkShareholdings the stkShareholdings to set
	 */
	public void setStkShareholdings(List<StockShareholding> stkShareholdings) {
		this.stkShareholdings = stkShareholdings;
	}

	/**
	 * @return the groups
	 */
	public List<StockGroup> getGroups() {
		return groups;
	}

	/**
	 * @param groups the groups to set
	 */
	public void setGroups(List<StockGroup> groups) {
		this.groups = groups;
	}

	/**
	 * @return the groupList
	 */
	public List<Group> getGroupList() {
		return groupList;
	}

	/**
	 * @param groupList the groupList to set
	 */
	public void setGroupList(List<Group> groupList) {
		this.groupList = groupList;
	}

	
	
}

