package com.valuelinkin.admin.service.core;

import java.util.List;

import com.jcommon.orm.Page;
import com.jcommon.exception.BusinessException;
import com.jcommon.result.ResponseResult;

import com.valuelinkin.admin.dto.StockFinancialDataDetailDTO;

public interface StockFinancialDataDetailService {

	ResponseResult findPage(StockFinancialDataDetailDTO dto) throws BusinessException;
	
	ResponseResult getById(Long id) throws BusinessException;

	ResponseResult save(StockFinancialDataDetailDTO dto) throws BusinessException;
	
	ResponseResult delete(StockFinancialDataDetailDTO dto) throws BusinessException ;
	
	ResponseResult deleteById(Long id) throws BusinessException ;
	
	ResponseResult update(StockFinancialDataDetailDTO dto) throws BusinessException ;
	
	ResponseResult findByExample(StockFinancialDataDetailDTO dto) throws BusinessException;
	
	ResponseResult getByExample(StockFinancialDataDetailDTO dto) throws BusinessException;
	
	ResponseResult deleteByIds(String ids) throws BusinessException ;
	

}
