package com.valuelinkin.admin.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jcommon.result.ResponseResult;
import com.valuelinkin.admin.dto.NewsColumnDTO;
import com.valuelinkin.admin.dto.NewsLiveDTO;
import com.valuelinkin.admin.service.core.NewsColumnService;
import com.valuelinkin.admin.service.core.NewsLiveService;

@Controller
@RequestMapping(value="/newsLive") 
public class NewsLiveController{
	
	@Autowired
	private NewsLiveService newsLiveService;
	
	@Autowired
	private NewsColumnService newsColumnService;
	
	@RequestMapping(value="/index",method=RequestMethod.GET)
	public String index( Model model) throws Exception {
		
		return "/newsLive/list";
	}
	
	/** 
	 * 执行搜索 
	 **/
	@RequestMapping(value="",method=RequestMethod.GET)
	@ResponseBody
	public ResponseResult findPage(NewsLiveDTO dto) {
		
		ResponseResult result = new ResponseResult();
		result = this.newsLiveService.findPage(dto);
		return result;
	}
	
	
	/** 
	 * 查看对象
	 **/
	@RequestMapping(value="/view/{id}",method=RequestMethod.GET)
	public String getById(@PathVariable Long id, Model model) {
		ResponseResult result = new ResponseResult();
		result = newsLiveService.getById(id);
		
		model.addAttribute("result", result);
		return "/newsLive/view";
		
	}
	

	/** 
	 * 进入新增页面
	 **/
	@RequestMapping(value="/create", method=RequestMethod.GET)
	public String create(NewsLiveDTO dto, Model model) throws Exception {
		model.addAttribute("newsLive", dto);
		ResponseResult result = newsColumnService.findChildColumnByCode("ZB");
		model.addAttribute("columnList", result.getData());
		return "/newsLive/create";
	}
	
	/** 
	 * 保存新增对象
	 **/
	@RequestMapping(value="",method=RequestMethod.POST)
	@ResponseBody
	public ResponseResult save(NewsLiveDTO dto) {
		ResponseResult result = null;
		result = newsLiveService.save(dto);
		return result;
	}
	
	
	/**
	 * 进入更新页面
	 **/
	@RequestMapping(value="/edit/{id}", method=RequestMethod.GET)
	public String edit(@PathVariable Long id, Model model) throws Exception {

		ResponseResult result = newsColumnService.findChildColumnByCode("ZB");
		model.addAttribute("columnList", result.getData());
		result  = newsLiveService.getById(id);
		model.addAttribute("result", result);
		
		return "/newsLive/edit";
	}
	
	/**
	 * 保存更新对象
	 **/
	@RequestMapping(value="/{id}", method=RequestMethod.PUT)
	@ResponseBody
	public ResponseResult update(@PathVariable Long id,NewsLiveDTO dto) throws Exception {
		ResponseResult result = new ResponseResult();
		dto.setNwsId(id);
		result = newsLiveService.update(dto);
		return result;
	}
	
	
	/**
	 *删除多个对象
	 **/
	@RequestMapping(value="/{ids}", method=RequestMethod.DELETE)
	@ResponseBody
	public ResponseResult delete(@PathVariable String ids) {
		
		ResponseResult result = null;
		result = newsLiveService.deleteByIds(ids);
		return result;
		
	}
	
	public void setNewsLiveService(NewsLiveService service) {
		this.newsLiveService = service;
	}
	
}
