package com.valuelinkin.admin.service.core;

import java.util.List;

import com.jcommon.orm.Page;
import com.jcommon.exception.BusinessException;
import com.jcommon.result.ResponseResult;

import com.valuelinkin.admin.dto.SuggestionDTO;

public interface SuggestionService {

	ResponseResult findPage(SuggestionDTO dto) throws BusinessException;
	
	ResponseResult getById(Long id) throws BusinessException;

	ResponseResult save(SuggestionDTO dto) throws BusinessException;
	
	ResponseResult delete(SuggestionDTO dto) throws BusinessException ;
	
	ResponseResult deleteById(Long id) throws BusinessException ;
	
	ResponseResult update(SuggestionDTO dto) throws BusinessException ;
	
	ResponseResult findByExample(SuggestionDTO dto) throws BusinessException;
	
	ResponseResult getByExample(SuggestionDTO dto) throws BusinessException;
	
	ResponseResult deleteByIds(String ids) throws BusinessException ;
	

}
