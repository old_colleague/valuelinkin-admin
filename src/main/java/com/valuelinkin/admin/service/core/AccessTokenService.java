package com.valuelinkin.admin.service.core;

import java.util.List;

import com.jcommon.orm.Page;
import com.jcommon.exception.BusinessException;
import com.jcommon.result.ResponseResult;

import com.valuelinkin.admin.dto.AccessTokenDTO;

public interface AccessTokenService {

	ResponseResult findPage(AccessTokenDTO dto) throws BusinessException;
	
	ResponseResult getById(Long id) throws BusinessException;

	ResponseResult save(AccessTokenDTO dto) throws BusinessException;
	
	ResponseResult delete(AccessTokenDTO dto) throws BusinessException ;
	
	ResponseResult deleteById(Long id) throws BusinessException ;
	
	ResponseResult update(AccessTokenDTO dto) throws BusinessException ;
	
	ResponseResult findByExample(AccessTokenDTO dto) throws BusinessException;
	
	ResponseResult getByExample(AccessTokenDTO dto) throws BusinessException;
	
	ResponseResult deleteByIds(String ids) throws BusinessException ;
	

}
