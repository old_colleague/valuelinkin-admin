package com.valuelinkin.admin.dao.mapper;


import org.apache.ibatis.annotations.Mapper;

import com.valuelinkin.admin.dao.entity.AccessToken;
import com.valuelinkin.admin.dao.BaseMapper;

@Mapper
public interface AccessTokenMapper extends BaseMapper<AccessToken>{
	

}
