package com.valuelinkin.admin.dto;

import com.valuelinkin.admin.dao.entity.Answer;

import java.util.Date;

/**
 * @atuhor: nail.zhang
 * @description:
 * @date: 2019/2/17 17:22
 */
public class QuestionResultDTO {

    private Long qsnId;
    private Long nwsId;
    private Long usrId;
    private java.lang.String qsnContent;
    private Integer qsnStatus;
    private java.util.Date createdOn;
    private java.util.Date modifiedOn;

    private String usrNickName;
    private String usrPhone;
    private String nwsTitle;
    private String nwsStockCode;

    private Answer answer;

    public Answer getAnswer() {
        return answer;
    }

    public void setAnswer(Answer answer) {
        this.answer = answer;
    }

    public Long getQsnId() {
        return qsnId;
    }

    public void setQsnId(Long qsnId) {
        this.qsnId = qsnId;
    }

    public Long getNwsId() {
        return nwsId;
    }

    public void setNwsId(Long nwsId) {
        this.nwsId = nwsId;
    }

    public Long getUsrId() {
        return usrId;
    }

    public void setUsrId(Long usrId) {
        this.usrId = usrId;
    }

    public String getQsnContent() {
        return qsnContent;
    }

    public void setQsnContent(String qsnContent) {
        this.qsnContent = qsnContent;
    }

    public Integer getQsnStatus() {
        return qsnStatus;
    }

    public void setQsnStatus(Integer qsnStatus) {
        this.qsnStatus = qsnStatus;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Date getModifiedOn() {
        return modifiedOn;
    }

    public void setModifiedOn(Date modifiedOn) {
        this.modifiedOn = modifiedOn;
    }

    public String getUsrNickName() {
        return usrNickName;
    }

    public void setUsrNickName(String usrNickName) {
        this.usrNickName = usrNickName;
    }

    public String getUsrPhone() {
        return usrPhone;
    }

    public void setUsrPhone(String usrPhone) {
        this.usrPhone = usrPhone;
    }

    public String getNwsTitle() {
        return nwsTitle;
    }

    public void setNwsTitle(String nwsTitle) {
        this.nwsTitle = nwsTitle;
    }

    public String getNwsStockCode() {
        return nwsStockCode;
    }

    public void setNwsStockCode(String nwsStockCode) {
        this.nwsStockCode = nwsStockCode;
    }
}
