package com.valuelinkin.admin.service.core.impl;

import java.util.Date;
import java.util.List;

import com.jcommon.utils.StringUtils;
import com.valuelinkin.admin.dao.entity.RelativeStock;
import com.valuelinkin.admin.dao.mapper.RelativeStockMapper;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jcommon.exception.BusinessException;
import com.jcommon.orm.Page;
import com.jcommon.result.ResponseResult;
import com.jcommon.utils.BeanUtils;
import com.jcommon.utils.RamdonString;
import com.valuelinkin.admin.common.utils.AppConstants;
import com.valuelinkin.admin.common.utils.StockUtils;
import com.valuelinkin.admin.dao.entity.NewsFlush;
import com.valuelinkin.admin.dao.mapper.NewsFlushMapper;
import com.valuelinkin.admin.dto.NewsFlushDTO;
import com.valuelinkin.admin.dto.StockInfoDTO;
import com.valuelinkin.admin.service.UidGenerator;
import com.valuelinkin.admin.service.core.NewsFlushService;

import tk.mybatis.mapper.entity.Example;

@Service
public class NewsFlushServiceImpl implements NewsFlushService{

	@Autowired
	private NewsFlushMapper newsFlushMapper;
	
	@Autowired
	private UidGenerator uidGenerator;

	@Autowired
	private RelativeStockMapper  relativeStockMapper;

	public ResponseResult findPage(NewsFlushDTO dto) {
		ResponseResult result = new ResponseResult();
		
		NewsFlush entity = new NewsFlush();
		BeanUtils.copyProperties(entity, dto);
		String keywords = StringUtils.trimToNull(dto.getKeywords());
		dto.setKeywords(keywords);

		Example example = new Example(NewsFlush.class);

		if(StringUtils.isNotBlank(keywords)) {
			keywords = "%"+keywords+"%";

			dto.setKeywords(keywords);

			example.createCriteria().orLike("nwsTitle",keywords)
					.orLike("nwsKeywords",keywords)
					.orLike("nwsBrief",keywords).orLike("nwsContent",keywords);
		}
		
		Page page = new Page();
		page.setPageNo(dto.getPageNo());
		page.setPageSize(dto.getPageSize());
		page.setTotalCount(newsFlushMapper.selectCountByExample(example));
		

		//example.createCriteria().andEqualTo("refId", rowNews.getRefId());
		example.setOrderByClause("nws_publish_date desc");
		
		//List<NewsFlush> list = newsFlushMapper.selectByExampleAndRowBounds(example, new RowBounds(page.getFirst(), page.getPageSize()));
		List<NewsFlush> list = newsFlushMapper.findPage(dto);

		page.setResult(list);
		result.setData(page);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
		
	}	
	
	public ResponseResult getById(Long id) throws BusinessException{
		ResponseResult result = new ResponseResult();
		NewsFlush entity = newsFlushMapper.selectByPrimaryKey(id);
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}

	
	/** 插入数据 */
	@Transactional
	public ResponseResult save(NewsFlushDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		NewsFlush entity = new NewsFlush();
		BeanUtils.copyProperties(entity, dto);
		entity.setNwsId(uidGenerator.nextId());
		
		StockInfoDTO stockInfoDTO = StockUtils.getStockInfo(dto.getNwsTitle());
		
		String title = "【"+stockInfoDTO.getStkCode() + " " +stockInfoDTO.getStkName()+"】";
		
		entity.setNwsTitle(title);
		Date now = new Date();
		entity.setNwsContent(dto.getNwsBrief());
		entity.setNwsStatus(1);
		entity.setNwsViewCount(RamdonString.getRamdonInt(100, 2000));
		entity.setNwsPublishDate(now);
		entity.setCreatedOn(now);
		entity.setModifiedOn(now);
		newsFlushMapper.insertSelective(entity);

		RelativeStock relativeStock = new RelativeStock();
		relativeStock.setLnkId(uidGenerator.nextId());
		relativeStock.setStkCode(stockInfoDTO.getStkCode());
		relativeStock.setRelateId(entity.getNwsId());
		relativeStockMapper.insertSelective(relativeStock);
		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult deleteById(Long id) throws BusinessException{
		ResponseResult result = new ResponseResult();
		newsFlushMapper.deleteByPrimaryKey(id);
		
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult delete(NewsFlushDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		
		NewsFlush entity = new NewsFlush();
		BeanUtils.copyProperties(entity, dto);
		
		newsFlushMapper.delete(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult deleteByIds(String ids) throws BusinessException{
		ResponseResult result = new ResponseResult();
		String[] items = ids.split(",");
		for (String str : items) {
			Long id = new Long(str);
			newsFlushMapper.deleteByPrimaryKey(id);
		}
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult update(NewsFlushDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		NewsFlush entity = new NewsFlush();
		BeanUtils.copyProperties(entity, dto);
		newsFlushMapper.updateByPrimaryKeySelective(entity);
		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	public ResponseResult findByExample(NewsFlushDTO dto) throws BusinessException {
		ResponseResult result = new ResponseResult();
		
		NewsFlush entity = new NewsFlush();
		BeanUtils.copyProperties(entity, dto);
		
		List list =newsFlushMapper.select(entity);
		
		result.setData(list);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	
	public ResponseResult getByExample(NewsFlushDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		
		NewsFlush entity = new NewsFlush();
		BeanUtils.copyProperties(entity, dto);
		
		entity =newsFlushMapper.selectOne(entity);
		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
		
	}
	
	public void setNewsFlushMapper(NewsFlushMapper dao) {
		this.newsFlushMapper = dao;
	}
}
