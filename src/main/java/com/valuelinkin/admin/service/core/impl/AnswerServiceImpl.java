package com.valuelinkin.admin.service.core.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jcommon.orm.Page;
import com.jcommon.exception.BusinessException;
import com.jcommon.utils.BeanUtils;
import com.jcommon.result.ResponseResult;

import com.valuelinkin.admin.dao.entity.Answer;
import com.valuelinkin.admin.dto.AnswerDTO;
import com.valuelinkin.admin.dao.mapper.AnswerMapper;
import com.valuelinkin.admin.service.core.AnswerService;
import com.valuelinkin.admin.service.UidGenerator;
import com.valuelinkin.admin.common.utils.AppConstants;

import tk.mybatis.mapper.entity.Example;

@Service
public class AnswerServiceImpl implements AnswerService{

	@Autowired
	private AnswerMapper answerMapper;
	
	@Autowired
	private UidGenerator uidGenerator;

	public ResponseResult findPage(AnswerDTO dto) {
		ResponseResult result = new ResponseResult();
		
		Answer entity = new Answer();
		BeanUtils.copyProperties(entity, dto);
		
		Page page = new Page();
		page.setPageNo(dto.getPageNo());
		page.setPageSize(dto.getPageSize());
		page.setTotalCount(answerMapper.selectCount(entity));
		
		Example example = new Example(Answer.class);
		//example.createCriteria().andEqualTo("refId", rowNews.getRefId());
		//example.setOrderByClause("nws_Publish_Date desc");
		
		List<Answer> list = answerMapper.selectByExampleAndRowBounds(example, new RowBounds(page.getFirst(), page.getPageSize()));
		
		page.setResult(list);
		result.setData(page);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
		
	}	
	
	public ResponseResult getById(Long id) throws BusinessException{
		ResponseResult result = new ResponseResult();
		Answer entity = answerMapper.selectByPrimaryKey(id);
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}

	
	/** 插入数据 */
	@Transactional
	public ResponseResult save(AnswerDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		Answer entity = new Answer();
		BeanUtils.copyProperties(entity, dto);
		entity.setAnsId(uidGenerator.nextId());
		
		answerMapper.insertSelective(entity);
		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult deleteById(Long id) throws BusinessException{
		ResponseResult result = new ResponseResult();
		answerMapper.deleteByPrimaryKey(id);
		
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult delete(AnswerDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		
		Answer entity = new Answer();
		BeanUtils.copyProperties(entity, dto);
		
		answerMapper.delete(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult deleteByIds(String ids) throws BusinessException{
		ResponseResult result = new ResponseResult();
		String[] items = ids.split(",");
		for (String str : items) {
			Long id = new Long(str);
			answerMapper.deleteByPrimaryKey(id);
		}
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult update(AnswerDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		Answer entity = new Answer();
		BeanUtils.copyProperties(entity, dto);
		answerMapper.updateByPrimaryKeySelective(entity);
		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	public ResponseResult findByExample(AnswerDTO dto) throws BusinessException {
		ResponseResult result = new ResponseResult();
		
		Answer entity = new Answer();
		BeanUtils.copyProperties(entity, dto);
		
		List list =answerMapper.select(entity);
		
		result.setData(list);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	
	public ResponseResult getByExample(AnswerDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		
		Answer entity = new Answer();
		BeanUtils.copyProperties(entity, dto);
		
		entity =answerMapper.selectOne(entity);
		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
		
	}
	
	public void setAnswerMapper(AnswerMapper dao) {
		this.answerMapper = dao;
	}
}
