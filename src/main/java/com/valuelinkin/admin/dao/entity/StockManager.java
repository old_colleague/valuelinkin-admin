package com.valuelinkin.admin.dao.entity;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 〈功能详细描述〉
 *
 * @author Nail.zhang
 * @version [V0.1, Jan 9, 2018]
 */

@Table(name="vln_stock_manager")
public class StockManager implements java.io.Serializable{
	private static final long serialVersionUID = 5454155825314635342L;
	

	//columns START
		@Id
		@Column(name = "id")
		private Long id;
		private java.lang.String stkCode;
		private java.lang.String mgrRealName;
		private java.lang.String mgrPosition;
		private java.lang.String mgrHeadPic;
		private java.lang.String mgrResume;
		private Integer mgrSortIndex;
		private java.util.Date createdOn;
		private java.util.Date modifiedOn;
	//columns END


	public StockManager(){
	}

	public StockManager(
		Long id
	){
		this.id = id;
	}

	public Integer getMgrSortIndex() {
		return mgrSortIndex;
	}

	public void setMgrSortIndex(Integer mgrSortIndex) {
		this.mgrSortIndex = mgrSortIndex;
	}

	public void setId(Long value) {
		this.id = value;
	}
	
	public Long getId() {
		return this.id;
	}
	
	public java.lang.String getStkCode() {
		return this.stkCode;
	}
	
	public void setStkCode(java.lang.String value) {
		this.stkCode = value;
	}
	
	public java.lang.String getMgrRealName() {
		return this.mgrRealName;
	}
	
	public void setMgrRealName(java.lang.String value) {
		this.mgrRealName = value;
	}
	
	public java.lang.String getMgrPosition() {
		return this.mgrPosition;
	}
	
	public void setMgrPosition(java.lang.String value) {
		this.mgrPosition = value;
	}
	
	public java.lang.String getMgrHeadPic() {
		return this.mgrHeadPic;
	}
	
	public void setMgrHeadPic(java.lang.String value) {
		this.mgrHeadPic = value;
	}
	
	public java.lang.String getMgrResume() {
		return this.mgrResume;
	}
	
	public void setMgrResume(java.lang.String value) {
		this.mgrResume = value;
	}
	
		
	public java.util.Date getCreatedOn() {
		return this.createdOn;
	}
	
	public void setCreatedOn(java.util.Date value) {
		this.createdOn = value;
	}
	
		
	public java.util.Date getModifiedOn() {
		return this.modifiedOn;
	}
	
	public void setModifiedOn(java.util.Date value) {
		this.modifiedOn = value;
	}
	

}

