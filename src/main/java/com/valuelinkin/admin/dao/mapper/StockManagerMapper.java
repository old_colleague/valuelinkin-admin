package com.valuelinkin.admin.dao.mapper;


import org.apache.ibatis.annotations.Mapper;

import com.valuelinkin.admin.dao.entity.StockManager;
import com.valuelinkin.admin.dao.BaseMapper;

@Mapper
public interface StockManagerMapper extends BaseMapper<StockManager>{
	

}
