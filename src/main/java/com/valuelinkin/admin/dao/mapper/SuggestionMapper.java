package com.valuelinkin.admin.dao.mapper;


import com.valuelinkin.admin.dto.SuggestionDTO;
import com.valuelinkin.admin.dto.SuggestionResultDTO;
import org.apache.ibatis.annotations.Mapper;

import com.valuelinkin.admin.dao.entity.Suggestion;
import com.valuelinkin.admin.dao.BaseMapper;

import java.util.List;

@Mapper
public interface SuggestionMapper extends BaseMapper<Suggestion>{

    /**
     * 分页查询用户反馈数据
     * @param dto
     * @return
     */
    List<SuggestionResultDTO> findPage(SuggestionDTO dto);
	

}
