package com.valuelinkin.admin.service.core;

import java.util.List;

import com.jcommon.orm.Page;
import com.jcommon.exception.BusinessException;
import com.jcommon.result.ResponseResult;

import com.valuelinkin.admin.dto.SysRoleDTO;

public interface SysRoleService {

	ResponseResult findPage(SysRoleDTO dto) throws BusinessException;
	
	ResponseResult getById(Long id) throws BusinessException;

	ResponseResult save(SysRoleDTO dto) throws BusinessException;
	
	ResponseResult delete(SysRoleDTO dto) throws BusinessException ;
	
	ResponseResult deleteById(Long id) throws BusinessException ;
	
	ResponseResult update(SysRoleDTO dto) throws BusinessException ;
	
	ResponseResult findByExample(SysRoleDTO dto) throws BusinessException;
	
	ResponseResult getByExample(SysRoleDTO dto) throws BusinessException;
	
	ResponseResult deleteByIds(String ids) throws BusinessException ;
	

}
