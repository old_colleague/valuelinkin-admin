package com.valuelinkin.admin.dto;

import com.valuelinkin.admin.dto.BaseDTO;

public class NewsColumnDTO extends BaseDTO {
    private static final long serialVersionUID = 3148176768559230877L;
    

	private Long clmId;
	private Long parentId;
	private java.lang.String clmCode;
	private java.lang.String clmDisplayName;
	private java.lang.String clmGroup;
	private Integer clmStatus;
	private java.lang.String clmRemark;
	private java.util.Date createdOn;
	private java.util.Date modifiedOn;

	
	public Long getClmId() {
		return this.clmId;
	}
	
	public void setClmId(Long value) {
		this.clmId = value;
	}
	
	public Long getParentId() {
		return this.parentId;
	}
	
	public void setParentId(Long value) {
		this.parentId = value;
	}
	
	public java.lang.String getClmCode() {
		return this.clmCode;
	}
	
	public void setClmCode(java.lang.String value) {
		this.clmCode = value;
	}
	
	public java.lang.String getClmDisplayName() {
		return this.clmDisplayName;
	}
	
	public void setClmDisplayName(java.lang.String value) {
		this.clmDisplayName = value;
	}
	
	public java.lang.String getClmGroup() {
		return this.clmGroup;
	}
	
	public void setClmGroup(java.lang.String value) {
		this.clmGroup = value;
	}
	
	public Integer getClmStatus() {
		return this.clmStatus;
	}
	
	public void setClmStatus(Integer value) {
		this.clmStatus = value;
	}
	
	public java.lang.String getClmRemark() {
		return this.clmRemark;
	}
	
	public void setClmRemark(java.lang.String value) {
		this.clmRemark = value;
	}
	
	public java.util.Date getCreatedOn() {
		return this.createdOn;
	}
	
	public void setCreatedOn(java.util.Date value) {
		this.createdOn = value;
	}
	
	public java.util.Date getModifiedOn() {
		return this.modifiedOn;
	}
	
	public void setModifiedOn(java.util.Date value) {
		this.modifiedOn = value;
	}
	
}

