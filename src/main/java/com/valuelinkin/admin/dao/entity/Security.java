package com.valuelinkin.admin.dao.entity;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * 〈功能详细描述〉
 *
 * @author Nail.zhang
 * @version [V0.1, Jan 9, 2018]
 */

@Table(name = "vln_security")
public class Security implements java.io.Serializable {
    private static final long serialVersionUID = 5454155825314635342L;


    //columns START
    @Id
    @Column(name = "stk_id")
    @GeneratedValue(generator = "JDBC")
    private Long stkId;
    private java.lang.String stkCode;
    private java.lang.String stkName;
    private java.lang.String stkKeys;
    private java.lang.String stkStatus;
    private java.util.Date createdOn;
    private java.util.Date modifiedOn;
    //columns END


    public Security() {
    }

    public Security(
            Long stkId
    ) {
        this.stkId = stkId;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Date getModifiedOn() {
        return modifiedOn;
    }

    public void setModifiedOn(Date modifiedOn) {
        this.modifiedOn = modifiedOn;
    }

    public void setStkId(Long value) {
        this.stkId = value;
    }

    public Long getStkId() {
        return this.stkId;
    }

    public java.lang.String getStkCode() {
        return this.stkCode;
    }

    public void setStkCode(java.lang.String value) {
        this.stkCode = value;
    }

    public java.lang.String getStkName() {
        return this.stkName;
    }

    public void setStkName(java.lang.String value) {
        this.stkName = value;
    }

    public java.lang.String getStkKeys() {
        return this.stkKeys;
    }

    public void setStkKeys(java.lang.String value) {
        this.stkKeys = value;
    }

    public java.lang.String getStkStatus() {
        return this.stkStatus;
    }

    public void setStkStatus(java.lang.String value) {
        this.stkStatus = value;
    }


}

