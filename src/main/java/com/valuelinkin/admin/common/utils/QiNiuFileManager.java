package com.valuelinkin.admin.common.utils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.jcommon.utils.Cipher;
import com.qiniu.util.StringMap;
import com.qiniu.util.UrlSafeBase64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.qiniu.common.QiniuException;
import com.qiniu.common.Zone;
import com.qiniu.http.Response;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.UploadManager;
import com.qiniu.storage.model.DefaultPutRet;
import com.qiniu.util.Auth;

public class QiNiuFileManager {
    private static Logger logger = LoggerFactory.getLogger(QiNiuFileManager.class);

    public static String fileUplaod(byte[] uploadBytes, String fileName, String bucket) {
        //构造一个带指定Zone对象的配置类
        Configuration cfg = new Configuration(Zone.zone2());
        //...其他参数参考类注释
        UploadManager uploadManager = new UploadManager(cfg);
        //...生成上传凭证，然后准备上传
        String accessKey = "oDdjyvfpbBxbB2kN3_scR-IPMj4GiiTPyBFpDqKc";
        String secretKey = "-Wl8WG_FVZAHN8tqZu7HtZtnn9GuzI7P29MnQTcI";

        //默认不指定key的情况下，以文件内容的hash值作为文件名
        String key = fileName;
        try {
            Auth auth = Auth.create(accessKey, secretKey);
            String upToken = auth.uploadToken(bucket);
            try {
                Response response = uploadManager.put(uploadBytes, key, upToken);
                //解析上传成功的结果
                DefaultPutRet putRet = new Gson().fromJson(response.bodyString(), DefaultPutRet.class);
                //logger.info(putRet.key);
                //logger.info(putRet.hash);
                if (key == null) {
                    return putRet.hash;
                }
                return putRet.key;
            } catch (QiniuException ex) {
                Response r = ex.response;
                logger.error(r.toString());
                try {
                    logger.error(r.bodyString());
                } catch (QiniuException ex2) {
                }
            }
        } catch (Exception ex) {
            logger.error(ex.toString());
        }
        return null;
    }

    public static String videoUpdateTransform(byte[] uploadBytes, String fileName, String bucket) {

        //设置转码操作参数
        String fops = "avthumb/mp4/s/640x360/vb/1.25m";
        //设置转码的队列
        String pipeline = "2109-v";

        //可以对转码后的文件进行使用saveas参数自定义命名，当然也可以不指定文件会默认命名并保存在当前空间。
        String saveFileName = UUID.randomUUID().toString();
        String urlbase64 = UrlSafeBase64.encodeToString(bucket+":"+saveFileName);
        String pfops = fops +"|saveas/"+ urlbase64;
        String key = null;
        String accessKey = "oDdjyvfpbBxbB2kN3_scR-IPMj4GiiTPyBFpDqKc";
        String secretKey = "-Wl8WG_FVZAHN8tqZu7HtZtnn9GuzI7P29MnQTcI";

        //构造一个带指定Zone对象的配置类
        Configuration cfg = new Configuration(Zone.zone2());
        //...其他参数参考类注释
        UploadManager uploadManager = new UploadManager(cfg);

        //密钥配置
        Auth auth = Auth.create(accessKey, secretKey);

        //上传策略中设置persistentOps字段和persistentPipeline字段
        String upToken = auth.uploadToken(bucket, null, 3600, new StringMap()
                .putNotEmpty("persistentOps", pfops)
                .putNotEmpty("persistentPipeline", pipeline), true);

        try {
            //调用put方法上传
            Response res = uploadManager.put(uploadBytes, key, upToken);
            //解析上传成功的结果
            DefaultPutRet putRet = new Gson().fromJson(res.bodyString(), DefaultPutRet.class);
//            if (key == null) {
//                return putRet.hash;
//            }
//            return putRet.key;
            return saveFileName;
        } catch (QiniuException ex) {
            Response r = ex.response;
            logger.error(r.toString());
            try {
                logger.error(r.bodyString());
            } catch (QiniuException ex2) {
            }
        }
        return null;
    }

    public static void main(String[] args) {

        File file = new File("C:\\Users\\admin\\Desktop\\mp.mp4");

        byte[] buffer = null;
        try {
            FileInputStream fis = new FileInputStream(file);
            ByteArrayOutputStream bos = new ByteArrayOutputStream(1000);
            byte[] b = new byte[1000];
            int n;
            while ((n = fis.read(b)) != -1) {
                bos.write(b, 0, n);
            }
            fis.close();
            bos.close();
            buffer = bos.toByteArray();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        String ss = videoUpdateTransform(buffer,null,"video");
        System.out.println(ss);

//        File file = new File("D:/project/qq-img");
//        File[] files = file.listFiles();
//        List<String> fileNames = new ArrayList();
//        for (File file2 : files) {
//            byte[] buffer = null;
//            try {
//                FileInputStream fis = new FileInputStream(file);
//                ByteArrayOutputStream bos = new ByteArrayOutputStream(1000);
//                byte[] b = new byte[1000];
//                int n;
//                while ((n = fis.read(b)) != -1) {
//                    bos.write(b, 0, n);
//                }
//                fis.close();
//                bos.close();
//                buffer = bos.toByteArray();
//            } catch (FileNotFoundException e) {
//                e.printStackTrace();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//
//            String ss = QiNiuFileManager.fileUplaod(buffer, "html/js/" + file.getName(), "image");
//        }


        //System.out.println(ss);
    }

}
