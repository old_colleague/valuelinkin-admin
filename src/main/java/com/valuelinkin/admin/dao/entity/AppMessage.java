package com.valuelinkin.admin.dao.entity;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * 〈功能详细描述〉
 *
 * @author Nail.zhang
 * @version [V0.1, Jan 9, 2018]
 */

@Table(name="vln_app_message")
public class AppMessage implements java.io.Serializable{
	private static final long serialVersionUID = 5454155825314635342L;
	

	//columns START
		@Id
		@Column(name = "msg_id")
		private Long msgId;
		private Long usrId;
		private java.lang.String msgType;
		private java.lang.String msgTitle;
		private java.lang.String msgContent;
		private Integer msgIsRead;
		private java.lang.String msgActionLink;
		private java.lang.String msgActionType;
		private Date msgDate;
	//columns END


	public AppMessage(){
	}

	public AppMessage(
		Long msgId
	){
		this.msgId = msgId;
	}

	public Date getMsgDate() {
		return msgDate;
	}

	public void setMsgDate(Date msgDate) {
		this.msgDate = msgDate;
	}

	public void setMsgId(Long value) {
		this.msgId = value;
	}
	
	public Long getMsgId() {
		return this.msgId;
	}
	
	public Long getUsrId() {
		return this.usrId;
	}
	
	public void setUsrId(Long value) {
		this.usrId = value;
	}
	
	public java.lang.String getMsgType() {
		return this.msgType;
	}
	
	public void setMsgType(java.lang.String value) {
		this.msgType = value;
	}
	
	public java.lang.String getMsgTitle() {
		return this.msgTitle;
	}
	
	public void setMsgTitle(java.lang.String value) {
		this.msgTitle = value;
	}
	
	public java.lang.String getMsgContent() {
		return this.msgContent;
	}
	
	public void setMsgContent(java.lang.String value) {
		this.msgContent = value;
	}
	
	public Integer getMsgIsRead() {
		return this.msgIsRead;
	}
	
	public void setMsgIsRead(Integer value) {
		this.msgIsRead = value;
	}
	
	public java.lang.String getMsgActionLink() {
		return this.msgActionLink;
	}
	
	public void setMsgActionLink(java.lang.String value) {
		this.msgActionLink = value;
	}
	
	public java.lang.String getMsgActionType() {
		return this.msgActionType;
	}
	
	public void setMsgActionType(java.lang.String value) {
		this.msgActionType = value;
	}
	

}

