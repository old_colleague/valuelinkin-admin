package com.valuelinkin.admin.dto;

import com.valuelinkin.admin.dto.BaseDTO;

public class SysPrivilegeDTO extends BaseDTO {
    private static final long serialVersionUID = 3148176768559230877L;
    

	private Long pvlId;
	private Long parentId;
	private java.lang.String privilegeName;
	private java.lang.String privilegeCode;
	private java.lang.String privilegeDesc;
	private Integer privilegeType;
	private Integer pvlStatus;
	private java.lang.String createdBy;
	private java.lang.String lastUpdatedBy;
	private java.util.Date createdOn;
	private java.util.Date modifiedOn;

	
	public Long getPvlId() {
		return this.pvlId;
	}
	
	public void setPvlId(Long value) {
		this.pvlId = value;
	}
	
	public Long getParentId() {
		return this.parentId;
	}
	
	public void setParentId(Long value) {
		this.parentId = value;
	}
	
	public java.lang.String getPrivilegeName() {
		return this.privilegeName;
	}
	
	public void setPrivilegeName(java.lang.String value) {
		this.privilegeName = value;
	}
	
	public java.lang.String getPrivilegeCode() {
		return this.privilegeCode;
	}
	
	public void setPrivilegeCode(java.lang.String value) {
		this.privilegeCode = value;
	}
	
	public java.lang.String getPrivilegeDesc() {
		return this.privilegeDesc;
	}
	
	public void setPrivilegeDesc(java.lang.String value) {
		this.privilegeDesc = value;
	}
	
	public Integer getPrivilegeType() {
		return this.privilegeType;
	}
	
	public void setPrivilegeType(Integer value) {
		this.privilegeType = value;
	}
	
	public Integer getPvlStatus() {
		return this.pvlStatus;
	}
	
	public void setPvlStatus(Integer value) {
		this.pvlStatus = value;
	}
	
	public java.lang.String getCreatedBy() {
		return this.createdBy;
	}
	
	public void setCreatedBy(java.lang.String value) {
		this.createdBy = value;
	}
	
	public java.lang.String getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}
	
	public void setLastUpdatedBy(java.lang.String value) {
		this.lastUpdatedBy = value;
	}
	
	public java.util.Date getCreatedOn() {
		return this.createdOn;
	}
	
	public void setCreatedOn(java.util.Date value) {
		this.createdOn = value;
	}
	
	public java.util.Date getModifiedOn() {
		return this.modifiedOn;
	}
	
	public void setModifiedOn(java.util.Date value) {
		this.modifiedOn = value;
	}
	
}

