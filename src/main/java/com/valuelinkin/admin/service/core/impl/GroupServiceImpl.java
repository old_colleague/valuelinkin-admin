package com.valuelinkin.admin.service.core.impl;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jcommon.exception.BusinessException;
import com.jcommon.orm.Page;
import com.jcommon.result.ResponseResult;
import com.jcommon.utils.BeanUtils;
import com.valuelinkin.admin.common.utils.AppConstants;
import com.valuelinkin.admin.dao.entity.Group;
import com.valuelinkin.admin.dao.mapper.GroupMapper;
import com.valuelinkin.admin.dto.GroupDTO;
import com.valuelinkin.admin.service.UidGenerator;
import com.valuelinkin.admin.service.core.GroupService;

import tk.mybatis.mapper.entity.Example;

@Service
public class GroupServiceImpl implements GroupService{

	@Autowired
	private GroupMapper groupMapper;
	
	@Autowired
	private UidGenerator uidGenerator;

	public ResponseResult findPage(GroupDTO dto) {
		ResponseResult result = new ResponseResult();
		
		Group entity = new Group();
		BeanUtils.copyProperties(entity, dto);
		
		Page page = new Page();
		page.setPageNo(dto.getPageNo());
		page.setPageSize(dto.getPageSize());
		page.setTotalCount(groupMapper.selectCount(entity));
		
		Example example = new Example(Group.class);
		//example.createCriteria().andEqualTo("refId", rowNews.getRefId());
		example.setOrderByClause("grp_Sort_Index desc");
		
		List<Group> list = groupMapper.selectByExampleAndRowBounds(example, new RowBounds(page.getFirst(), page.getPageSize()));
		
		page.setResult(list);
		result.setData(page);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
		
	}	
	
	public ResponseResult getById(Long id) throws BusinessException{
		ResponseResult result = new ResponseResult();
		Group entity = groupMapper.selectByPrimaryKey(id);
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}

	
	/** 插入数据 */
	@Transactional
	public ResponseResult save(GroupDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		Group entity = new Group();
		BeanUtils.copyProperties(entity, dto);
		entity.setGrpId(uidGenerator.nextId());
		Date now = new Date();
		entity.setCreatedOn(now);
		entity.setModifiedOn(now);
		groupMapper.insertSelective(entity);
		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult deleteById(Long id) throws BusinessException{
		ResponseResult result = new ResponseResult();
		groupMapper.deleteByPrimaryKey(id);
		
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult delete(GroupDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		
		Group entity = new Group();
		BeanUtils.copyProperties(entity, dto);
		
		groupMapper.delete(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult deleteByIds(String ids) throws BusinessException{
		ResponseResult result = new ResponseResult();
		String[] items = ids.split(",");
		for (String str : items) {
			Long id = new Long(str);
			groupMapper.deleteByPrimaryKey(id);
		}
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult update(GroupDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		Group entity = new Group();
		BeanUtils.copyProperties(entity, dto);
		
		Date now = new Date();
		entity.setModifiedOn(now);
		
		groupMapper.updateByPrimaryKeySelective(entity);
		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	public ResponseResult findByExample(GroupDTO dto) throws BusinessException {
		ResponseResult result = new ResponseResult();
		
		Group entity = new Group();
		BeanUtils.copyProperties(entity, dto);
		
		List list =groupMapper.select(entity);
		
		result.setData(list);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	
	public ResponseResult getByExample(GroupDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		
		Group entity = new Group();
		BeanUtils.copyProperties(entity, dto);
		
		entity =groupMapper.selectOne(entity);
		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
		
	}
	
	public void setGroupMapper(GroupMapper dao) {
		this.groupMapper = dao;
	}
}
