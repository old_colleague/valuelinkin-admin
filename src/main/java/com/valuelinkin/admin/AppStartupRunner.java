
package com.valuelinkin.admin;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * 
 * 
 * @atuhor: nail.zhang
 * @description:
 * @date: Created in  2018年11月27日
 */
@Component
public class AppStartupRunner implements CommandLineRunner  {
	
	private static Logger logger = LoggerFactory.getLogger(AppStartupRunner.class);
	

	@Override
	public void run(String... arg0) throws Exception {
		logger.info("=====启动加载数据=====");
		//dataStatsQuartzJob.execute();
		logger.info("=====启动加载数据完成=====");

	}

}
