package com.valuelinkin.admin.dao.entity;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 〈功能详细描述〉
 *
 * @author Nail.zhang
 * @version [V0.1, Jan 9, 2018]
 */

@Table(name="vln_verify_code")
public class VerifyCode implements java.io.Serializable{
	private static final long serialVersionUID = 5454155825314635342L;
	

	//columns START
		@Id
		@Column(name = "id")
		private Long id;
		private java.lang.String phoneNo;
		private java.lang.String vrfCode;
		private java.util.Date vrfExpireTime;
		private Integer vrfSendCount;
		private java.util.Date createdOn;
		private java.util.Date modifiedOn;
	//columns END


	public VerifyCode(){
	}

	public VerifyCode(
		Long id
	){
		this.id = id;
	}

	

	public void setId(Long value) {
		this.id = value;
	}
	
	public Long getId() {
		return this.id;
	}
	
	public java.lang.String getPhoneNo() {
		return this.phoneNo;
	}
	
	public void setPhoneNo(java.lang.String value) {
		this.phoneNo = value;
	}
	
	public java.lang.String getVrfCode() {
		return this.vrfCode;
	}
	
	public void setVrfCode(java.lang.String value) {
		this.vrfCode = value;
	}
	
		
	public java.util.Date getVrfExpireTime() {
		return this.vrfExpireTime;
	}
	
	public void setVrfExpireTime(java.util.Date value) {
		this.vrfExpireTime = value;
	}
	
	public Integer getVrfSendCount() {
		return this.vrfSendCount;
	}
	
	public void setVrfSendCount(Integer value) {
		this.vrfSendCount = value;
	}
	
		
	public java.util.Date getCreatedOn() {
		return this.createdOn;
	}
	
	public void setCreatedOn(java.util.Date value) {
		this.createdOn = value;
	}
	
		
	public java.util.Date getModifiedOn() {
		return this.modifiedOn;
	}
	
	public void setModifiedOn(java.util.Date value) {
		this.modifiedOn = value;
	}
	

}

