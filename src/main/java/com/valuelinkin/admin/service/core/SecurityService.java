package com.valuelinkin.admin.service.core;

import java.util.List;

import com.jcommon.orm.Page;
import com.jcommon.exception.BusinessException;
import com.jcommon.result.ResponseResult;

import com.valuelinkin.admin.dto.SecurityDTO;

public interface SecurityService {

	ResponseResult findPage(SecurityDTO dto) throws BusinessException;
	
	ResponseResult getById(Long id) throws BusinessException;

	ResponseResult save(SecurityDTO dto) throws BusinessException;
	
	ResponseResult delete(SecurityDTO dto) throws BusinessException ;
	
	ResponseResult deleteById(Long id) throws BusinessException ;
	
	ResponseResult update(SecurityDTO dto) throws BusinessException ;
	
	ResponseResult findByExample(SecurityDTO dto) throws BusinessException;
	
	ResponseResult getByExample(SecurityDTO dto) throws BusinessException;
	
	ResponseResult deleteByIds(String ids) throws BusinessException ;
	

}
