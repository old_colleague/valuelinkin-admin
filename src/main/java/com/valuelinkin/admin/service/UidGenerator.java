package com.valuelinkin.admin.service;

public interface UidGenerator {
	
	Long nextId();

}
