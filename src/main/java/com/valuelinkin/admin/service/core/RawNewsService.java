package com.valuelinkin.admin.service.core;

import java.util.List;

import com.jcommon.orm.Page;
import com.jcommon.exception.BusinessException;
import com.jcommon.result.ResponseResult;

import com.valuelinkin.admin.dto.RawNewsDTO;

public interface RawNewsService {

	ResponseResult findPage(RawNewsDTO dto) throws BusinessException;
	
	ResponseResult getById(Long id) throws BusinessException;

	ResponseResult save(RawNewsDTO dto) throws BusinessException;
	
	ResponseResult delete(RawNewsDTO dto) throws BusinessException ;
	
	ResponseResult deleteById(Long id) throws BusinessException ;
	
	ResponseResult update(RawNewsDTO dto) throws BusinessException ;
	
	ResponseResult findByExample(RawNewsDTO dto) throws BusinessException;
	
	ResponseResult getByExample(RawNewsDTO dto) throws BusinessException;
	
	ResponseResult deleteByIds(String ids) throws BusinessException ;
	

}
