package com.valuelinkin.admin.dto;

/**
 * 
 * @atuhor: nail.zhang
 * @description:
 * @date: Created in  2019年2月10日
 */
public class GroupStockDTO {
	
	private Long id;
	private Long stkId;
	private Long grpId;
	private Integer sortIndex;
	
	private java.lang.String stkCode;
	private java.lang.String stkName;
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the stkId
	 */
	public Long getStkId() {
		return stkId;
	}
	/**
	 * @param stkId the stkId to set
	 */
	public void setStkId(Long stkId) {
		this.stkId = stkId;
	}
	/**
	 * @return the grpId
	 */
	public Long getGrpId() {
		return grpId;
	}
	/**
	 * @param grpId the grpId to set
	 */
	public void setGrpId(Long grpId) {
		this.grpId = grpId;
	}
	/**
	 * @return the sortIndex
	 */
	public Integer getSortIndex() {
		return sortIndex;
	}
	/**
	 * @param sortIndex the sortIndex to set
	 */
	public void setSortIndex(Integer sortIndex) {
		this.sortIndex = sortIndex;
	}
	/**
	 * @return the stkCode
	 */
	public java.lang.String getStkCode() {
		return stkCode;
	}
	/**
	 * @param stkCode the stkCode to set
	 */
	public void setStkCode(java.lang.String stkCode) {
		this.stkCode = stkCode;
	}
	/**
	 * @return the stkName
	 */
	public java.lang.String getStkName() {
		return stkName;
	}
	/**
	 * @param stkName the stkName to set
	 */
	public void setStkName(java.lang.String stkName) {
		this.stkName = stkName;
	}

}
