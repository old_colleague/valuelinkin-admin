package com.valuelinkin.admin.dto;

import com.valuelinkin.admin.dto.BaseDTO;

public class GroupDTO extends BaseDTO {
    private static final long serialVersionUID = 3148176768559230877L;
    

	private Long grpId;
	private java.lang.String grpName;
	private java.lang.String grpDesc;
	private Integer grpStatus;
	private Integer grpSortIndex;
	private java.util.Date createdOn;
	private java.util.Date modifiedOn;

	
	public Long getGrpId() {
		return this.grpId;
	}
	
	public void setGrpId(Long value) {
		this.grpId = value;
	}
	
	public java.lang.String getGrpName() {
		return this.grpName;
	}
	
	public void setGrpName(java.lang.String value) {
		this.grpName = value;
	}
	
	public java.lang.String getGrpDesc() {
		return this.grpDesc;
	}
	
	public void setGrpDesc(java.lang.String value) {
		this.grpDesc = value;
	}
	
	public Integer getGrpStatus() {
		return this.grpStatus;
	}
	
	public void setGrpStatus(Integer value) {
		this.grpStatus = value;
	}
	
	public Integer getGrpSortIndex() {
		return this.grpSortIndex;
	}
	
	public void setGrpSortIndex(Integer value) {
		this.grpSortIndex = value;
	}
	
	public java.util.Date getCreatedOn() {
		return this.createdOn;
	}
	
	public void setCreatedOn(java.util.Date value) {
		this.createdOn = value;
	}
	
	public java.util.Date getModifiedOn() {
		return this.modifiedOn;
	}
	
	public void setModifiedOn(java.util.Date value) {
		this.modifiedOn = value;
	}
	
}

