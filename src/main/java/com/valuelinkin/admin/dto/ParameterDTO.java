package com.valuelinkin.admin.dto;

import com.valuelinkin.admin.dto.BaseDTO;

public class ParameterDTO extends BaseDTO {
    private static final long serialVersionUID = 3148176768559230877L;
    

	private Long prmId;
	private java.lang.String prmName;
	private java.lang.String prmKey;
	private java.lang.String prmValue;
	private java.lang.String prmGroup;
	private java.util.Date createdOn;
	private java.util.Date modifiedOn;

	
	public Long getPrmId() {
		return this.prmId;
	}
	
	public void setPrmId(Long value) {
		this.prmId = value;
	}
	
	public java.lang.String getPrmName() {
		return this.prmName;
	}
	
	public void setPrmName(java.lang.String value) {
		this.prmName = value;
	}
	
	public java.lang.String getPrmKey() {
		return this.prmKey;
	}
	
	public void setPrmKey(java.lang.String value) {
		this.prmKey = value;
	}
	
	public java.lang.String getPrmValue() {
		return this.prmValue;
	}
	
	public void setPrmValue(java.lang.String value) {
		this.prmValue = value;
	}
	
	public java.lang.String getPrmGroup() {
		return this.prmGroup;
	}
	
	public void setPrmGroup(java.lang.String value) {
		this.prmGroup = value;
	}
	
	public java.util.Date getCreatedOn() {
		return this.createdOn;
	}
	
	public void setCreatedOn(java.util.Date value) {
		this.createdOn = value;
	}
	
	public java.util.Date getModifiedOn() {
		return this.modifiedOn;
	}
	
	public void setModifiedOn(java.util.Date value) {
		this.modifiedOn = value;
	}
	
}

