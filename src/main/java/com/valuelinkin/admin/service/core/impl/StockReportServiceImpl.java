package com.valuelinkin.admin.service.core.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jcommon.orm.Page;
import com.jcommon.exception.BusinessException;
import com.jcommon.utils.BeanUtils;
import com.jcommon.result.ResponseResult;

import com.valuelinkin.admin.dao.entity.StockReport;
import com.valuelinkin.admin.dto.StockReportDTO;
import com.valuelinkin.admin.dao.mapper.StockReportMapper;
import com.valuelinkin.admin.service.core.StockReportService;
import com.valuelinkin.admin.service.UidGenerator;
import com.valuelinkin.admin.common.utils.AppConstants;

import tk.mybatis.mapper.entity.Example;

@Service
public class StockReportServiceImpl implements StockReportService{

	@Autowired
	private StockReportMapper stockReportMapper;
	
	@Autowired
	private UidGenerator uidGenerator;

	public ResponseResult findPage(StockReportDTO dto) {
		ResponseResult result = new ResponseResult();
		
		StockReport entity = new StockReport();
		BeanUtils.copyProperties(entity, dto);
		
		Page page = new Page();
		page.setPageNo(dto.getPageNo());
		page.setPageSize(dto.getPageSize());
		page.setTotalCount(stockReportMapper.selectCount(entity));
		
		Example example = new Example(StockReport.class);
		//example.createCriteria().andEqualTo("refId", rowNews.getRefId());
		//example.setOrderByClause("nws_Publish_Date desc");
		
		List<StockReport> list = stockReportMapper.selectByExampleAndRowBounds(example, new RowBounds(page.getFirst(), page.getPageSize()));
		
		page.setResult(list);
		result.setData(page);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
		
	}	
	
	public ResponseResult getById(Long id) throws BusinessException{
		ResponseResult result = new ResponseResult();
		StockReport entity = stockReportMapper.selectByPrimaryKey(id);
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}

	
	/** 插入数据 */
	@Transactional
	public ResponseResult save(StockReportDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		StockReport entity = new StockReport();
		BeanUtils.copyProperties(entity, dto);
		entity.setId(uidGenerator.nextId());
		Date now = new Date();
		entity.setCreatedOn(now);
		entity.setModifiedOn(now);
		entity.setRptPublishDate(now);
		stockReportMapper.insertSelective(entity);
		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult deleteById(Long id) throws BusinessException{
		ResponseResult result = new ResponseResult();
		stockReportMapper.deleteByPrimaryKey(id);
		
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult delete(StockReportDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		
		StockReport entity = new StockReport();
		BeanUtils.copyProperties(entity, dto);
		
		stockReportMapper.delete(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult deleteByIds(String ids) throws BusinessException{
		ResponseResult result = new ResponseResult();
		String[] items = ids.split(",");
		for (String str : items) {
			Long id = new Long(str);
			stockReportMapper.deleteByPrimaryKey(id);
		}
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult update(StockReportDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		StockReport entity = new StockReport();
		BeanUtils.copyProperties(entity, dto);
		Date now = new Date();
		entity.setModifiedOn(now);
		
		stockReportMapper.updateByPrimaryKeySelective(entity);
		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	public ResponseResult findByExample(StockReportDTO dto) throws BusinessException {
		ResponseResult result = new ResponseResult();
		
		StockReport entity = new StockReport();
		BeanUtils.copyProperties(entity, dto);
		
		List list =stockReportMapper.select(entity);
		
		result.setData(list);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	
	public ResponseResult getByExample(StockReportDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		
		StockReport entity = new StockReport();
		BeanUtils.copyProperties(entity, dto);
		
		entity =stockReportMapper.selectOne(entity);
		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
		
	}
	
	public void setStockReportMapper(StockReportMapper dao) {
		this.stockReportMapper = dao;
	}
}
