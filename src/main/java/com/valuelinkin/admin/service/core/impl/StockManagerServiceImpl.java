package com.valuelinkin.admin.service.core.impl;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jcommon.exception.BusinessException;
import com.jcommon.orm.Page;
import com.jcommon.result.ResponseResult;
import com.jcommon.utils.BeanUtils;
import com.valuelinkin.admin.common.utils.AppConstants;
import com.valuelinkin.admin.dao.entity.StockManager;
import com.valuelinkin.admin.dao.mapper.StockManagerMapper;
import com.valuelinkin.admin.dto.StockManagerDTO;
import com.valuelinkin.admin.service.UidGenerator;
import com.valuelinkin.admin.service.core.StockManagerService;

import tk.mybatis.mapper.entity.Example;

@Service
public class StockManagerServiceImpl implements StockManagerService{

	@Autowired
	private StockManagerMapper stockManagerMapper;
	
	@Autowired
	private UidGenerator uidGenerator;

	public ResponseResult findPage(StockManagerDTO dto) {
		ResponseResult result = new ResponseResult();
		
		StockManager entity = new StockManager();
		BeanUtils.copyProperties(entity, dto);
		
		Page page = new Page();
		page.setPageNo(dto.getPageNo());
		page.setPageSize(dto.getPageSize());
		page.setTotalCount(stockManagerMapper.selectCount(entity));
		
		Example example = new Example(StockManager.class);
		//example.createCriteria().andEqualTo("refId", rowNews.getRefId());
		example.setOrderByClause("mgr_sort_index desc");
		
		List<StockManager> list = stockManagerMapper.selectByExampleAndRowBounds(example, new RowBounds(page.getFirst(), page.getPageSize()));
		
		page.setResult(list);
		result.setData(page);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
		
	}	
	
	public ResponseResult getById(Long id) throws BusinessException{
		ResponseResult result = new ResponseResult();
		StockManager entity = stockManagerMapper.selectByPrimaryKey(id);
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}

	
	/** 插入数据 */
	@Transactional
	public ResponseResult save(StockManagerDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		StockManager entity = new StockManager();
		BeanUtils.copyProperties(entity, dto);
		entity.setId(uidGenerator.nextId());
		Date now = new Date();
		entity.setCreatedOn(now);
		entity.setModifiedOn(now);
		
		stockManagerMapper.insertSelective(entity);
		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult deleteById(Long id) throws BusinessException{
		ResponseResult result = new ResponseResult();
		stockManagerMapper.deleteByPrimaryKey(id);
		
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult delete(StockManagerDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		
		StockManager entity = new StockManager();
		BeanUtils.copyProperties(entity, dto);
		
		stockManagerMapper.delete(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult deleteByIds(String ids) throws BusinessException{
		ResponseResult result = new ResponseResult();
		String[] items = ids.split(",");
		for (String str : items) {
			Long id = new Long(str);
			stockManagerMapper.deleteByPrimaryKey(id);
		}
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult update(StockManagerDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		StockManager entity = new StockManager();
		BeanUtils.copyProperties(entity, dto);
		stockManagerMapper.updateByPrimaryKeySelective(entity);
		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	public ResponseResult findByExample(StockManagerDTO dto) throws BusinessException {
		ResponseResult result = new ResponseResult();
		
		StockManager entity = new StockManager();
		BeanUtils.copyProperties(entity, dto);

		Example example = new Example(StockManager.class);
		example.createCriteria().andEqualTo("stkCode", dto.getStkCode());
		example.setOrderByClause("mgr_sort_index desc");

		List list =stockManagerMapper.selectByExample(example);
		
		result.setData(list);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	
	public ResponseResult getByExample(StockManagerDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		
		StockManager entity = new StockManager();
		BeanUtils.copyProperties(entity, dto);
		
		entity =stockManagerMapper.selectOne(entity);
		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
		
	}
	
	public void setStockManagerMapper(StockManagerMapper dao) {
		this.stockManagerMapper = dao;
	}
}
