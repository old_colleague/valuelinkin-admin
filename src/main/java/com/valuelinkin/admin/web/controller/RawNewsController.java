package com.valuelinkin.admin.web.controller;

import java.util.List;

import com.jcommon.orm.Page;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.ui.Model;

import com.valuelinkin.admin.dto.RawNewsDTO;
import com.valuelinkin.admin.service.core.RawNewsService;

import com.jcommon.result.ResponseResult;
import com.jcommon.utils.BeanUtils;

@Controller
@RequestMapping(value="/rawNews") 
public class RawNewsController{
	
	@Autowired
	private RawNewsService rawNewsService;
	
	@RequestMapping(value="/index",method=RequestMethod.GET)
	public String index( Model model) throws Exception {
		
		return "/rawNews/list";
	}
	
	/** 
	 * 执行搜索 
	 **/
	@RequestMapping(value="",method=RequestMethod.GET)
	@ResponseBody
	public ResponseResult findPage(RawNewsDTO dto) {
		
		ResponseResult result = new ResponseResult();
		result = this.rawNewsService.findPage(dto);
		return result;
	}
	
	
	/** 
	 * 查看对象
	 **/
	@RequestMapping(value="/view/{id}",method=RequestMethod.GET)
	public String getById(@PathVariable Long id, Model model) {
		ResponseResult result = new ResponseResult();
		result = rawNewsService.getById(id);
		
		model.addAttribute("result", result);
		return "/rawNews/view";
		
	}
	

	/** 
	 * 进入新增页面
	 **/
	@RequestMapping(value="/create", method=RequestMethod.GET)
	public String create(RawNewsDTO dto, Model model) throws Exception {
		model.addAttribute("rawNews", dto);
		return "/rawNews/create";
	}
	
	/** 
	 * 保存新增对象
	 **/
	@RequestMapping(value="",method=RequestMethod.POST)
	@ResponseBody
	public ResponseResult save(RawNewsDTO dto) {
		ResponseResult result = null;
		result = rawNewsService.save(dto);
		return result;
	}
	
	
	/**
	 * 进入更新页面
	 **/
	@RequestMapping(value="/edit/{id}", method=RequestMethod.GET)
	public String edit(@PathVariable Long id, Model model) throws Exception {

		ResponseResult result  = rawNewsService.getById(id);
		model.addAttribute("result", result);
		
		return "/rawNews/edit";
	}
	
	/**
	 * 保存更新对象
	 **/
	@RequestMapping(value="/{id}", method=RequestMethod.PUT)
	@ResponseBody
	public ResponseResult update(@PathVariable Long id,RawNewsDTO dto) throws Exception {
		ResponseResult result = new ResponseResult();
		dto.setNwsId(id);
		result = rawNewsService.update(dto);
		return result;
	}
	
	
	/**
	 *删除多个对象
	 **/
	@RequestMapping(value="/{ids}", method=RequestMethod.DELETE)
	@ResponseBody
	public ResponseResult delete(@PathVariable String ids) {
		
		ResponseResult result = null;
		result = rawNewsService.deleteByIds(ids);
		return result;
		
	}
	
	public void setRawNewsService(RawNewsService service) {
		this.rawNewsService = service;
	}
	
}
