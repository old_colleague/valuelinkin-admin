package com.valuelinkin.admin.service.core;

import java.util.List;

import com.jcommon.orm.Page;
import com.jcommon.exception.BusinessException;
import com.jcommon.result.ResponseResult;

import com.valuelinkin.admin.dto.StockReportDTO;

public interface StockReportService {

	ResponseResult findPage(StockReportDTO dto) throws BusinessException;
	
	ResponseResult getById(Long id) throws BusinessException;

	ResponseResult save(StockReportDTO dto) throws BusinessException;
	
	ResponseResult delete(StockReportDTO dto) throws BusinessException ;
	
	ResponseResult deleteById(Long id) throws BusinessException ;
	
	ResponseResult update(StockReportDTO dto) throws BusinessException ;
	
	ResponseResult findByExample(StockReportDTO dto) throws BusinessException;
	
	ResponseResult getByExample(StockReportDTO dto) throws BusinessException;
	
	ResponseResult deleteByIds(String ids) throws BusinessException ;
	

}
