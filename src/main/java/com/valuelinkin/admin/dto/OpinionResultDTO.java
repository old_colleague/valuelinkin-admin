package com.valuelinkin.admin.dto;

import java.util.Date;

/**
 * @atuhor: nail.zhang
 * @description:
 * @date: 2019/2/17 13:48
 */
public class OpinionResultDTO {

    private Long opnId;
    private Long clmId;
    private Long usrId;
    private java.lang.String opnTitle;
    private java.lang.String opnBrief;
    private java.util.Date opnPublishDate;
    private Integer opnStatus;
    private java.lang.String opnStockCode;
    private java.lang.String opnViewCount;
    private Integer opnStickTop;
    private java.lang.String opnColor;
    private java.lang.String opnThumbnailUrl;
    private java.lang.String opnVideoUrl;
    private java.util.Date createdOn;
    private java.util.Date modifiedOn;

    private Integer opnType;
    private String stkName;
    private String clmDisplayName;
    private  String usrNickName;
    private String usrPhone;



    public String getClmDisplayName() {
        return clmDisplayName;
    }

    public void setClmDisplayName(String clmDisplayName) {
        this.clmDisplayName = clmDisplayName;
    }

    public String getUsrNickName() {
        return usrNickName;
    }

    public void setUsrNickName(String usrNickName) {
        this.usrNickName = usrNickName;
    }

    public String getUsrPhone() {
        return usrPhone;
    }

    public void setUsrPhone(String usrPhone) {
        this.usrPhone = usrPhone;
    }

    public Long getOpnId() {
        return opnId;
    }

    public void setOpnId(Long opnId) {
        this.opnId = opnId;
    }

    public Long getClmId() {
        return clmId;
    }

    public void setClmId(Long clmId) {
        this.clmId = clmId;
    }

    public Long getUsrId() {
        return usrId;
    }

    public void setUsrId(Long usrId) {
        this.usrId = usrId;
    }

    public String getOpnTitle() {
        return opnTitle;
    }

    public void setOpnTitle(String opnTitle) {
        this.opnTitle = opnTitle;
    }

    public String getOpnBrief() {
        return opnBrief;
    }

    public void setOpnBrief(String opnBrief) {
        this.opnBrief = opnBrief;
    }

    public Date getOpnPublishDate() {
        return opnPublishDate;
    }

    public void setOpnPublishDate(Date opnPublishDate) {
        this.opnPublishDate = opnPublishDate;
    }

    public Integer getOpnStatus() {
        return opnStatus;
    }

    public void setOpnStatus(Integer opnStatus) {
        this.opnStatus = opnStatus;
    }

    public String getOpnStockCode() {
        return opnStockCode;
    }

    public void setOpnStockCode(String opnStockCode) {
        this.opnStockCode = opnStockCode;
    }

    public String getOpnViewCount() {
        return opnViewCount;
    }

    public void setOpnViewCount(String opnViewCount) {
        this.opnViewCount = opnViewCount;
    }

    public Integer getOpnStickTop() {
        return opnStickTop;
    }

    public void setOpnStickTop(Integer opnStickTop) {
        this.opnStickTop = opnStickTop;
    }

    public String getOpnColor() {
        return opnColor;
    }

    public void setOpnColor(String opnColor) {
        this.opnColor = opnColor;
    }

    public String getOpnThumbnailUrl() {
        return opnThumbnailUrl;
    }

    public void setOpnThumbnailUrl(String opnThumbnailUrl) {
        this.opnThumbnailUrl = opnThumbnailUrl;
    }

    public String getOpnVideoUrl() {
        return opnVideoUrl;
    }

    public void setOpnVideoUrl(String opnVideoUrl) {
        this.opnVideoUrl = opnVideoUrl;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Date getModifiedOn() {
        return modifiedOn;
    }

    public void setModifiedOn(Date modifiedOn) {
        this.modifiedOn = modifiedOn;
    }

    public Integer getOpnType() {
        return opnType;
    }

    public void setOpnType(Integer opnType) {
        this.opnType = opnType;
    }

    public String getStkName() {
        return stkName;
    }

    public void setStkName(String stkName) {
        this.stkName = stkName;
    }
}
