package com.valuelinkin.admin.dao.mapper;


import org.apache.ibatis.annotations.Mapper;

import com.valuelinkin.admin.dao.entity.RawNews;
import com.valuelinkin.admin.dao.BaseMapper;

@Mapper
public interface RawNewsMapper extends BaseMapper<RawNews>{
	

}
