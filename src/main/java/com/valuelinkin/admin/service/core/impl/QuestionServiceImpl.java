package com.valuelinkin.admin.service.core.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.jcommon.utils.StringUtils;
import com.valuelinkin.admin.dao.entity.Answer;
import com.valuelinkin.admin.dao.entity.NewsLive;
import com.valuelinkin.admin.dao.mapper.AnswerMapper;
import com.valuelinkin.admin.dto.QuestionResultDTO;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jcommon.orm.Page;
import com.jcommon.exception.BusinessException;
import com.jcommon.utils.BeanUtils;
import com.jcommon.result.ResponseResult;

import com.valuelinkin.admin.dao.entity.Question;
import com.valuelinkin.admin.dto.QuestionDTO;
import com.valuelinkin.admin.dao.mapper.QuestionMapper;
import com.valuelinkin.admin.service.core.QuestionService;
import com.valuelinkin.admin.service.UidGenerator;
import com.valuelinkin.admin.common.utils.AppConstants;

import tk.mybatis.mapper.entity.Example;

@Service
public class QuestionServiceImpl implements QuestionService {

    @Autowired
    private QuestionMapper questionMapper;

    @Autowired
    private UidGenerator uidGenerator;

    @Autowired
    private AnswerMapper answerMapper;

    public ResponseResult findPage(QuestionDTO dto) {
        ResponseResult result = new ResponseResult();

        Question entity = new Question();
        BeanUtils.copyProperties(entity, dto);

        Page page = new Page();
        page.setPageNo(dto.getPageNo());
        page.setPageSize(dto.getPageSize());

        Example example = new Example(Question.class);
        Example.Criteria criteria = example.createCriteria();
        String keywords = StringUtils.trimToNull(dto.getKeywords());
        dto.setKeywords(keywords);
        if (StringUtils.isNotBlank(keywords)) {
            keywords = "%" + keywords + "%";
            dto.setKeywords(keywords);
            criteria.orLike("qsnContent", keywords);
        }
        if (dto.getQsnStatus() != null) {
            Example.Criteria criteria2 = example.createCriteria();
            criteria2.andEqualTo("qsnStatus", dto.getQsnStatus());
            example.and(criteria2);
        }
        page.setTotalCount(questionMapper.selectCountByExample(example));

        List<QuestionResultDTO> list = questionMapper.findPage(dto);
        for (QuestionResultDTO questionResultDTO : list) {
            Answer answer = new Answer();
            answer.setQsnId(questionResultDTO.getQsnId());
            answer = answerMapper.selectOne(answer);
            if(answer==null){
                answer = new Answer();
            }
            questionResultDTO.setAnswer(answer);
        }

        page.setResult(list);
        result.setData(page);
        result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
        return result;
    }

    public ResponseResult getById(Long id) throws BusinessException {
        ResponseResult result = new ResponseResult();
        Question entity = questionMapper.selectByPrimaryKey(id);
        QuestionResultDTO questionResultDTO = new QuestionResultDTO();

        BeanUtils.copyProperties(questionResultDTO, entity);

        Answer answer = new Answer();
        answer.setQsnId(entity.getQsnId());
        answer = answerMapper.selectOne(answer);
        if(answer!=null){
            questionResultDTO.setAnswer(answer);
        }

        result.setData(questionResultDTO);
        result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
        return result;
    }


    /**
     * 插入数据
     */
    @Transactional
    public ResponseResult save(QuestionDTO dto) throws BusinessException {
        ResponseResult result = new ResponseResult();
        Question entity = new Question();
        BeanUtils.copyProperties(entity, dto);
        entity.setQsnId(uidGenerator.nextId());

        questionMapper.insertSelective(entity);

        result.setData(entity);
        result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
        return result;
    }

    @Transactional
    public ResponseResult deleteById(Long id) throws BusinessException {
        ResponseResult result = new ResponseResult();
        questionMapper.deleteByPrimaryKey(id);

        result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
        return result;
    }

    @Transactional
    public ResponseResult delete(QuestionDTO dto) throws BusinessException {
        ResponseResult result = new ResponseResult();

        Question entity = new Question();
        BeanUtils.copyProperties(entity, dto);

        questionMapper.delete(entity);
        result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
        return result;
    }

    @Transactional
    public ResponseResult deleteByIds(String ids) throws BusinessException {
        ResponseResult result = new ResponseResult();
        String[] items = ids.split(",");
        for (String str : items) {
            Long id = new Long(str);
            questionMapper.deleteByPrimaryKey(id);
        }
        result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
        return result;
    }

    @Transactional
    public ResponseResult update(QuestionDTO dto) throws BusinessException {
        ResponseResult result = new ResponseResult();
        Question entity = new Question();
        Date now = new Date();
        BeanUtils.copyProperties(entity, dto);
        entity.setModifiedOn(now);
        entity.setQsnStatus(1);
        questionMapper.updateByPrimaryKeySelective(entity);

        Answer answer = new Answer();

        answer.setQsnId(entity.getQsnId());
        answer = answerMapper.selectOne(answer);
        if(answer==null){
            answer = new Answer();
        }
        answer.setQsnId(entity.getQsnId());

        answer.setAnsContent(dto.getAnsContent());
        answer.setAnsStatus(0);
        answer.setUsrId(dto.getAnsReplier());
        answer.setCreatedOn(now);
        answer.setModifiedOn(now);
        if(answer.getAnsId()==null){
            answer.setAnsId(uidGenerator.nextId());
            answerMapper.insertSelective(answer);
        }else{
            answerMapper.updateByPrimaryKeySelective(answer);
        }

        result.setData(entity);
        result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
        return result;
    }

    public ResponseResult findByExample(QuestionDTO dto) throws BusinessException {
        ResponseResult result = new ResponseResult();

        Question entity = new Question();
        BeanUtils.copyProperties(entity, dto);

        List list = questionMapper.select(entity);

        result.setData(list);
        result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
        return result;
    }


    public ResponseResult getByExample(QuestionDTO dto) throws BusinessException {
        ResponseResult result = new ResponseResult();

        Question entity = new Question();
        BeanUtils.copyProperties(entity, dto);

        entity = questionMapper.selectOne(entity);

        result.setData(entity);
        result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
        return result;

    }

    public void setQuestionMapper(QuestionMapper dao) {
        this.questionMapper = dao;
    }
}
