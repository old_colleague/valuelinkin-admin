package com.valuelinkin.admin.dao.mapper;


import org.apache.ibatis.annotations.Mapper;

import com.valuelinkin.admin.dao.entity.SysRole;
import com.valuelinkin.admin.dao.BaseMapper;

@Mapper
public interface SysRoleMapper extends BaseMapper<SysRole>{
	

}
