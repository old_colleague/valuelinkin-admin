package com.valuelinkin.admin.dao.entity;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 〈功能详细描述〉
 *
 * @author Nail.zhang
 * @version [V0.1, Jan 9, 2018]
 */

@Table(name="vln_ads")
public class Ads implements java.io.Serializable{
	private static final long serialVersionUID = 5454155825314635342L;
	

	//columns START
		@Id
		@Column(name = "ads_id")
		private Long adsId;
		private java.lang.String adsLocation;
		private java.lang.String adsTerminalType;
		private java.lang.String adsTitle;
		private java.lang.String adsImgUrl;
		private java.lang.String adsActionType;
		private java.lang.String adsActionLink;
		private Integer adsSortIndex;
		private java.lang.String adsRemark;
		private java.util.Date createdOn;
		private java.util.Date modifiedOn;
	//columns END


	public Ads(){
	}

	public Ads(
		Long adsId
	){
		this.adsId = adsId;
	}

	

	public void setAdsId(Long value) {
		this.adsId = value;
	}
	
	public Long getAdsId() {
		return this.adsId;
	}
	
	public java.lang.String getAdsLocation() {
		return this.adsLocation;
	}
	
	public void setAdsLocation(java.lang.String value) {
		this.adsLocation = value;
	}
	
	public java.lang.String getAdsTerminalType() {
		return this.adsTerminalType;
	}
	
	public void setAdsTerminalType(java.lang.String value) {
		this.adsTerminalType = value;
	}
	
	public java.lang.String getAdsTitle() {
		return this.adsTitle;
	}
	
	public void setAdsTitle(java.lang.String value) {
		this.adsTitle = value;
	}
	
	public java.lang.String getAdsImgUrl() {
		return this.adsImgUrl;
	}
	
	public void setAdsImgUrl(java.lang.String value) {
		this.adsImgUrl = value;
	}
	
	public java.lang.String getAdsActionType() {
		return this.adsActionType;
	}
	
	public void setAdsActionType(java.lang.String value) {
		this.adsActionType = value;
	}
	
	public java.lang.String getAdsActionLink() {
		return this.adsActionLink;
	}
	
	public void setAdsActionLink(java.lang.String value) {
		this.adsActionLink = value;
	}
	
	public Integer getAdsSortIndex() {
		return this.adsSortIndex;
	}
	
	public void setAdsSortIndex(Integer value) {
		this.adsSortIndex = value;
	}
	
	public java.lang.String getAdsRemark() {
		return this.adsRemark;
	}
	
	public void setAdsRemark(java.lang.String value) {
		this.adsRemark = value;
	}
	
		
	public java.util.Date getCreatedOn() {
		return this.createdOn;
	}
	
	public void setCreatedOn(java.util.Date value) {
		this.createdOn = value;
	}
	
		
	public java.util.Date getModifiedOn() {
		return this.modifiedOn;
	}
	
	public void setModifiedOn(java.util.Date value) {
		this.modifiedOn = value;
	}
	

}

