package com.valuelinkin.admin.web.controller;

import java.util.List;

import com.jcommon.orm.Page;

import com.jcommon.utils.StringUtils;
import com.valuelinkin.admin.dao.entity.NewsColumn;
import com.valuelinkin.admin.dao.mapper.NewsColumnMapper;
import com.valuelinkin.admin.service.core.NewsColumnService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.ui.Model;

import com.valuelinkin.admin.dto.OpinionDTO;
import com.valuelinkin.admin.service.core.OpinionService;

import com.jcommon.result.ResponseResult;
import com.jcommon.utils.BeanUtils;

@Controller
@RequestMapping(value="/opinion") 
public class OpinionController{
	
	@Autowired
	private OpinionService opinionService;

	@Autowired
	private NewsColumnService newsColumnService;

	@Autowired
	private NewsColumnMapper newsColumnMapper;
	
	@RequestMapping(value="/index",method=RequestMethod.GET)
	public String index( Model model,OpinionDTO dto) throws Exception {

		model.addAttribute("opinion", dto);
		
		return "/opinion/list";
	}
	
	/** 
	 * 执行搜索 
	 **/
	@RequestMapping(value="",method=RequestMethod.GET)
	@ResponseBody
	public ResponseResult findPage(OpinionDTO dto) {
		
		ResponseResult result = new ResponseResult();
		result = this.opinionService.findPage(dto);
		return result;
	}
	
	
	/** 
	 * 查看对象
	 **/
	@RequestMapping(value="/view/{id}",method=RequestMethod.GET)
	public String getById(@PathVariable Long id, Model model) {
		ResponseResult result = new ResponseResult();
		result = opinionService.getById(id);
		
		model.addAttribute("result", result);
		return "/opinion/view";
		
	}
	

	/** 
	 * 进入新增页面
	 **/
	@RequestMapping(value="/create", method=RequestMethod.GET)
	public String create(OpinionDTO dto, Model model) throws Exception {


		if(StringUtils.isNotBlank(dto.getClmCode())){
			NewsColumn newsColumn = new NewsColumn();
			newsColumn.setClmCode(dto.getClmCode());
			newsColumn = newsColumnMapper.selectOne(newsColumn);
			dto.setClmId(newsColumn.getClmId());
		}else{
			ResponseResult result = newsColumnService.findChildColumnByCode("GD");
			model.addAttribute("columnList", result.getData());
		}
		model.addAttribute("opinion", dto);

		return "/opinion/create";
	}
	
	/** 
	 * 保存新增对象
	 **/
	@RequestMapping(value="",method=RequestMethod.POST)
	@ResponseBody
	public ResponseResult save(OpinionDTO dto) {
		ResponseResult result = null;
		result = opinionService.save(dto);
		return result;
	}
	
	
	/**
	 * 进入更新页面
	 **/
	@RequestMapping(value="/edit/{id}", method=RequestMethod.GET)
	public String edit(@PathVariable Long id, Model model) throws Exception {

		ResponseResult result  = opinionService.getById(id);
		model.addAttribute("result", result);

		result = newsColumnService.findChildColumnByCode("GD");
		model.addAttribute("columnList", result.getData());
		
		return "/opinion/edit";
	}
	
	/**
	 * 保存更新对象
	 **/
	@RequestMapping(value="/{id}", method=RequestMethod.PUT)
	@ResponseBody
	public ResponseResult update(@PathVariable Long id,OpinionDTO dto) throws Exception {
		ResponseResult result = new ResponseResult();
		dto.setOpnId(id);
		result = opinionService.update(dto);
		return result;
	}
	
	
	/**
	 *删除多个对象
	 **/
	@RequestMapping(value="/{ids}", method=RequestMethod.DELETE)
	@ResponseBody
	public ResponseResult delete(@PathVariable String ids) {
		
		ResponseResult result = null;
		result = opinionService.deleteByIds(ids);
		return result;
		
	}
	
	public void setOpinionService(OpinionService service) {
		this.opinionService = service;
	}
	
}
