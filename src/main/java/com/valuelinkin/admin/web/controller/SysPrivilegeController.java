package com.valuelinkin.admin.web.controller;

import java.util.List;

import com.jcommon.orm.Page;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.ui.Model;

import com.valuelinkin.admin.dto.SysPrivilegeDTO;
import com.valuelinkin.admin.service.core.SysPrivilegeService;

import com.jcommon.result.ResponseResult;
import com.jcommon.utils.BeanUtils;

@Controller
@RequestMapping(value="/sysPrivilege") 
public class SysPrivilegeController{
	
	@Autowired
	private SysPrivilegeService sysPrivilegeService;
	
	@RequestMapping(value="/index",method=RequestMethod.GET)
	public String index( Model model) throws Exception {
		
		return "/sysPrivilege/list";
	}
	
	/** 
	 * 执行搜索 
	 **/
	@RequestMapping(value="",method=RequestMethod.GET)
	@ResponseBody
	public ResponseResult findPage(SysPrivilegeDTO dto) {
		
		ResponseResult result = new ResponseResult();
		result = this.sysPrivilegeService.findPage(dto);
		return result;
	}
	
	
	/** 
	 * 查看对象
	 **/
	@RequestMapping(value="/view/{id}",method=RequestMethod.GET)
	public String getById(@PathVariable Long id, Model model) {
		ResponseResult result = new ResponseResult();
		result = sysPrivilegeService.getById(id);
		
		model.addAttribute("result", result);
		return "/sysPrivilege/view";
		
	}
	

	/** 
	 * 进入新增页面
	 **/
	@RequestMapping(value="/create", method=RequestMethod.GET)
	public String create(SysPrivilegeDTO dto, Model model) throws Exception {
		model.addAttribute("sysPrivilege", dto);
		return "/sysPrivilege/create";
	}
	
	/** 
	 * 保存新增对象
	 **/
	@RequestMapping(value="",method=RequestMethod.POST)
	@ResponseBody
	public ResponseResult save(SysPrivilegeDTO dto) {
		ResponseResult result = null;
		result = sysPrivilegeService.save(dto);
		return result;
	}
	
	
	/**
	 * 进入更新页面
	 **/
	@RequestMapping(value="/edit/{id}", method=RequestMethod.GET)
	public String edit(@PathVariable Long id, Model model) throws Exception {

		ResponseResult result  = sysPrivilegeService.getById(id);
		model.addAttribute("result", result);
		
		return "/sysPrivilege/edit";
	}
	
	/**
	 * 保存更新对象
	 **/
	@RequestMapping(value="/{id}", method=RequestMethod.PUT)
	@ResponseBody
	public ResponseResult update(@PathVariable Long id,SysPrivilegeDTO dto) throws Exception {
		ResponseResult result = new ResponseResult();
		dto.setPvlId(id);
		result = sysPrivilegeService.update(dto);
		return result;
	}
	
	
	/**
	 *删除多个对象
	 **/
	@RequestMapping(value="/{ids}", method=RequestMethod.DELETE)
	@ResponseBody
	public ResponseResult delete(@PathVariable String ids) {
		
		ResponseResult result = null;
		result = sysPrivilegeService.deleteByIds(ids);
		return result;
		
	}
	
	public void setSysPrivilegeService(SysPrivilegeService service) {
		this.sysPrivilegeService = service;
	}
	
}
