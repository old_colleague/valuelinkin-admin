package com.valuelinkin.admin.service.core.impl;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jcommon.exception.BusinessException;
import com.jcommon.orm.Page;
import com.jcommon.result.ResponseResult;
import com.jcommon.utils.BeanUtils;
import com.jcommon.utils.Cipher;
import com.valuelinkin.admin.common.utils.AppConstants;
import com.valuelinkin.admin.dao.entity.SysAdmin;
import com.valuelinkin.admin.dao.entity.SysAdminRole;
import com.valuelinkin.admin.dao.mapper.SysAdminMapper;
import com.valuelinkin.admin.dao.mapper.SysAdminRoleMapper;
import com.valuelinkin.admin.dto.SysAdminDTO;
import com.valuelinkin.admin.service.UidGenerator;
import com.valuelinkin.admin.service.core.SysAdminService;

import tk.mybatis.mapper.entity.Example;

@Service
public class SysAdminServiceImpl implements SysAdminService{

	@Autowired
	private SysAdminMapper sysAdminMapper;
	
	@Autowired
	private SysAdminRoleMapper sysAdminRoleMapper;
	
	@Autowired
	private UidGenerator uidGenerator;

	public ResponseResult findPage(SysAdminDTO dto) {
		ResponseResult result = new ResponseResult();
		
		SysAdmin entity = new SysAdmin();
		BeanUtils.copyProperties(entity, dto);
		
		Page page = new Page();
		page.setPageNo(dto.getPageNo());
		page.setPageSize(dto.getPageSize());
		page.setTotalCount(sysAdminMapper.selectCount(entity));
		
		Example example = new Example(SysAdmin.class);
		//example.createCriteria().andEqualTo("refId", rowNews.getRefId());
		//example.setOrderByClause("nws_Publish_Date desc");
		
		List<SysAdmin> list = sysAdminMapper.selectByExampleAndRowBounds(example, new RowBounds(page.getFirst(), page.getPageSize()));
		
		page.setResult(list);
		result.setData(page);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
		
	}	
	
	public ResponseResult getById(Long id) throws BusinessException{
		ResponseResult result = new ResponseResult();
		SysAdmin entity = sysAdminMapper.selectByPrimaryKey(id);

		SysAdminRole sysAdminRole = new SysAdminRole();
		sysAdminRole.setAdmId(entity.getAdmId());
		sysAdminRole = sysAdminRoleMapper.selectOne(sysAdminRole);

		SysAdminDTO adminDTO = new SysAdminDTO();
		BeanUtils.copyProperties(adminDTO,entity);
		if (sysAdminRole!=null){
			adminDTO.setRleId(sysAdminRole.getRleId());
		}

		result.setData(adminDTO);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}

	
	/** 插入数据 */
	@Transactional
	public ResponseResult save(SysAdminDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		SysAdmin entity = new SysAdmin();
		BeanUtils.copyProperties(entity, dto);
		entity.setAdmId(uidGenerator.nextId());
		
		entity.setCreatedOn(new Date());
		entity.setModifiedOn(new Date());
		entity.setAdmIsPwdExpired(1);
		entity.setAdmPassword(Cipher.MD5(dto.getAdmPassword()));
		//entity.set
		
		sysAdminMapper.insertSelective(entity);
		
		SysAdminRole sysAdminRole = new SysAdminRole();
		sysAdminRole.setId(uidGenerator.nextId());
		sysAdminRole.setAdmId(entity.getAdmId());
		sysAdminRole.setRleId(dto.getRleId());
		sysAdminRoleMapper.insertSelective(sysAdminRole);
		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult deleteById(Long id) throws BusinessException{
		ResponseResult result = new ResponseResult();
		sysAdminMapper.deleteByPrimaryKey(id);
		
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult delete(SysAdminDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		
		SysAdmin entity = new SysAdmin();
		BeanUtils.copyProperties(entity, dto);
		
		sysAdminMapper.delete(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult deleteByIds(String ids) throws BusinessException{
		ResponseResult result = new ResponseResult();
		String[] items = ids.split(",");
		for (String str : items) {
			Long id = new Long(str);
			sysAdminMapper.deleteByPrimaryKey(id);
		}
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult update(SysAdminDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		SysAdmin entity = new SysAdmin();
		BeanUtils.copyProperties(entity, dto);
		sysAdminMapper.updateByPrimaryKeySelective(entity);

		SysAdminRole sysAdminRole = new SysAdminRole();

		sysAdminRole.setAdmId(entity.getAdmId());
		sysAdminRole = sysAdminRoleMapper.selectOne(sysAdminRole);
		if(sysAdminRole==null){
			sysAdminRole = new SysAdminRole();
			sysAdminRole.setId(uidGenerator.nextId());
			sysAdminRole.setAdmId(entity.getAdmId());
			sysAdminRole.setRleId(dto.getRleId());
			sysAdminRoleMapper.insertSelective(sysAdminRole);
		}else{
			sysAdminRole.setRleId(dto.getRleId());
			sysAdminRoleMapper.updateByPrimaryKey(sysAdminRole);
		}

		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	public ResponseResult findByExample(SysAdminDTO dto) throws BusinessException {
		ResponseResult result = new ResponseResult();
		
		SysAdmin entity = new SysAdmin();
		BeanUtils.copyProperties(entity, dto);
		
		List list =sysAdminMapper.select(entity);
		
		result.setData(list);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	
	public ResponseResult getByExample(SysAdminDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		
		SysAdmin entity = new SysAdmin();
		BeanUtils.copyProperties(entity, dto);
		
		entity =sysAdminMapper.selectOne(entity);
		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
		
	}
	
	public void setSysAdminMapper(SysAdminMapper dao) {
		this.sysAdminMapper = dao;
	}

	/**
	 * @param dto
	 * @return
	 */
	@Transactional
	public void changePassword(SysAdminDTO dto)  {
		SysAdmin entity = sysAdminMapper.selectByPrimaryKey(dto.getAdmId());
		dto.setAdmPassword(Cipher.MD5(dto.getAdmPassword()));
		dto.setNewPassword(Cipher.MD5(dto.getNewPassword()));
		if (!entity.getAdmPassword().equalsIgnoreCase(dto.getAdmPassword())) {
			throw new BusinessException("info.usr_password");
		}
		entity.setAdmPassword(dto.getNewPassword());
		// 修改
		sysAdminMapper.updateByPrimaryKeySelective(entity);
	}

	@Transactional
	public void resetPassword(SysAdminDTO dto)  {
		SysAdmin entity = sysAdminMapper.selectByPrimaryKey(dto.getAdmLoginName());
		dto.setAdmPassword(Cipher.MD5(dto.getAdmPassword()));
		entity.setAdmPassword(dto.getAdmPassword());
		// 修改
		sysAdminMapper.updateByPrimaryKeySelective(entity);
	}

	/**
	 * 登录
	 * 
	 * @return
	 */
	public SysAdminDTO authrize(SysAdminDTO dto) {
		Date now = new Date();
		SysAdmin admin = new SysAdmin();
		admin.setAdmLoginName(dto.getAdmLoginName());
		admin =sysAdminMapper.selectOne(admin);
		// 账户在系统中不存在
		if (admin == null) {
			throw new BusinessException("info.username_not_exist");
		}
		// 密码错误
		if (!admin.getAdmPassword().equalsIgnoreCase(dto.getAdmPassword())) {
			throw new BusinessException("info.usr_password");
		}
		// 账户被停用
		if (AppConstants.STATUS_SUSPENDED.equals(admin.getAdmStatus())) {
			throw new BusinessException("info.usr_suspended");
		}

		 SysAdminRole role = new SysAdminRole();
		role.setAdmId(admin.getAdmId());
		role = sysAdminRoleMapper.selectOne(role);
		 if(role==null){
		 throw new BusinessException("info.user_no_permission");
		 }
		 dto.setRleId(role.getRleId());
		dto.setAdmDisplayName(admin.getAdmDisplayName());
		dto.setAdmStatus(admin.getAdmStatus());
		//dto.setAdmIsPasswordExpired(admin.getAdmIsPasswordExpired());
		return dto;
	}
}
