package com.valuelinkin.admin.dao.mapper;


import org.apache.ibatis.annotations.Mapper;

import com.valuelinkin.admin.dao.entity.Answer;
import com.valuelinkin.admin.dao.BaseMapper;

@Mapper
public interface AnswerMapper extends BaseMapper<Answer>{
	

}
