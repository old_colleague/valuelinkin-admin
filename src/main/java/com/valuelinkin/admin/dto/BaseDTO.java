/**
 * 文件名：BaseDTO.java
 * 公司名：深圳弘金信息科技有限公司
 * 版权所有：Copyright © 2015 Shenzhen Holborn Information Technologies Co..Ltd, Inc. All rights reserved.
 * 作者：Nail.zhang
 * 时间：2017年03月18日
 */
package com.valuelinkin.admin.dto;

import java.io.Serializable;

/**
 * 
 * @author Nail.zhang
 *
 */
public class BaseDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7178617021105026631L;
	
	private int pageNo = 1;// 当前页
	private int pageSize = 10;// 每一页大小
	
	private String orderBy;
	
	private String accessToken;

	public int getPageNo() {
		return pageNo;
	}

	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
	
	public int getFirst() {
		return ((pageNo - 1) * pageSize) ;
	}

}
