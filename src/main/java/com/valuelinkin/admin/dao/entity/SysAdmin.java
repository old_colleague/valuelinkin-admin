package com.valuelinkin.admin.dao.entity;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 〈功能详细描述〉
 *
 * @author Nail.zhang
 * @version [V0.1, Jan 9, 2018]
 */

@Table(name="sys_admin")
public class SysAdmin implements java.io.Serializable{
	private static final long serialVersionUID = 5454155825314635342L;
	

	//columns START
		@Id
		@Column(name = "adm_id")
		private Long admId;
		private java.lang.String admLoginName;
		private java.lang.String admDisplayName;
		private java.lang.String admPassword;
		private Integer admIsPwdExpired;
		private Integer admStatus;
		private java.lang.String admRemark;
		private java.util.Date createdOn;
		private java.util.Date modifiedOn;
	//columns END


	public SysAdmin(){
	}

	public SysAdmin(
		Long admId
	){
		this.admId = admId;
	}

	

	public void setAdmId(Long value) {
		this.admId = value;
	}
	
	public Long getAdmId() {
		return this.admId;
	}
	
	public java.lang.String getAdmLoginName() {
		return this.admLoginName;
	}
	
	public void setAdmLoginName(java.lang.String value) {
		this.admLoginName = value;
	}
	
	public java.lang.String getAdmDisplayName() {
		return this.admDisplayName;
	}
	
	public void setAdmDisplayName(java.lang.String value) {
		this.admDisplayName = value;
	}
	
	public java.lang.String getAdmPassword() {
		return this.admPassword;
	}
	
	public void setAdmPassword(java.lang.String value) {
		this.admPassword = value;
	}
	
	public Integer getAdmIsPwdExpired() {
		return this.admIsPwdExpired;
	}
	
	public void setAdmIsPwdExpired(Integer value) {
		this.admIsPwdExpired = value;
	}
	
	public Integer getAdmStatus() {
		return this.admStatus;
	}
	
	public void setAdmStatus(Integer value) {
		this.admStatus = value;
	}
	
	public java.lang.String getAdmRemark() {
		return this.admRemark;
	}
	
	public void setAdmRemark(java.lang.String value) {
		this.admRemark = value;
	}
	
		
	public java.util.Date getCreatedOn() {
		return this.createdOn;
	}
	
	public void setCreatedOn(java.util.Date value) {
		this.createdOn = value;
	}
	
		
	public java.util.Date getModifiedOn() {
		return this.modifiedOn;
	}
	
	public void setModifiedOn(java.util.Date value) {
		this.modifiedOn = value;
	}
	

}

