package com.valuelinkin.admin.service.core.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jcommon.orm.Page;
import com.jcommon.exception.BusinessException;
import com.jcommon.utils.BeanUtils;
import com.jcommon.result.ResponseResult;

import com.valuelinkin.admin.dao.entity.StockFinancialData;
import com.valuelinkin.admin.dto.StockFinancialDataDTO;
import com.valuelinkin.admin.dao.mapper.StockFinancialDataMapper;
import com.valuelinkin.admin.service.core.StockFinancialDataService;
import com.valuelinkin.admin.service.UidGenerator;
import com.valuelinkin.admin.common.utils.AppConstants;

import tk.mybatis.mapper.entity.Example;

@Service
public class StockFinancialDataServiceImpl implements StockFinancialDataService{

	@Autowired
	private StockFinancialDataMapper stockFinancialDataMapper;
	
	@Autowired
	private UidGenerator uidGenerator;

	public ResponseResult findPage(StockFinancialDataDTO dto) {
		ResponseResult result = new ResponseResult();
		
		StockFinancialData entity = new StockFinancialData();
		BeanUtils.copyProperties(entity, dto);
		
		Page page = new Page();
		page.setPageNo(dto.getPageNo());
		page.setPageSize(dto.getPageSize());
		page.setTotalCount(stockFinancialDataMapper.selectCount(entity));
		
		Example example = new Example(StockFinancialData.class);
		//example.createCriteria().andEqualTo("refId", rowNews.getRefId());
		//example.setOrderByClause("nws_Publish_Date desc");
		
		List<StockFinancialData> list = stockFinancialDataMapper.selectByExampleAndRowBounds(example, new RowBounds(page.getFirst(), page.getPageSize()));
		
		page.setResult(list);
		result.setData(page);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
		
	}	
	
	public ResponseResult getById(Long id) throws BusinessException{
		ResponseResult result = new ResponseResult();
		StockFinancialData entity = stockFinancialDataMapper.selectByPrimaryKey(id);
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}

	
	/** 插入数据 */
	@Transactional
	public ResponseResult save(StockFinancialDataDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		StockFinancialData entity = new StockFinancialData();
		BeanUtils.copyProperties(entity, dto);
		entity.setSfdId(uidGenerator.nextId());
		
		stockFinancialDataMapper.insertSelective(entity);
		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult deleteById(Long id) throws BusinessException{
		ResponseResult result = new ResponseResult();
		stockFinancialDataMapper.deleteByPrimaryKey(id);
		
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult delete(StockFinancialDataDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		
		StockFinancialData entity = new StockFinancialData();
		BeanUtils.copyProperties(entity, dto);
		
		stockFinancialDataMapper.delete(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult deleteByIds(String ids) throws BusinessException{
		ResponseResult result = new ResponseResult();
		String[] items = ids.split(",");
		for (String str : items) {
			Long id = new Long(str);
			stockFinancialDataMapper.deleteByPrimaryKey(id);
		}
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult update(StockFinancialDataDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		StockFinancialData entity = new StockFinancialData();
		BeanUtils.copyProperties(entity, dto);
		stockFinancialDataMapper.updateByPrimaryKeySelective(entity);
		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	public ResponseResult findByExample(StockFinancialDataDTO dto) throws BusinessException {
		ResponseResult result = new ResponseResult();
		
		StockFinancialData entity = new StockFinancialData();
		BeanUtils.copyProperties(entity, dto);
		
		List list =stockFinancialDataMapper.select(entity);
		
		result.setData(list);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	
	public ResponseResult getByExample(StockFinancialDataDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		
		StockFinancialData entity = new StockFinancialData();
		BeanUtils.copyProperties(entity, dto);
		
		entity =stockFinancialDataMapper.selectOne(entity);
		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
		
	}
	
	public void setStockFinancialDataMapper(StockFinancialDataMapper dao) {
		this.stockFinancialDataMapper = dao;
	}
}
