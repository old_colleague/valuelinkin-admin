package com.valuelinkin.admin.service.core.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jcommon.orm.Page;
import com.jcommon.exception.BusinessException;
import com.jcommon.utils.BeanUtils;
import com.jcommon.result.ResponseResult;

import com.valuelinkin.admin.dao.entity.VerifyCode;
import com.valuelinkin.admin.dto.VerifyCodeDTO;
import com.valuelinkin.admin.dao.mapper.VerifyCodeMapper;
import com.valuelinkin.admin.service.core.VerifyCodeService;
import com.valuelinkin.admin.service.UidGenerator;
import com.valuelinkin.admin.common.utils.AppConstants;

import tk.mybatis.mapper.entity.Example;

@Service
public class VerifyCodeServiceImpl implements VerifyCodeService{

	@Autowired
	private VerifyCodeMapper verifyCodeMapper;
	
	@Autowired
	private UidGenerator uidGenerator;

	public ResponseResult findPage(VerifyCodeDTO dto) {
		ResponseResult result = new ResponseResult();
		
		VerifyCode entity = new VerifyCode();
		BeanUtils.copyProperties(entity, dto);
		
		Page page = new Page();
		page.setPageNo(dto.getPageNo());
		page.setPageSize(dto.getPageSize());
		page.setTotalCount(verifyCodeMapper.selectCount(entity));
		
		Example example = new Example(VerifyCode.class);
		//example.createCriteria().andEqualTo("refId", rowNews.getRefId());
		//example.setOrderByClause("nws_Publish_Date desc");
		
		List<VerifyCode> list = verifyCodeMapper.selectByExampleAndRowBounds(example, new RowBounds(page.getFirst(), page.getPageSize()));
		
		page.setResult(list);
		result.setData(page);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
		
	}	
	
	public ResponseResult getById(Long id) throws BusinessException{
		ResponseResult result = new ResponseResult();
		VerifyCode entity = verifyCodeMapper.selectByPrimaryKey(id);
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}

	
	/** 插入数据 */
	@Transactional
	public ResponseResult save(VerifyCodeDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		VerifyCode entity = new VerifyCode();
		BeanUtils.copyProperties(entity, dto);
		entity.setId(uidGenerator.nextId());
		
		verifyCodeMapper.insertSelective(entity);
		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult deleteById(Long id) throws BusinessException{
		ResponseResult result = new ResponseResult();
		verifyCodeMapper.deleteByPrimaryKey(id);
		
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult delete(VerifyCodeDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		
		VerifyCode entity = new VerifyCode();
		BeanUtils.copyProperties(entity, dto);
		
		verifyCodeMapper.delete(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult deleteByIds(String ids) throws BusinessException{
		ResponseResult result = new ResponseResult();
		String[] items = ids.split(",");
		for (String str : items) {
			Long id = new Long(str);
			verifyCodeMapper.deleteByPrimaryKey(id);
		}
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult update(VerifyCodeDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		VerifyCode entity = new VerifyCode();
		BeanUtils.copyProperties(entity, dto);
		verifyCodeMapper.updateByPrimaryKeySelective(entity);
		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	public ResponseResult findByExample(VerifyCodeDTO dto) throws BusinessException {
		ResponseResult result = new ResponseResult();
		
		VerifyCode entity = new VerifyCode();
		BeanUtils.copyProperties(entity, dto);
		
		List list =verifyCodeMapper.select(entity);
		
		result.setData(list);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	
	public ResponseResult getByExample(VerifyCodeDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		
		VerifyCode entity = new VerifyCode();
		BeanUtils.copyProperties(entity, dto);
		
		entity =verifyCodeMapper.selectOne(entity);
		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
		
	}
	
	public void setVerifyCodeMapper(VerifyCodeMapper dao) {
		this.verifyCodeMapper = dao;
	}
}
