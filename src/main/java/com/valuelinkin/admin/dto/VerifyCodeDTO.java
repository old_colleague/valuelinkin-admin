package com.valuelinkin.admin.dto;

import com.valuelinkin.admin.dto.BaseDTO;

public class VerifyCodeDTO extends BaseDTO {
    private static final long serialVersionUID = 3148176768559230877L;
    

	private Long id;
	private java.lang.String phoneNo;
	private java.lang.String vrfCode;
	private java.util.Date vrfExpireTime;
	private Integer vrfSendCount;
	private java.util.Date createdOn;
	private java.util.Date modifiedOn;

	
	public Long getId() {
		return this.id;
	}
	
	public void setId(Long value) {
		this.id = value;
	}
	
	public java.lang.String getPhoneNo() {
		return this.phoneNo;
	}
	
	public void setPhoneNo(java.lang.String value) {
		this.phoneNo = value;
	}
	
	public java.lang.String getVrfCode() {
		return this.vrfCode;
	}
	
	public void setVrfCode(java.lang.String value) {
		this.vrfCode = value;
	}
	
	public java.util.Date getVrfExpireTime() {
		return this.vrfExpireTime;
	}
	
	public void setVrfExpireTime(java.util.Date value) {
		this.vrfExpireTime = value;
	}
	
	public Integer getVrfSendCount() {
		return this.vrfSendCount;
	}
	
	public void setVrfSendCount(Integer value) {
		this.vrfSendCount = value;
	}
	
	public java.util.Date getCreatedOn() {
		return this.createdOn;
	}
	
	public void setCreatedOn(java.util.Date value) {
		this.createdOn = value;
	}
	
	public java.util.Date getModifiedOn() {
		return this.modifiedOn;
	}
	
	public void setModifiedOn(java.util.Date value) {
		this.modifiedOn = value;
	}
	
}

