package com.valuelinkin.admin.service.core.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jcommon.orm.Page;
import com.jcommon.exception.BusinessException;
import com.jcommon.utils.BeanUtils;
import com.jcommon.result.ResponseResult;

import com.valuelinkin.admin.dao.entity.SysAdminRole;
import com.valuelinkin.admin.dto.SysAdminRoleDTO;
import com.valuelinkin.admin.dao.mapper.SysAdminRoleMapper;
import com.valuelinkin.admin.service.core.SysAdminRoleService;
import com.valuelinkin.admin.service.UidGenerator;
import com.valuelinkin.admin.common.utils.AppConstants;

import tk.mybatis.mapper.entity.Example;

@Service
public class SysAdminRoleServiceImpl implements SysAdminRoleService{

	@Autowired
	private SysAdminRoleMapper sysAdminRoleMapper;
	
	@Autowired
	private UidGenerator uidGenerator;

	public ResponseResult findPage(SysAdminRoleDTO dto) {
		ResponseResult result = new ResponseResult();
		
		SysAdminRole entity = new SysAdminRole();
		BeanUtils.copyProperties(entity, dto);
		
		Page page = new Page();
		page.setPageNo(dto.getPageNo());
		page.setPageSize(dto.getPageSize());
		page.setTotalCount(sysAdminRoleMapper.selectCount(entity));
		
		Example example = new Example(SysAdminRole.class);
		//example.createCriteria().andEqualTo("refId", rowNews.getRefId());
		//example.setOrderByClause("nws_Publish_Date desc");
		
		List<SysAdminRole> list = sysAdminRoleMapper.selectByExampleAndRowBounds(example, new RowBounds(page.getFirst(), page.getPageSize()));
		
		page.setResult(list);
		result.setData(page);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
		
	}	
	
	public ResponseResult getById(Long id) throws BusinessException{
		ResponseResult result = new ResponseResult();
		SysAdminRole entity = sysAdminRoleMapper.selectByPrimaryKey(id);
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}

	
	/** 插入数据 */
	@Transactional
	public ResponseResult save(SysAdminRoleDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		SysAdminRole entity = new SysAdminRole();
		BeanUtils.copyProperties(entity, dto);
		entity.setId(uidGenerator.nextId());
		
		sysAdminRoleMapper.insertSelective(entity);
		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult deleteById(Long id) throws BusinessException{
		ResponseResult result = new ResponseResult();
		sysAdminRoleMapper.deleteByPrimaryKey(id);
		
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult delete(SysAdminRoleDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		
		SysAdminRole entity = new SysAdminRole();
		BeanUtils.copyProperties(entity, dto);
		
		sysAdminRoleMapper.delete(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult deleteByIds(String ids) throws BusinessException{
		ResponseResult result = new ResponseResult();
		String[] items = ids.split(",");
		for (String str : items) {
			Long id = new Long(str);
			sysAdminRoleMapper.deleteByPrimaryKey(id);
		}
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult update(SysAdminRoleDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		SysAdminRole entity = new SysAdminRole();
		BeanUtils.copyProperties(entity, dto);
		sysAdminRoleMapper.updateByPrimaryKeySelective(entity);
		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	public ResponseResult findByExample(SysAdminRoleDTO dto) throws BusinessException {
		ResponseResult result = new ResponseResult();
		
		SysAdminRole entity = new SysAdminRole();
		BeanUtils.copyProperties(entity, dto);
		
		List list =sysAdminRoleMapper.select(entity);
		
		result.setData(list);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	
	public ResponseResult getByExample(SysAdminRoleDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		
		SysAdminRole entity = new SysAdminRole();
		BeanUtils.copyProperties(entity, dto);
		
		entity =sysAdminRoleMapper.selectOne(entity);
		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
		
	}
	
	public void setSysAdminRoleMapper(SysAdminRoleMapper dao) {
		this.sysAdminRoleMapper = dao;
	}
}
