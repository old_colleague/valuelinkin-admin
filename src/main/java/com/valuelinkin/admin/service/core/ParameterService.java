package com.valuelinkin.admin.service.core;

import java.util.List;

import com.jcommon.orm.Page;
import com.jcommon.exception.BusinessException;
import com.jcommon.result.ResponseResult;

import com.valuelinkin.admin.dto.ParameterDTO;

public interface ParameterService {

	ResponseResult findPage(ParameterDTO dto) throws BusinessException;
	
	ResponseResult getById(Long id) throws BusinessException;

	ResponseResult save(ParameterDTO dto) throws BusinessException;
	
	ResponseResult delete(ParameterDTO dto) throws BusinessException ;
	
	ResponseResult deleteById(Long id) throws BusinessException ;
	
	ResponseResult update(ParameterDTO dto) throws BusinessException ;
	
	ResponseResult findByExample(ParameterDTO dto) throws BusinessException;
	
	ResponseResult getByExample(ParameterDTO dto) throws BusinessException;
	
	ResponseResult deleteByIds(String ids) throws BusinessException ;
	

}
