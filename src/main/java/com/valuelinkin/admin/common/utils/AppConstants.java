
package com.valuelinkin.admin.common.utils;

/**
 * @author Nail.zhang
 */
public class AppConstants {

    public static final String LOGIN_SESSION = "admin_login_session";

    public static final String LOGIN_USER_PERMISSION = "userPermissions";

    // 返回成功
    public static final int RETURN_SUCCESS_CODE = 0;

    public static final int RETURN_SUCCESS_CODE_INFO = 1;

    // 返回错误
    public static final int RETURN_FAIL_CODE = -1;

    public static final int RETURN_ERROR_CODE_INVALID_PARAMETERS = -3;// 参数错误

    public static final int RETURN_ERROR_CODE_CANNOT_ACCESS_THIS_API = -4;// 无访问次api的权限

    public static final int RETURN_ERROR_CODE_PARTNER_TOTAL_INVOKE_OVER_LIMIT = -5;// 合作商户每日总调用次数超过限制

    public static final int RETURN_ERROR_CODE_PARTNER_INVOKE_SINGLE_API_OVER_LIMIT = -6;// 合作商户每日单个api调用次数超过限制

    public static final int RETURN_ERROR_CODE_PARTNER_INVOKE_FREQUENCY_OVER_LIMIT = -7;// 合作商户调用频率超过限制

    public static final int RETURN_ERROR_CODE_INVALID_PARTNER = -8;// 无效的合作商户号

    public static final int RETURN_ERROR_CODE_VERIFY_SIGN_ERROR = -9;// 验证签名失败

    // Session 过期
    public static final int RETURN_SESSION_EXPIRED_CODE = -999;

    public static final String STATUS_ACTIVE = "ACTIVE";

    public static final String STATUS_ENDED = "ENDED";

    public static final String STATUS_PENDING = "PENDING";

    public static final String STATUS_COMPLETE = "COMPLETE";

    public static final String STATUS_OPEN = "OPEN";

    public static final String STATUS_SUSPENDED = "SUSPENDED";

}
