package com.valuelinkin.admin.web.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jcommon.result.ResponseResult;
import com.valuelinkin.admin.common.utils.AppConstants;
import com.valuelinkin.admin.dto.SysAdminDTO;
import com.valuelinkin.admin.dto.SysRoleDTO;
import com.valuelinkin.admin.service.core.SysAdminService;
import com.valuelinkin.admin.service.core.SysRoleService;

@Controller
@RequestMapping(value="/sysAdmin") 
public class SysAdminController{
	
	@Autowired
	private SysAdminService sysAdminService;
	
	@Autowired
	private SysRoleService sysRoleService;
	
	@RequestMapping(value="/index",method=RequestMethod.GET)
	public String index( Model model) throws Exception {
		
		return "/sysAdmin/list";
	}
	
	/** 
	 * 执行搜索 
	 **/
	@RequestMapping(value="",method=RequestMethod.GET)
	@ResponseBody
	public ResponseResult findPage(SysAdminDTO dto) {
		
		ResponseResult result = new ResponseResult();
		result = this.sysAdminService.findPage(dto);
		return result;
	}
	
	
	/** 
	 * 查看对象
	 **/
	@RequestMapping(value="/view/{id}",method=RequestMethod.GET)
	public String getById(@PathVariable Long id, Model model) {
		ResponseResult result = new ResponseResult();
		result = sysAdminService.getById(id);
		
		model.addAttribute("result", result);
		return "/sysAdmin/view";
		
	}
	

	/** 
	 * 进入新增页面
	 **/
	@RequestMapping(value="/create", method=RequestMethod.GET)
	public String create(SysAdminDTO dto, Model model) throws Exception {
		model.addAttribute("sysAdmin", dto);
		SysRoleDTO sysRoleDTO = new SysRoleDTO();
		sysRoleDTO.setRleStatus(0);
		ResponseResult roleResult = sysRoleService.findByExample(sysRoleDTO);
		
		model.addAttribute("sysAdmin", dto);
		model.addAttribute("roleList", roleResult.getData());
		
		return "/sysAdmin/create";
	}
	
	/** 
	 * 保存新增对象
	 **/
	@RequestMapping(value="",method=RequestMethod.POST)
	@ResponseBody
	public ResponseResult save(SysAdminDTO dto) {
		ResponseResult result = null;
		result = sysAdminService.save(dto);
		return result;
	}
	
	
	/**
	 * 进入更新页面
	 **/
	@RequestMapping(value="/edit/{id}", method=RequestMethod.GET)
	public String edit(@PathVariable Long id, Model model) throws Exception {

		ResponseResult result  = sysAdminService.getById(id);
		model.addAttribute("result", result);

		SysRoleDTO sysRoleDTO = new SysRoleDTO();
		sysRoleDTO.setRleStatus(0);
		ResponseResult roleResult = sysRoleService.findByExample(sysRoleDTO);
		model.addAttribute("roleList", roleResult.getData());
		
		return "/sysAdmin/edit";
	}
	
	/**
	 * 保存更新对象
	 **/
	@RequestMapping(value="/{id}", method=RequestMethod.PUT)
	@ResponseBody
	public ResponseResult update(@PathVariable Long id,SysAdminDTO dto) throws Exception {
		ResponseResult result = new ResponseResult();
		dto.setAdmId(id);
		result = sysAdminService.update(dto);
		return result;
	}
	
	
	@RequestMapping(value="/changePassword")
	public String changePassword(HttpSession session,SysAdminDTO dto, Model model) throws Exception {
		SysAdminDTO adminDTO = (SysAdminDTO)session.getAttribute(AppConstants.LOGIN_SESSION);
		ResponseResult result = sysAdminService.getByExample(adminDTO);
		model.addAttribute("result", result);
		return "/sysAdmin/changepassword";
	}
	
	@RequestMapping(value="/savePassword")
	@ResponseBody
	public ResponseResult savePassword(HttpServletRequest request,HttpServletResponse response, SysAdminDTO dto) throws Exception {
		ResponseResult result = new ResponseResult();		
		sysAdminService.changePassword(dto);		
		result.setErrorCode(0);
		result.setData(dto);
		return result;
	}
	
	@RequestMapping(value="/resetPwd")
	@ResponseBody
	public ResponseResult resetPwd(HttpServletRequest request,HttpServletResponse response, SysAdminDTO dto) throws Exception {
		ResponseResult result = new ResponseResult();		
		sysAdminService.resetPassword(dto);		
		result.setErrorCode(0);
		result.setData(dto);
		
		return result;
	}
	
	
	/**
	 *删除多个对象
	 **/
	@RequestMapping(value="/{ids}", method=RequestMethod.DELETE)
	@ResponseBody
	public ResponseResult delete(@PathVariable String ids) {
		
		ResponseResult result = null;
		result = sysAdminService.deleteByIds(ids);
		return result;
		
	}
	
	public void setSysAdminService(SysAdminService service) {
		this.sysAdminService = service;
	}
	
}
