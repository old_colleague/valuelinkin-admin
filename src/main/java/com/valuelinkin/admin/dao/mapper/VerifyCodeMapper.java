package com.valuelinkin.admin.dao.mapper;


import org.apache.ibatis.annotations.Mapper;

import com.valuelinkin.admin.dao.entity.VerifyCode;
import com.valuelinkin.admin.dao.BaseMapper;

@Mapper
public interface VerifyCodeMapper extends BaseMapper<VerifyCode>{
	

}
