package com.valuelinkin.admin.service.core.impl;

import com.jcommon.exception.BusinessException;
import com.jcommon.result.ResponseResult;
import com.valuelinkin.admin.common.utils.AppConstants;
import com.valuelinkin.admin.dao.entity.AppMessage;
import com.valuelinkin.admin.dao.mapper.AppMessageMapper;
import com.valuelinkin.admin.dao.mapper.UserMapper;
import com.valuelinkin.admin.dto.AppMessageDTO;
import com.valuelinkin.admin.service.UidGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * @atuhor: nail.zhang
 * @description:
 * @date: 2019/3/19 8:22
 */
@Service
public class BatchSaveAppMessageService {

    @Autowired
    private AppMessageMapper appMessageMapper;

    @Autowired
    private UidGenerator uidGenerator;

    @Autowired
    private UserMapper userMapper;


    @Transactional
    public ResponseResult batchSave(List<Long> userIdList, AppMessageDTO dto) throws BusinessException {
        ResponseResult result = new ResponseResult();

        for (Long usrId : userIdList) {

            AppMessage entity = new AppMessage();
            entity.setUsrId(usrId);
            entity.setMsgTitle(dto.getMsgTitle());
            entity.setMsgContent(dto.getMsgContent());
            entity.setMsgActionLink("");
            entity.setMsgActionType("");
            entity.setMsgIsRead(0);
            entity.setMsgType("1");
            entity.setMsgDate(new Date());
            entity.setMsgId(uidGenerator.nextId());

            appMessageMapper.insertSelective(entity);
        }

        result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
        return result;
    }
}
