package com.valuelinkin.admin.dto;

import com.valuelinkin.admin.dto.BaseDTO;

public class AppMessageDTO extends BaseDTO {
    private static final long serialVersionUID = 3148176768559230877L;
    

	private Long msgId;
	private Long usrId;
	private java.lang.String msgType;
	private java.lang.String msgTitle;
	private java.lang.String msgContent;
	private Integer msgIsRead;
	private java.lang.String msgActionLink;
	private java.lang.String msgActionType;

	private String usrRole;

	public String getUsrRole() {
		return usrRole;
	}

	public void setUsrRole(String usrRole) {
		this.usrRole = usrRole;
	}

	public Long getMsgId() {
		return this.msgId;
	}
	
	public void setMsgId(Long value) {
		this.msgId = value;
	}
	
	public Long getUsrId() {
		return this.usrId;
	}
	
	public void setUsrId(Long value) {
		this.usrId = value;
	}
	
	public java.lang.String getMsgType() {
		return this.msgType;
	}
	
	public void setMsgType(java.lang.String value) {
		this.msgType = value;
	}
	
	public java.lang.String getMsgTitle() {
		return this.msgTitle;
	}
	
	public void setMsgTitle(java.lang.String value) {
		this.msgTitle = value;
	}
	
	public java.lang.String getMsgContent() {
		return this.msgContent;
	}
	
	public void setMsgContent(java.lang.String value) {
		this.msgContent = value;
	}
	
	public Integer getMsgIsRead() {
		return this.msgIsRead;
	}
	
	public void setMsgIsRead(Integer value) {
		this.msgIsRead = value;
	}
	
	public java.lang.String getMsgActionLink() {
		return this.msgActionLink;
	}
	
	public void setMsgActionLink(java.lang.String value) {
		this.msgActionLink = value;
	}
	
	public java.lang.String getMsgActionType() {
		return this.msgActionType;
	}
	
	public void setMsgActionType(java.lang.String value) {
		this.msgActionType = value;
	}
	
}

