package com.valuelinkin.admin.service.core;

import com.jcommon.exception.BusinessException;
import com.jcommon.result.ResponseResult;
import com.valuelinkin.admin.dto.SysAdminDTO;

public interface SysAdminService {

	ResponseResult findPage(SysAdminDTO dto) throws BusinessException;
	
	ResponseResult getById(Long id) throws BusinessException;

	ResponseResult save(SysAdminDTO dto) throws BusinessException;
	
	ResponseResult delete(SysAdminDTO dto) throws BusinessException ;
	
	ResponseResult deleteById(Long id) throws BusinessException ;
	
	ResponseResult update(SysAdminDTO dto) throws BusinessException ;
	
	ResponseResult findByExample(SysAdminDTO dto) throws BusinessException;
	
	ResponseResult getByExample(SysAdminDTO dto) throws BusinessException;
	
	ResponseResult deleteByIds(String ids) throws BusinessException ;
	
	
	SysAdminDTO authrize(SysAdminDTO dto);
	
	void resetPassword(SysAdminDTO dto) ;
	
	void changePassword(SysAdminDTO dto);
	

}
