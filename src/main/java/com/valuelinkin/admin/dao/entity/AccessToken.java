package com.valuelinkin.admin.dao.entity;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 〈功能详细描述〉
 *
 * @author Nail.zhang
 * @version [V0.1, Jan 9, 2018]
 */

@Table(name="vln_access_token")
public class AccessToken implements java.io.Serializable{
	private static final long serialVersionUID = 5454155825314635342L;
	

	//columns START
		@Id
		@Column(name = "tkn_id")
		private Long tknId;
		private java.lang.String tknLoginId;
		private java.lang.String tknToken;
		private java.util.Date tknCreateTime;
		private java.util.Date tknExpiredTime;
		private java.lang.String tknSourceId;
		private java.lang.String tknTerminalId;
		private java.lang.String tknRefreshToken;
	//columns END


	public AccessToken(){
	}

	public AccessToken(
		Long tknId
	){
		this.tknId = tknId;
	}

	

	public void setTknId(Long value) {
		this.tknId = value;
	}
	
	public Long getTknId() {
		return this.tknId;
	}
	
	public java.lang.String getTknLoginId() {
		return this.tknLoginId;
	}
	
	public void setTknLoginId(java.lang.String value) {
		this.tknLoginId = value;
	}
	
	public java.lang.String getTknToken() {
		return this.tknToken;
	}
	
	public void setTknToken(java.lang.String value) {
		this.tknToken = value;
	}
	
		
	public java.util.Date getTknCreateTime() {
		return this.tknCreateTime;
	}
	
	public void setTknCreateTime(java.util.Date value) {
		this.tknCreateTime = value;
	}
	
		
	public java.util.Date getTknExpiredTime() {
		return this.tknExpiredTime;
	}
	
	public void setTknExpiredTime(java.util.Date value) {
		this.tknExpiredTime = value;
	}
	
	public java.lang.String getTknSourceId() {
		return this.tknSourceId;
	}
	
	public void setTknSourceId(java.lang.String value) {
		this.tknSourceId = value;
	}
	
	public java.lang.String getTknTerminalId() {
		return this.tknTerminalId;
	}
	
	public void setTknTerminalId(java.lang.String value) {
		this.tknTerminalId = value;
	}
	
	public java.lang.String getTknRefreshToken() {
		return this.tknRefreshToken;
	}
	
	public void setTknRefreshToken(java.lang.String value) {
		this.tknRefreshToken = value;
	}
	

}

