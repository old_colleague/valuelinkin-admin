package com.valuelinkin.admin.dao.mapper;


import org.apache.ibatis.annotations.Mapper;

import com.valuelinkin.admin.dao.entity.StockFinancialDataDetail;
import com.valuelinkin.admin.dao.BaseMapper;

@Mapper
public interface StockFinancialDataDetailMapper extends BaseMapper<StockFinancialDataDetail>{
	

}
