package com.valuelinkin.admin.dto;

import com.valuelinkin.admin.dto.BaseDTO;

import java.util.Date;

public class QuestionDTO extends BaseDTO {
    private static final long serialVersionUID = 3148176768559230877L;


    private Long qsnId;
    private Long nwsId;
    private Long usrId;
    private java.lang.String qsnContent;
    private Integer qsnStatus;

    private String keywords;

    private Date startDate;
    private Date endDate;

    private String ansContent;
    private Long ansReplier;

    public String getAnsContent() {
        return ansContent;
    }

    public void setAnsContent(String ansContent) {
        this.ansContent = ansContent;
    }

    public Long getAnsReplier() {
        return ansReplier;
    }

    public void setAnsReplier(Long ansReplier) {
        this.ansReplier = ansReplier;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Long getQsnId() {
        return this.qsnId;
    }

    public void setQsnId(Long value) {
        this.qsnId = value;
    }

    public Long getNwsId() {
        return this.nwsId;
    }

    public void setNwsId(Long value) {
        this.nwsId = value;
    }

    public Long getUsrId() {
        return this.usrId;
    }

    public void setUsrId(Long value) {
        this.usrId = value;
    }

    public java.lang.String getQsnContent() {
        return this.qsnContent;
    }

    public void setQsnContent(java.lang.String value) {
        this.qsnContent = value;
    }

    public Integer getQsnStatus() {
        return this.qsnStatus;
    }

    public void setQsnStatus(Integer value) {
        this.qsnStatus = value;
    }

}

