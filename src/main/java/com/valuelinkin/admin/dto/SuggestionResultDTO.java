package com.valuelinkin.admin.dto;
import lombok.Data;

@Data
public class SuggestionResultDTO {

	private Long sgsId;
	private Long usrId;
	private String sgsContent;
	private String sgsStatus;
	private String sgsReply;
	private java.util.Date sgsReplyOn;
	private String sgsReplyBy;
	private java.util.Date createdOn;
	private java.util.Date modifiedOn;

	private String usrNickName;
	private String usrPhone;
	
}

