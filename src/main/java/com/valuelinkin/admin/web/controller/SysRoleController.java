package com.valuelinkin.admin.web.controller;

import java.util.Collection;
import java.util.List;

import com.jcommon.orm.Page;

import com.valuelinkin.admin.dto.PrivilegeListDTO;
import com.valuelinkin.admin.service.core.SysPrivilegeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.ui.Model;

import com.valuelinkin.admin.dto.SysRoleDTO;
import com.valuelinkin.admin.service.core.SysRoleService;

import com.jcommon.result.ResponseResult;
import com.jcommon.utils.BeanUtils;

@Controller
@RequestMapping(value="/sysRole") 
public class SysRoleController{
	
	@Autowired
	private SysRoleService sysRoleService;

	@Autowired
	private SysPrivilegeService sysPrivilegeService;
	
	@RequestMapping(value="/index",method=RequestMethod.GET)
	public String index( Model model) throws Exception {
		
		return "/sysRole/list";
	}
	
	/** 
	 * 执行搜索 
	 **/
	@RequestMapping(value="",method=RequestMethod.GET)
	@ResponseBody
	public ResponseResult findPage(SysRoleDTO dto) {
		
		ResponseResult result = new ResponseResult();
		result = this.sysRoleService.findPage(dto);
		return result;
	}
	
	
	/** 
	 * 查看对象
	 **/
	@RequestMapping(value="/view/{id}",method=RequestMethod.GET)
	public String getById(@PathVariable Long id, Model model) {
		ResponseResult result = new ResponseResult();
		result = sysRoleService.getById(id);
		
		model.addAttribute("result", result);
		return "/sysRole/view";
		
	}
	

	/** 
	 * 进入新增页面
	 **/
	@RequestMapping(value="/create", method=RequestMethod.GET)
	public String create(SysRoleDTO dto, Model model) throws Exception {
		ResponseResult<Collection<PrivilegeListDTO>> result = sysPrivilegeService.findAllPvlList();
		model.addAttribute("sysRole", dto);

		model.addAttribute("allPvlList",result.getData());
		return "/sysRole/create";
	}
	
	/** 
	 * 保存新增对象
	 **/
	@RequestMapping(value="",method=RequestMethod.POST)
	@ResponseBody
	public ResponseResult save(SysRoleDTO dto) {
		ResponseResult result = null;
		result = sysRoleService.save(dto);
		return result;
	}
	
	
	/**
	 * 进入更新页面
	 **/
	@RequestMapping(value="/edit/{id}", method=RequestMethod.GET)
	public String edit(@PathVariable Long id, Model model) throws Exception {

		ResponseResult result  = sysRoleService.getById(id);
		model.addAttribute("result", result);
		ResponseResult<Collection<PrivilegeListDTO>> result2 = sysPrivilegeService.findRolePvlToEdit(id);
		model.addAttribute("allPvlList",result2.getData());
		
		return "/sysRole/edit";
	}
	
	/**
	 * 保存更新对象
	 **/
	@RequestMapping(value="/{id}", method=RequestMethod.PUT)
	@ResponseBody
	public ResponseResult update(@PathVariable Long id,SysRoleDTO dto) throws Exception {
		ResponseResult result = new ResponseResult();
		dto.setRleId(id);
		result = sysRoleService.update(dto);
		return result;
	}
	
	
	/**
	 *删除多个对象
	 **/
	@RequestMapping(value="/{ids}", method=RequestMethod.DELETE)
	@ResponseBody
	public ResponseResult delete(@PathVariable String ids) {
		
		ResponseResult result = null;
		result = sysRoleService.deleteByIds(ids);
		return result;
		
	}
	
	public void setSysRoleService(SysRoleService service) {
		this.sysRoleService = service;
	}
	
}
