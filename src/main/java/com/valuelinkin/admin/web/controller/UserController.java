package com.valuelinkin.admin.web.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jcommon.result.ResponseResult;
import com.valuelinkin.admin.common.utils.AppConstants;
import com.valuelinkin.admin.dto.SysAdminDTO;
import com.valuelinkin.admin.dto.UserDTO;
import com.valuelinkin.admin.service.core.UserService;

@Controller
@RequestMapping(value="/user") 
public class UserController{
	
	@Autowired
	private UserService userService;
	
	@RequestMapping(value="/index",method=RequestMethod.GET)
	public String index( Model model) throws Exception {
		
		return "/user/list";
	}
	
	/** 
	 * 执行搜索 
	 **/
	@RequestMapping(value="",method=RequestMethod.GET)
	@ResponseBody
	public ResponseResult findPage(UserDTO dto) {
		
		ResponseResult result = new ResponseResult();
		result = this.userService.findPage(dto);
		return result;
	}
	
	
	/** 
	 * 查看对象
	 **/
	@RequestMapping(value="/view/{id}",method=RequestMethod.GET)
	public String getById(@PathVariable Long id, Model model) {
		ResponseResult result = new ResponseResult();
		result = userService.getById(id);
		
		model.addAttribute("result", result);
		return "/user/view";
		
	}
	

	/** 
	 * 进入新增页面
	 **/
	@RequestMapping(value="/create", method=RequestMethod.GET)
	public String create(UserDTO dto, Model model) throws Exception {
		model.addAttribute("user", dto);
		return "/user/create";
	}
	
	/** 
	 * 保存新增对象
	 **/
	@RequestMapping(value="",method=RequestMethod.POST)
	@ResponseBody
	public ResponseResult save(UserDTO dto,HttpSession session) {
		ResponseResult result = null;
		SysAdminDTO adminDTO = (SysAdminDTO)session.getAttribute(AppConstants.LOGIN_SESSION);
		dto.setCreateBy(adminDTO.getAdmLoginName());
		result = userService.save(dto);
		return result;
	}
	
	@RequestMapping(value="/resetPwd/{id}", method=RequestMethod.GET)
	public String toResetPwdPage(@PathVariable Long id, Model model) throws Exception {
		ResponseResult result  = userService.getById(id);
		model.addAttribute("result", result);
		return "/user/resetPwd";
	}
	
	@RequestMapping(value="/savePassword", method=RequestMethod.PUT)
	@ResponseBody
	public ResponseResult savePassword(UserDTO dto, Model model) throws Exception {
		ResponseResult result  = userService.resetPwd(dto);
		model.addAttribute("result", result);
		return result;
	}
	
	
	/**
	 * 进入更新页面
	 **/
	@RequestMapping(value="/edit/{id}", method=RequestMethod.GET)
	public String edit(@PathVariable Long id, Model model) throws Exception {

		ResponseResult result  = userService.getById(id);
		model.addAttribute("result", result);
		
		return "/user/edit";
	}
	
	/**
	 * 保存更新对象
	 **/
	@RequestMapping(value="/{id}", method=RequestMethod.PUT)
	@ResponseBody
	public ResponseResult update(@PathVariable Long id,UserDTO dto) throws Exception {
		ResponseResult result = new ResponseResult();
		dto.setUsrId(id);
		result = userService.update(dto);
		return result;
	}
	
	
	/**
	 *删除多个对象
	 **/
	@RequestMapping(value="/{ids}", method=RequestMethod.DELETE)
	@ResponseBody
	public ResponseResult delete(@PathVariable String ids) {
		
		ResponseResult result = null;
		result = userService.deleteByIds(ids);
		return result;
		
	}
	
	public void setUserService(UserService service) {
		this.userService = service;
	}
	
}
