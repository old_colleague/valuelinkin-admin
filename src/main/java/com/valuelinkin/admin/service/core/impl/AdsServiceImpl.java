package com.valuelinkin.admin.service.core.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jcommon.orm.Page;
import com.jcommon.exception.BusinessException;
import com.jcommon.utils.BeanUtils;
import com.jcommon.result.ResponseResult;

import com.valuelinkin.admin.dao.entity.Ads;
import com.valuelinkin.admin.dto.AdsDTO;
import com.valuelinkin.admin.dao.mapper.AdsMapper;
import com.valuelinkin.admin.service.core.AdsService;
import com.valuelinkin.admin.service.UidGenerator;
import com.valuelinkin.admin.common.utils.AppConstants;

import tk.mybatis.mapper.entity.Example;

@Service
public class AdsServiceImpl implements AdsService{

	@Autowired
	private AdsMapper adsMapper;
	
	@Autowired
	private UidGenerator uidGenerator;

	public ResponseResult findPage(AdsDTO dto) {
		ResponseResult result = new ResponseResult();
		
		Ads entity = new Ads();
		BeanUtils.copyProperties(entity, dto);
		
		Page page = new Page();
		page.setPageNo(dto.getPageNo());
		page.setPageSize(dto.getPageSize());
		page.setTotalCount(adsMapper.selectCount(entity));
		
		Example example = new Example(Ads.class);
		//example.createCriteria().andEqualTo("refId", rowNews.getRefId());
		//example.setOrderByClause("nws_Publish_Date desc");
		
		List<Ads> list = adsMapper.selectByExampleAndRowBounds(example, new RowBounds(page.getFirst(), page.getPageSize()));
		
		page.setResult(list);
		result.setData(page);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
		
	}	
	
	public ResponseResult getById(Long id) throws BusinessException{
		ResponseResult result = new ResponseResult();
		Ads entity = adsMapper.selectByPrimaryKey(id);
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}

	
	/** 插入数据 */
	@Transactional
	public ResponseResult save(AdsDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		Ads entity = new Ads();
		BeanUtils.copyProperties(entity, dto);
		entity.setAdsId(uidGenerator.nextId());
		Date now = new Date();
		entity.setCreatedOn(now);
		entity.setModifiedOn(now);
		//entity.setAdsActionType("HREF");
		entity.setAdsTerminalType("iOS");
		
		adsMapper.insertSelective(entity);
		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult deleteById(Long id) throws BusinessException{
		ResponseResult result = new ResponseResult();
		adsMapper.deleteByPrimaryKey(id);
		
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult delete(AdsDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		
		Ads entity = new Ads();
		BeanUtils.copyProperties(entity, dto);
		
		adsMapper.delete(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult deleteByIds(String ids) throws BusinessException{
		ResponseResult result = new ResponseResult();
		String[] items = ids.split(",");
		for (String str : items) {
			Long id = new Long(str);
			adsMapper.deleteByPrimaryKey(id);
		}
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult update(AdsDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		Ads entity = new Ads();
		BeanUtils.copyProperties(entity, dto);
		adsMapper.updateByPrimaryKeySelective(entity);
		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	public ResponseResult findByExample(AdsDTO dto) throws BusinessException {
		ResponseResult result = new ResponseResult();
		
		Ads entity = new Ads();
		BeanUtils.copyProperties(entity, dto);
		
		List list =adsMapper.select(entity);
		
		result.setData(list);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	
	public ResponseResult getByExample(AdsDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		
		Ads entity = new Ads();
		BeanUtils.copyProperties(entity, dto);
		
		entity =adsMapper.selectOne(entity);
		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
		
	}
	
	public void setAdsMapper(AdsMapper dao) {
		this.adsMapper = dao;
	}
}
