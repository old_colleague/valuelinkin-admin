package com.valuelinkin.admin.dao.mapper;


import org.apache.ibatis.annotations.Mapper;

import com.valuelinkin.admin.dao.entity.StockWatchlist;
import com.valuelinkin.admin.dao.BaseMapper;

@Mapper
public interface StockWatchlistMapper extends BaseMapper<StockWatchlist>{
	

}
