package com.valuelinkin.admin.dao.mapper;


import org.apache.ibatis.annotations.Mapper;

import com.valuelinkin.admin.dao.entity.SysRolePermission;
import com.valuelinkin.admin.dao.BaseMapper;

@Mapper
public interface SysRolePermissionMapper extends BaseMapper<SysRolePermission>{
	

}
