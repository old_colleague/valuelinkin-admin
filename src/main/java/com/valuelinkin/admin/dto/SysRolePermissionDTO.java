package com.valuelinkin.admin.dto;

import com.valuelinkin.admin.dto.BaseDTO;

public class SysRolePermissionDTO extends BaseDTO {
    private static final long serialVersionUID = 3148176768559230877L;
    

	private Long id;
	private Long pvlId;
	private Long rleId;

	
	public Long getId() {
		return this.id;
	}
	
	public void setId(Long value) {
		this.id = value;
	}
	
	public Long getPvlId() {
		return this.pvlId;
	}
	
	public void setPvlId(Long value) {
		this.pvlId = value;
	}
	
	public Long getRleId() {
		return this.rleId;
	}
	
	public void setRleId(Long value) {
		this.rleId = value;
	}
	
}

