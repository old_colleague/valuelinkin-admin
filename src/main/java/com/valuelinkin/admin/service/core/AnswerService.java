package com.valuelinkin.admin.service.core;

import java.util.List;

import com.jcommon.orm.Page;
import com.jcommon.exception.BusinessException;
import com.jcommon.result.ResponseResult;

import com.valuelinkin.admin.dto.AnswerDTO;

public interface AnswerService {

	ResponseResult findPage(AnswerDTO dto) throws BusinessException;
	
	ResponseResult getById(Long id) throws BusinessException;

	ResponseResult save(AnswerDTO dto) throws BusinessException;
	
	ResponseResult delete(AnswerDTO dto) throws BusinessException ;
	
	ResponseResult deleteById(Long id) throws BusinessException ;
	
	ResponseResult update(AnswerDTO dto) throws BusinessException ;
	
	ResponseResult findByExample(AnswerDTO dto) throws BusinessException;
	
	ResponseResult getByExample(AnswerDTO dto) throws BusinessException;
	
	ResponseResult deleteByIds(String ids) throws BusinessException ;
	

}
