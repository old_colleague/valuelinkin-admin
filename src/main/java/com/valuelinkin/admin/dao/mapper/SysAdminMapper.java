package com.valuelinkin.admin.dao.mapper;


import org.apache.ibatis.annotations.Mapper;

import com.valuelinkin.admin.dao.entity.SysAdmin;
import com.valuelinkin.admin.dao.BaseMapper;

@Mapper
public interface SysAdminMapper extends BaseMapper<SysAdmin>{
	

}
