package com.valuelinkin.admin.service.core.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.jcommon.utils.RamdonString;
import com.jcommon.utils.StringUtils;
import com.valuelinkin.admin.common.utils.StockUtils;
import com.valuelinkin.admin.dao.entity.NewsColumn;
import com.valuelinkin.admin.dao.entity.RelativeStock;
import com.valuelinkin.admin.dao.entity.User;
import com.valuelinkin.admin.dao.mapper.NewsColumnMapper;
import com.valuelinkin.admin.dao.mapper.RelativeStockMapper;
import com.valuelinkin.admin.dao.mapper.UserMapper;
import com.valuelinkin.admin.dto.OpinionResultDTO;
import com.valuelinkin.admin.dto.StockInfoDTO;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jcommon.orm.Page;
import com.jcommon.exception.BusinessException;
import com.jcommon.utils.BeanUtils;
import com.jcommon.result.ResponseResult;

import com.valuelinkin.admin.dao.entity.Opinion;
import com.valuelinkin.admin.dto.OpinionDTO;
import com.valuelinkin.admin.dao.mapper.OpinionMapper;
import com.valuelinkin.admin.service.core.OpinionService;
import com.valuelinkin.admin.service.UidGenerator;
import com.valuelinkin.admin.common.utils.AppConstants;

import tk.mybatis.mapper.entity.Example;

@Service
public class OpinionServiceImpl implements OpinionService {

    @Autowired
    private OpinionMapper opinionMapper;

    @Autowired
    private UidGenerator uidGenerator;

    @Autowired
    private NewsColumnMapper newsColumnMapper;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private RelativeStockMapper relativeStockMapper;

    public ResponseResult findPage(OpinionDTO dto) {
        ResponseResult result = new ResponseResult();

        Opinion entity = new Opinion();
        BeanUtils.copyProperties(entity, dto);

        Page page = new Page();
        page.setPageNo(dto.getPageNo());
        page.setPageSize(dto.getPageSize());


        Example example = new Example(Opinion.class);
        String keywords = StringUtils.trimToNull(dto.getKeywords());
        Example.Criteria criteria = example.createCriteria();
        dto.setKeywords(keywords);
        if (StringUtils.isNotBlank(keywords)) {
            keywords = "%" + keywords + "%";
            dto.setKeywords(keywords);

            criteria.orLike("opnTitle", keywords).orLike("opnStockCode", keywords)
                    .orLike("opnBrief", keywords)
                    .orLike("opnContent", keywords);
        }
        if (StringUtils.isNotBlank(dto.getClmCode())) {
            Example.Criteria criteria2 = example.createCriteria();
            NewsColumn newsColumn = new NewsColumn();
            newsColumn.setClmCode(dto.getClmCode());
            newsColumn = newsColumnMapper.selectOne(newsColumn);
            if (newsColumn != null) {
                criteria2.andEqualTo("clmId", newsColumn.getClmId());
                example.and(criteria2);
            }
        } else {
            Example.Criteria criteria2 = example.createCriteria();
            criteria2.andGreaterThan("usrId", 0);
            example.and(criteria2);

        }
        if (dto.getClmId() != null && dto.getClmId() > 0) {
            Example.Criteria criteria2 = example.createCriteria();
            criteria2.andEqualTo("clmId", dto.getClmId());
            example.and(criteria2);
        }
        page.setTotalCount(opinionMapper.selectCountByExample(example));
        //List<OpinionDTO> list = opinionMapper.selectByExampleAndRowBounds(example, new RowBounds(page.getFirst(), page.getPageSize()));

        List<OpinionResultDTO> list = opinionMapper.findPage(dto);

        page.setResult(list);
        result.setData(page);
        result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
        return result;

    }

    public ResponseResult getById(Long id) throws BusinessException {
        ResponseResult result = new ResponseResult();
        Opinion entity = opinionMapper.selectByPrimaryKey(id);
        OpinionDTO opinionDTO = new OpinionDTO();
        BeanUtils.copyProperties(opinionDTO, entity);

        User user = userMapper.selectByPrimaryKey(opinionDTO.getUsrId());
        if (user != null) {
            opinionDTO.setUsrNickName(user.getUsrNickName());
        }

        NewsColumn newsColumn = newsColumnMapper.selectByPrimaryKey(entity.getClmId());

        opinionDTO.setClmCode(newsColumn.getClmCode());

        result.setData(opinionDTO);
        result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
        return result;
    }


    /**
     * 插入数据
     */
    @Transactional
    public ResponseResult save(OpinionDTO dto) throws BusinessException {
        ResponseResult result = new ResponseResult();
        Opinion entity = new Opinion();
        BeanUtils.copyProperties(entity, dto);
        entity.setOpnId(uidGenerator.nextId());

        NewsColumn newsColumn = newsColumnMapper.selectByPrimaryKey(dto.getClmId());
        if (newsColumn != null && newsColumn.getClmCode().equals("DKKS")) {
            //entity.setNwsStatus(3);
        } else {
            StockInfoDTO stockInfoDTO = StockUtils.getStockInfo(dto.getOpnStockCode());

            if (stockInfoDTO == null || StringUtils.isBlank(stockInfoDTO.getStkCode())) {
                result.setErrorMessage("股票代码不存在");
                return result;
            }

            String title = "【" + stockInfoDTO.getStkCode() + " " + stockInfoDTO.getStkName() + "】";
            //entity.setOpnTitle(title);

            RelativeStock relativeStock = new RelativeStock();
            relativeStock.setLnkId(uidGenerator.nextId());
            relativeStock.setStkCode(stockInfoDTO.getStkCode());
            relativeStock.setRelateId(entity.getOpnId());
            relativeStockMapper.insertSelective(relativeStock);
        }

        if (newsColumn == null) {
            result.setErrorMessage("参数错误，栏目代码不存在");
            return result;
        }

        if (StringUtils.isBlank(entity.getOpnContent())) {
            entity.setOpnContent(entity.getOpnBrief());
        }
        Date now = new Date();
        entity.setCreatedOn(now);
        entity.setModifiedOn(now);
        entity.setOpnPublishDate(now);
        entity.setOpnViewCount(RamdonString.getRamdonInt(100, 2000));
        entity.setOpnStatus(1);
        opinionMapper.insertSelective(entity);

        result.setData(entity);
        result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
        return result;
    }

    @Transactional
    public ResponseResult deleteById(Long id) throws BusinessException {
        ResponseResult result = new ResponseResult();
        opinionMapper.deleteByPrimaryKey(id);

        result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
        return result;
    }

    @Transactional
    public ResponseResult delete(OpinionDTO dto) throws BusinessException {
        ResponseResult result = new ResponseResult();

        Opinion entity = new Opinion();
        BeanUtils.copyProperties(entity, dto);

        opinionMapper.delete(entity);
        result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
        return result;
    }

    @Transactional
    public ResponseResult deleteByIds(String ids) throws BusinessException {
        ResponseResult result = new ResponseResult();
        String[] items = ids.split(",");
        for (String str : items) {
            Long id = new Long(str);
            opinionMapper.deleteByPrimaryKey(id);
        }
        result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
        return result;
    }

    @Transactional
    public ResponseResult update(OpinionDTO dto) throws BusinessException {
        ResponseResult result = new ResponseResult();
        Opinion entity = new Opinion();
        BeanUtils.copyProperties(entity, dto);
        opinionMapper.updateByPrimaryKeySelective(entity);

        result.setData(entity);
        result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
        return result;
    }

    public ResponseResult findByExample(OpinionDTO dto) throws BusinessException {
        ResponseResult result = new ResponseResult();

        Opinion entity = new Opinion();
        BeanUtils.copyProperties(entity, dto);

        List list = opinionMapper.select(entity);

        result.setData(list);
        result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
        return result;
    }


    public ResponseResult getByExample(OpinionDTO dto) throws BusinessException {
        ResponseResult result = new ResponseResult();

        Opinion entity = new Opinion();
        BeanUtils.copyProperties(entity, dto);

        entity = opinionMapper.selectOne(entity);

        result.setData(entity);
        result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
        return result;

    }

    public void setOpinionMapper(OpinionMapper dao) {
        this.opinionMapper = dao;
    }
}
