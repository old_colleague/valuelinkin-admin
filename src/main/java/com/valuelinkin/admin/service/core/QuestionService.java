package com.valuelinkin.admin.service.core;

import java.util.List;

import com.jcommon.orm.Page;
import com.jcommon.exception.BusinessException;
import com.jcommon.result.ResponseResult;

import com.valuelinkin.admin.dto.QuestionDTO;

public interface QuestionService {

	ResponseResult findPage(QuestionDTO dto) throws BusinessException;
	
	ResponseResult getById(Long id) throws BusinessException;

	ResponseResult save(QuestionDTO dto) throws BusinessException;
	
	ResponseResult delete(QuestionDTO dto) throws BusinessException ;
	
	ResponseResult deleteById(Long id) throws BusinessException ;
	
	ResponseResult update(QuestionDTO dto) throws BusinessException ;
	
	ResponseResult findByExample(QuestionDTO dto) throws BusinessException;
	
	ResponseResult getByExample(QuestionDTO dto) throws BusinessException;
	
	ResponseResult deleteByIds(String ids) throws BusinessException ;
	

}
