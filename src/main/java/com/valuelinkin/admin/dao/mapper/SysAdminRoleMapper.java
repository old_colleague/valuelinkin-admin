package com.valuelinkin.admin.dao.mapper;


import org.apache.ibatis.annotations.Mapper;

import com.valuelinkin.admin.dao.entity.SysAdminRole;
import com.valuelinkin.admin.dao.BaseMapper;

@Mapper
public interface SysAdminRoleMapper extends BaseMapper<SysAdminRole>{
	

}
