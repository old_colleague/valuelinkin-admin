package com.valuelinkin.admin.dto;

import java.util.Date;

/**
 * @atuhor: nail.zhang
 * @description:
 * @date: 2019/2/11 22:57
 */
public class NewsLiveResultDTO {

    private Long nwsId;
    private Long clmId;
    private java.lang.String nwsTitle;
    private java.lang.String nwsKeywords;
    private java.lang.String nwsBrief;
    private java.lang.String nwsThumbnailUrl;
    private java.lang.String nwsVideoUrl;

    private java.util.Date nwsPublishDate;
    private Integer nwsStatus;
    private java.lang.String nwsViewCount;
    private Integer nwsStickTop;
    private java.lang.String nwsColor;
    private java.lang.String nwsStockCode;
    private java.lang.String nwsRelateNews;
    private java.util.Date createdOn;
    private java.util.Date modifiedOn;

    private String clmDisplayName;

    private String nwsHeadUrl;

    public String getNwsHeadUrl() {
        return nwsHeadUrl;
    }

    public void setNwsHeadUrl(String nwsHeadUrl) {
        this.nwsHeadUrl = nwsHeadUrl;
    }

    public Long getNwsId() {
        return nwsId;
    }

    public void setNwsId(Long nwsId) {
        this.nwsId = nwsId;
    }

    public Long getClmId() {
        return clmId;
    }

    public void setClmId(Long clmId) {
        this.clmId = clmId;
    }

    public String getNwsTitle() {
        return nwsTitle;
    }

    public void setNwsTitle(String nwsTitle) {
        this.nwsTitle = nwsTitle;
    }

    public String getNwsKeywords() {
        return nwsKeywords;
    }

    public void setNwsKeywords(String nwsKeywords) {
        this.nwsKeywords = nwsKeywords;
    }

    public String getNwsBrief() {
        return nwsBrief;
    }

    public void setNwsBrief(String nwsBrief) {
        this.nwsBrief = nwsBrief;
    }

    public String getNwsThumbnailUrl() {
        return nwsThumbnailUrl;
    }

    public void setNwsThumbnailUrl(String nwsThumbnailUrl) {
        this.nwsThumbnailUrl = nwsThumbnailUrl;
    }

    public String getNwsVideoUrl() {
        return nwsVideoUrl;
    }

    public void setNwsVideoUrl(String nwsVideoUrl) {
        this.nwsVideoUrl = nwsVideoUrl;
    }

    public Date getNwsPublishDate() {
        return nwsPublishDate;
    }

    public void setNwsPublishDate(Date nwsPublishDate) {
        this.nwsPublishDate = nwsPublishDate;
    }

    public Integer getNwsStatus() {
        return nwsStatus;
    }

    public void setNwsStatus(Integer nwsStatus) {
        this.nwsStatus = nwsStatus;
    }

    public String getNwsViewCount() {
        return nwsViewCount;
    }

    public void setNwsViewCount(String nwsViewCount) {
        this.nwsViewCount = nwsViewCount;
    }

    public Integer getNwsStickTop() {
        return nwsStickTop;
    }

    public void setNwsStickTop(Integer nwsStickTop) {
        this.nwsStickTop = nwsStickTop;
    }

    public String getNwsColor() {
        return nwsColor;
    }

    public void setNwsColor(String nwsColor) {
        this.nwsColor = nwsColor;
    }

    public String getNwsStockCode() {
        return nwsStockCode;
    }

    public void setNwsStockCode(String nwsStockCode) {
        this.nwsStockCode = nwsStockCode;
    }

    public String getNwsRelateNews() {
        return nwsRelateNews;
    }

    public void setNwsRelateNews(String nwsRelateNews) {
        this.nwsRelateNews = nwsRelateNews;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Date getModifiedOn() {
        return modifiedOn;
    }

    public void setModifiedOn(Date modifiedOn) {
        this.modifiedOn = modifiedOn;
    }

    public String getClmDisplayName() {
        return clmDisplayName;
    }

    public void setClmDisplayName(String clmDisplayName) {
        this.clmDisplayName = clmDisplayName;
    }





}
