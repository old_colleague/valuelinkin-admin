package com.valuelinkin.admin.service.core;

import java.util.List;

import com.jcommon.orm.Page;
import com.jcommon.exception.BusinessException;
import com.jcommon.result.ResponseResult;

import com.valuelinkin.admin.dto.NewsColumnDTO;

public interface NewsColumnService {

	ResponseResult findPage(NewsColumnDTO dto) throws BusinessException;
	
	ResponseResult getById(Long id) throws BusinessException;

	ResponseResult save(NewsColumnDTO dto) throws BusinessException;
	
	ResponseResult delete(NewsColumnDTO dto) throws BusinessException ;
	
	ResponseResult deleteById(Long id) throws BusinessException ;
	
	ResponseResult update(NewsColumnDTO dto) throws BusinessException ;
	
	ResponseResult findByExample(NewsColumnDTO dto) throws BusinessException;
	
	ResponseResult getByExample(NewsColumnDTO dto) throws BusinessException;
	
	ResponseResult deleteByIds(String ids) throws BusinessException ;
	
	ResponseResult findChildColumnByCode(String clmCode) throws BusinessException;
	

}
