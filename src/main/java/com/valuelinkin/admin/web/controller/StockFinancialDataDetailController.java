package com.valuelinkin.admin.web.controller;

import java.util.List;

import com.jcommon.orm.Page;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.ui.Model;

import com.valuelinkin.admin.dto.StockFinancialDataDetailDTO;
import com.valuelinkin.admin.service.core.StockFinancialDataDetailService;

import com.jcommon.result.ResponseResult;
import com.jcommon.utils.BeanUtils;

@Controller
@RequestMapping(value="/stockFinancialDataDetail") 
public class StockFinancialDataDetailController{
	
	@Autowired
	private StockFinancialDataDetailService stockFinancialDataDetailService;
	
	@RequestMapping(value="/index",method=RequestMethod.GET)
	public String index( Model model) throws Exception {
		
		return "/stockFinancialDataDetail/list";
	}
	
	/** 
	 * 执行搜索 
	 **/
	@RequestMapping(value="",method=RequestMethod.GET)
	@ResponseBody
	public ResponseResult findPage(StockFinancialDataDetailDTO dto) {
		
		ResponseResult result = new ResponseResult();
		result = this.stockFinancialDataDetailService.findPage(dto);
		return result;
	}
	
	
	/** 
	 * 查看对象
	 **/
	@RequestMapping(value="/view/{id}",method=RequestMethod.GET)
	public String getById(@PathVariable Long id, Model model) {
		ResponseResult result = new ResponseResult();
		result = stockFinancialDataDetailService.getById(id);
		
		model.addAttribute("result", result);
		return "/stockFinancialDataDetail/view";
		
	}
	

	/** 
	 * 进入新增页面
	 **/
	@RequestMapping(value="/create", method=RequestMethod.GET)
	public String create(StockFinancialDataDetailDTO dto, Model model) throws Exception {
		model.addAttribute("stockFinancialDataDetail", dto);
		return "/stockFinancialDataDetail/create";
	}
	
	/** 
	 * 保存新增对象
	 **/
	@RequestMapping(value="",method=RequestMethod.POST)
	@ResponseBody
	public ResponseResult save(StockFinancialDataDetailDTO dto) {
		ResponseResult result = null;
		result = stockFinancialDataDetailService.save(dto);
		return result;
	}
	
	
	/**
	 * 进入更新页面
	 **/
	@RequestMapping(value="/edit/{id}", method=RequestMethod.GET)
	public String edit(@PathVariable Long id, Model model) throws Exception {

		ResponseResult result  = stockFinancialDataDetailService.getById(id);
		model.addAttribute("result", result);
		
		return "/stockFinancialDataDetail/edit";
	}
	
	/**
	 * 保存更新对象
	 **/
	@RequestMapping(value="/{id}", method=RequestMethod.PUT)
	@ResponseBody
	public ResponseResult update(@PathVariable Long id,StockFinancialDataDetailDTO dto) throws Exception {
		ResponseResult result = new ResponseResult();
		dto.setDtlId(id);
		result = stockFinancialDataDetailService.update(dto);
		return result;
	}
	
	
	/**
	 *删除多个对象
	 **/
	@RequestMapping(value="/{ids}", method=RequestMethod.DELETE)
	@ResponseBody
	public ResponseResult delete(@PathVariable String ids) {
		
		ResponseResult result = null;
		result = stockFinancialDataDetailService.deleteByIds(ids);
		return result;
		
	}
	
	public void setStockFinancialDataDetailService(StockFinancialDataDetailService service) {
		this.stockFinancialDataDetailService = service;
	}
	
}
