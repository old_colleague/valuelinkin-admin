
package com.valuelinkin.admin;

import java.io.IOException;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.convert.converter.Converter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import com.alibaba.fastjson.serializer.JSONSerializer;
import com.alibaba.fastjson.serializer.ObjectSerializer;
import com.alibaba.fastjson.serializer.SerializeConfig;
import com.alibaba.fastjson.serializer.SerializeWriter;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.serializer.ToStringSerializer;
import com.alibaba.fastjson.support.config.FastJsonConfig;
import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import com.jcommon.utils.DateUtils;
import com.jcommon.utils.StringUtils;
import com.valuelinkin.admin.web.interceptor.LoginInterceptor;

/**
 * 
 * 
 * @atuhor: nail.zhang
 * @description:
 * @date: Created in 2018年11月27日
 */
@Configuration
public class WebAppConfigurer implements WebMvcConfigurer {

	private static Logger logger = LoggerFactory.getLogger(WebAppConfigurer.class);

	private static final SerializerFeature[] features = { SerializerFeature.WriteMapNullValue, // 输出空置字段
			SerializerFeature.WriteNullListAsEmpty, // list字段如果为null，输出为[]，而不是null
			SerializerFeature.WriteNullNumberAsZero, // 数值字段如果为null，输出为0，而不是null
			SerializerFeature.WriteNullBooleanAsFalse, // Boolean字段如果为null，输出为false，而不是null
			SerializerFeature.WriteNullStringAsEmpty // 字符类型字段如果为null，输出为""，而不是null
			// SerializerFeature.

	};

	@Autowired
	@Lazy
	private LoginInterceptor loginInterceptor;

	@Bean
	public LocaleResolver localeResolver() {
		SessionLocaleResolver slr = new SessionLocaleResolver();
		// 默认语言
		slr.setDefaultLocale(Locale.CHINESE);
		return slr;
	}

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		// 多个拦截器组成一个拦截器链
		// addPathPatterns 用于添加拦截规则
		// excludePathPatterns 用户排除拦截
		registry.addInterceptor(loginInterceptor).addPathPatterns("/*").addPathPatterns("/*/*").excludePathPatterns("/lib/*", "/login*",
				"/authrize*");

	}

	private CorsConfiguration buildConfig() {
		CorsConfiguration corsConfiguration = new CorsConfiguration();
		corsConfiguration.addAllowedOrigin("*"); // 1
		corsConfiguration.addAllowedHeader("*"); // 2
		corsConfiguration.addAllowedMethod("*"); // 3
		return corsConfiguration;
	}

	@Bean
	public CorsFilter corsFilter() {
		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		source.registerCorsConfiguration("/**", buildConfig());
		return new CorsFilter(source);
	}

	@Bean
	public HttpMessageConverters fastJsonHttpMessageConverters() {
		// 1.定义一个converters转换消息的对象
		FastJsonHttpMessageConverter fastConverter = new FastJsonHttpMessageConverter();
		// 2.添加fastjson的配置信息，比如: 是否需要格式化返回的json数据
		FastJsonConfig fastJsonConfig = new FastJsonConfig();
		fastJsonConfig.setSerializerFeatures(SerializerFeature.PrettyFormat);
		fastJsonConfig.setDateFormat("yyyy-MM-dd HH:mm:ss");

		SerializeConfig serializeConfig = fastJsonConfig.getSerializeConfig();
		serializeConfig.put(BigInteger.class, ToStringSerializer.instance);
		serializeConfig.put(Long.class, ToStringSerializer.instance);
		serializeConfig.put(Long.TYPE, ToStringSerializer.instance);

		ObjectSerializer doubleSerializer = new ObjectSerializer() {
			@Override
			public void write(JSONSerializer serializer, Object object, Object fieldName, java.lang.reflect.Type fieldType, int features)
					throws IOException {
				SerializeWriter out = serializer.getWriter();
				DecimalFormat df = new DecimalFormat("#.0000");// 四舍五入
				if (object == null) {
					out.writeNull();
					return;
				}
				out.writeString(df.format(object));
			}
		};

		// serializeConfig.put(Double.TYPE, doubleSerializer);
		// serializeConfig.put(Double.class, doubleSerializer);

		// serializeConfig.put(Float.TYPE, doubleSerializer);
		// serializeConfig.put(Float.class, doubleSerializer);
		// serializeConfig.put(BigDecimal.class, doubleSerializer);

		fastJsonConfig.setSerializeConfig(serializeConfig);
		fastJsonConfig.setSerializerFeatures(features);

		fastJsonConfig.setSerializeConfig(serializeConfig);

		// 3.在converter中添加配置信息
		fastConverter.setFastJsonConfig(fastJsonConfig);
		// 4.将converter赋值给HttpMessageConverter
		HttpMessageConverter<?> converter = fastConverter;
		List<MediaType> list = new ArrayList<MediaType>();
		list.add(MediaType.APPLICATION_JSON_UTF8);
		fastConverter.setSupportedMediaTypes(list);
		// 5.返回HttpMessageConverters对象
		return new HttpMessageConverters(converter);
	}

	@Bean
	public Converter<String, Date> addDateConvert() {
		return new Converter<String, Date>() {
			@Override
			public Date convert(String source) {
				SimpleDateFormat sdf = new SimpleDateFormat(DateUtils.TIME_FORMAT);
				Date date = null;
				if (StringUtils.trimToNull(source) == null) {
					return date;
				}
				try {
					date = sdf.parse((String) source);
				} catch (Exception e) {
					logger.error("", e);
				}
				return date;
			}
		};
	}

}
