package com.valuelinkin.admin.dto;

import java.util.List;

public class StockDTO extends BaseDTO {
    private static final long serialVersionUID = 3148176768559230877L;
    

	private Long stkId;
	private java.lang.String stkCode;
	private java.lang.String stkName;
	private java.lang.String stkLogo;
	private java.lang.String stkKeys;
	private Integer stkShareholders;
	private Double stkCirculatingSharesRate;
	//财务报表标题
	private String sdfDataTitle;
	
	private List<String> dtlTitle;
	private List<String> dtlDataValue;
	private List<String> dtlDataRate;
	
	private List<String> sshRealName;	
	private List<Double> sshWeight;
	
	private java.util.Date createdOn;
	private java.util.Date modifiedOn;
	
	private Long grpId;
	
	private String keywords;

	
	public Long getStkId() {
		return this.stkId;
	}
	
	public void setStkId(Long value) {
		this.stkId = value;
	}
	
	public java.lang.String getStkCode() {
		return this.stkCode;
	}
	
	public void setStkCode(java.lang.String value) {
		this.stkCode = value;
	}
	
	public java.lang.String getStkName() {
		return this.stkName;
	}
	
	public void setStkName(java.lang.String value) {
		this.stkName = value;
	}
	
	public java.lang.String getStkLogo() {
		return this.stkLogo;
	}
	
	public void setStkLogo(java.lang.String value) {
		this.stkLogo = value;
	}
	
	public java.lang.String getStkKeys() {
		return this.stkKeys;
	}
	
	public void setStkKeys(java.lang.String value) {
		this.stkKeys = value;
	}
	
	public Integer getStkShareholders() {
		return this.stkShareholders;
	}
	
	public void setStkShareholders(Integer value) {
		this.stkShareholders = value;
	}
	
	public Double getStkCirculatingSharesRate() {
		return this.stkCirculatingSharesRate;
	}
	
	public void setStkCirculatingSharesRate(Double value) {
		this.stkCirculatingSharesRate = value;
	}
	
	public java.util.Date getCreatedOn() {
		return this.createdOn;
	}
	
	public void setCreatedOn(java.util.Date value) {
		this.createdOn = value;
	}
	
	public java.util.Date getModifiedOn() {
		return this.modifiedOn;
	}
	
	public void setModifiedOn(java.util.Date value) {
		this.modifiedOn = value;
	}

	/**
	 * @return the sdfDataTitle
	 */
	public String getSdfDataTitle() {
		return sdfDataTitle;
	}

	/**
	 * @param sdfDataTitle the sdfDataTitle to set
	 */
	public void setSdfDataTitle(String sdfDataTitle) {
		this.sdfDataTitle = sdfDataTitle;
	}

	/**
	 * @return the dtlTitle
	 */
	public List<String> getDtlTitle() {
		return dtlTitle;
	}

	/**
	 * @param dtlTitle the dtlTitle to set
	 */
	public void setDtlTitle(List<String> dtlTitle) {
		this.dtlTitle = dtlTitle;
	}

	/**
	 * @return the dtlDataValue
	 */
	public List<String> getDtlDataValue() {
		return dtlDataValue;
	}

	/**
	 * @param dtlDataValue the dtlDataValue to set
	 */
	public void setDtlDataValue(List<String> dtlDataValue) {
		this.dtlDataValue = dtlDataValue;
	}

	/**
	 * @return the dtlDataRate
	 */
	public List<String> getDtlDataRate() {
		return dtlDataRate;
	}

	/**
	 * @param dtlDataRate the dtlDataRate to set
	 */
	public void setDtlDataRate(List<String> dtlDataRate) {
		this.dtlDataRate = dtlDataRate;
	}

	/**
	 * @return the sshRealName
	 */
	public List<String> getSshRealName() {
		return sshRealName;
	}

	/**
	 * @param sshRealName the sshRealName to set
	 */
	public void setSshRealName(List<String> sshRealName) {
		this.sshRealName = sshRealName;
	}

	/**
	 * @return the sshWeight
	 */
	public List<Double> getSshWeight() {
		return sshWeight;
	}

	/**
	 * @param sshWeight the sshWeight to set
	 */
	public void setSshWeight(List<Double> sshWeight) {
		this.sshWeight = sshWeight;
	}

	/**
	 * @return the grpId
	 */
	public Long getGrpId() {
		return grpId;
	}

	/**
	 * @param grpId the grpId to set
	 */
	public void setGrpId(Long grpId) {
		this.grpId = grpId;
	}

	/**
	 * @return the keywords
	 */
	public String getKeywords() {
		return keywords;
	}

	/**
	 * @param keywords the keywords to set
	 */
	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}
	
}

