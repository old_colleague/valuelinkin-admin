package com.valuelinkin.admin.service.core;

import java.util.List;

import com.jcommon.orm.Page;
import com.jcommon.exception.BusinessException;
import com.jcommon.result.ResponseResult;

import com.valuelinkin.admin.dto.AppMessageDTO;

public interface AppMessageService {

	ResponseResult findPage(AppMessageDTO dto) throws BusinessException;
	
	ResponseResult getById(Long id) throws BusinessException;

	ResponseResult save(AppMessageDTO dto) throws BusinessException;
	
	ResponseResult delete(AppMessageDTO dto) throws BusinessException ;
	
	ResponseResult deleteById(Long id) throws BusinessException ;
	
	ResponseResult update(AppMessageDTO dto) throws BusinessException ;
	
	ResponseResult findByExample(AppMessageDTO dto) throws BusinessException;
	
	ResponseResult getByExample(AppMessageDTO dto) throws BusinessException;
	
	ResponseResult deleteByIds(String ids) throws BusinessException ;

	ResponseResult batchSave( List<Long> userIdList,AppMessageDTO dto) throws BusinessException;
	

}
