package com.valuelinkin.admin.dto;

import com.valuelinkin.admin.dto.BaseDTO;

public class AccessTokenDTO extends BaseDTO {
    private static final long serialVersionUID = 3148176768559230877L;
    

	private Long tknId;
	private java.lang.String tknLoginId;
	private java.lang.String tknToken;
	private java.util.Date tknCreateTime;
	private java.util.Date tknExpiredTime;
	private java.lang.String tknSourceId;
	private java.lang.String tknTerminalId;
	private java.lang.String tknRefreshToken;

	
	public Long getTknId() {
		return this.tknId;
	}
	
	public void setTknId(Long value) {
		this.tknId = value;
	}
	
	public java.lang.String getTknLoginId() {
		return this.tknLoginId;
	}
	
	public void setTknLoginId(java.lang.String value) {
		this.tknLoginId = value;
	}
	
	public java.lang.String getTknToken() {
		return this.tknToken;
	}
	
	public void setTknToken(java.lang.String value) {
		this.tknToken = value;
	}
	
	public java.util.Date getTknCreateTime() {
		return this.tknCreateTime;
	}
	
	public void setTknCreateTime(java.util.Date value) {
		this.tknCreateTime = value;
	}
	
	public java.util.Date getTknExpiredTime() {
		return this.tknExpiredTime;
	}
	
	public void setTknExpiredTime(java.util.Date value) {
		this.tknExpiredTime = value;
	}
	
	public java.lang.String getTknSourceId() {
		return this.tknSourceId;
	}
	
	public void setTknSourceId(java.lang.String value) {
		this.tknSourceId = value;
	}
	
	public java.lang.String getTknTerminalId() {
		return this.tknTerminalId;
	}
	
	public void setTknTerminalId(java.lang.String value) {
		this.tknTerminalId = value;
	}
	
	public java.lang.String getTknRefreshToken() {
		return this.tknRefreshToken;
	}
	
	public void setTknRefreshToken(java.lang.String value) {
		this.tknRefreshToken = value;
	}
	
}

