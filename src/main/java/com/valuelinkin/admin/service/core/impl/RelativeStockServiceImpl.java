package com.valuelinkin.admin.service.core.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jcommon.orm.Page;
import com.jcommon.exception.BusinessException;
import com.jcommon.utils.BeanUtils;
import com.jcommon.result.ResponseResult;

import com.valuelinkin.admin.dao.entity.RelativeStock;
import com.valuelinkin.admin.dto.RelativeStockDTO;
import com.valuelinkin.admin.dao.mapper.RelativeStockMapper;
import com.valuelinkin.admin.service.core.RelativeStockService;
import com.valuelinkin.admin.service.UidGenerator;
import com.valuelinkin.admin.common.utils.AppConstants;

import tk.mybatis.mapper.entity.Example;

@Service
public class RelativeStockServiceImpl implements RelativeStockService{

	@Autowired
	private RelativeStockMapper relativeStockMapper;
	
	@Autowired
	private UidGenerator uidGenerator;

	public ResponseResult findPage(RelativeStockDTO dto) {
		ResponseResult result = new ResponseResult();
		
		RelativeStock entity = new RelativeStock();
		BeanUtils.copyProperties(entity, dto);
		
		Page page = new Page();
		page.setPageNo(dto.getPageNo());
		page.setPageSize(dto.getPageSize());
		page.setTotalCount(relativeStockMapper.selectCount(entity));
		
		Example example = new Example(RelativeStock.class);
		//example.createCriteria().andEqualTo("refId", rowNews.getRefId());
		//example.setOrderByClause("nws_Publish_Date desc");
		
		List<RelativeStock> list = relativeStockMapper.selectByExampleAndRowBounds(example, new RowBounds(page.getFirst(), page.getPageSize()));
		
		page.setResult(list);
		result.setData(page);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
		
	}	
	
	public ResponseResult getById(Long id) throws BusinessException{
		ResponseResult result = new ResponseResult();
		RelativeStock entity = relativeStockMapper.selectByPrimaryKey(id);
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}

	
	/** 插入数据 */
	@Transactional
	public ResponseResult save(RelativeStockDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		RelativeStock entity = new RelativeStock();
		BeanUtils.copyProperties(entity, dto);
		entity.setLnkId(uidGenerator.nextId());
		
		relativeStockMapper.insertSelective(entity);
		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult deleteById(Long id) throws BusinessException{
		ResponseResult result = new ResponseResult();
		relativeStockMapper.deleteByPrimaryKey(id);
		
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult delete(RelativeStockDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		
		RelativeStock entity = new RelativeStock();
		BeanUtils.copyProperties(entity, dto);
		
		relativeStockMapper.delete(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult deleteByIds(String ids) throws BusinessException{
		ResponseResult result = new ResponseResult();
		String[] items = ids.split(",");
		for (String str : items) {
			Long id = new Long(str);
			relativeStockMapper.deleteByPrimaryKey(id);
		}
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult update(RelativeStockDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		RelativeStock entity = new RelativeStock();
		BeanUtils.copyProperties(entity, dto);
		relativeStockMapper.updateByPrimaryKeySelective(entity);
		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	public ResponseResult findByExample(RelativeStockDTO dto) throws BusinessException {
		ResponseResult result = new ResponseResult();
		
		RelativeStock entity = new RelativeStock();
		BeanUtils.copyProperties(entity, dto);
		
		List list =relativeStockMapper.select(entity);
		
		result.setData(list);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	
	public ResponseResult getByExample(RelativeStockDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		
		RelativeStock entity = new RelativeStock();
		BeanUtils.copyProperties(entity, dto);
		
		entity =relativeStockMapper.selectOne(entity);
		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
		
	}
	
	public void setRelativeStockMapper(RelativeStockMapper dao) {
		this.relativeStockMapper = dao;
	}
}
