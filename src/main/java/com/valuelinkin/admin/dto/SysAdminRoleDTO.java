package com.valuelinkin.admin.dto;

import com.valuelinkin.admin.dto.BaseDTO;

public class SysAdminRoleDTO extends BaseDTO {
    private static final long serialVersionUID = 3148176768559230877L;
    

	private Long id;
	private Long admId;
	private Long rleId;

	
	public Long getId() {
		return this.id;
	}
	
	public void setId(Long value) {
		this.id = value;
	}
	
	public Long getAdmId() {
		return this.admId;
	}
	
	public void setAdmId(Long value) {
		this.admId = value;
	}
	
	public Long getRleId() {
		return this.rleId;
	}
	
	public void setRleId(Long value) {
		this.rleId = value;
	}
	
}

