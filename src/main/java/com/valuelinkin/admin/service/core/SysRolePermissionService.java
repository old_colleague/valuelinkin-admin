package com.valuelinkin.admin.service.core;

import java.util.List;

import com.jcommon.orm.Page;
import com.jcommon.exception.BusinessException;
import com.jcommon.result.ResponseResult;

import com.valuelinkin.admin.dto.SysRolePermissionDTO;

public interface SysRolePermissionService {

	ResponseResult findPage(SysRolePermissionDTO dto) throws BusinessException;
	
	ResponseResult getById(Long id) throws BusinessException;

	ResponseResult save(SysRolePermissionDTO dto) throws BusinessException;
	
	ResponseResult delete(SysRolePermissionDTO dto) throws BusinessException ;
	
	ResponseResult deleteById(Long id) throws BusinessException ;
	
	ResponseResult update(SysRolePermissionDTO dto) throws BusinessException ;
	
	ResponseResult findByExample(SysRolePermissionDTO dto) throws BusinessException;
	
	ResponseResult getByExample(SysRolePermissionDTO dto) throws BusinessException;
	
	ResponseResult deleteByIds(String ids) throws BusinessException ;
	

}
