package com.valuelinkin.admin.service.core;

import java.util.List;

import com.jcommon.orm.Page;
import com.jcommon.exception.BusinessException;
import com.jcommon.result.ResponseResult;

import com.valuelinkin.admin.dto.StockShareholdingDTO;

public interface StockShareholdingService {

	ResponseResult findPage(StockShareholdingDTO dto) throws BusinessException;
	
	ResponseResult getById(Long id) throws BusinessException;

	ResponseResult save(StockShareholdingDTO dto) throws BusinessException;
	
	ResponseResult delete(StockShareholdingDTO dto) throws BusinessException ;
	
	ResponseResult deleteById(Long id) throws BusinessException ;
	
	ResponseResult update(StockShareholdingDTO dto) throws BusinessException ;
	
	ResponseResult findByExample(StockShareholdingDTO dto) throws BusinessException;
	
	ResponseResult getByExample(StockShareholdingDTO dto) throws BusinessException;
	
	ResponseResult deleteByIds(String ids) throws BusinessException ;
	

}
