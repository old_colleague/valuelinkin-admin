package com.valuelinkin.admin.dao.mapper;


import org.apache.ibatis.annotations.Mapper;

import com.valuelinkin.admin.dao.entity.AppMessage;
import com.valuelinkin.admin.dao.BaseMapper;

@Mapper
public interface AppMessageMapper extends BaseMapper<AppMessage>{
	

}
