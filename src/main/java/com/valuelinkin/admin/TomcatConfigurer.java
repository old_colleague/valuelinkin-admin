
package com.valuelinkin.admin;

import javax.servlet.MultipartConfigElement;

import org.apache.catalina.connector.Connector;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.embedded.tomcat.TomcatConnectorCustomizer;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.boot.web.servlet.server.ServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 
 * 
 * @atuhor: nail.zhang
 * @description:
 * @date: Created in  2018年11月27日
 */
@Configuration
public class TomcatConfigurer {


	@Value("${server.protocol}")
	private String protocol;


	@Value("${server.MaxFileSize}")
	private String maxFileSize;
	@Value("${server.MaxRequestSize}")
	private String maxRequestSize;

	@Bean
	public ServletWebServerFactory servletContainer() {
		TomcatServletWebServerFactory tomcat = new TomcatServletWebServerFactory();
		tomcat.addConnectorCustomizers(new MyTomcatConnectionCustomizer());
		return tomcat;
	}

	@Bean
	public MultipartConfigElement multipartConfigElement() {
		MultipartConfigFactory factory = new MultipartConfigFactory();
		// 单个数据大小
		//factory.setMaxFileSize(maxFileSize); // KB,MB
		/// 总上传数据大小
		//factory.setMaxRequestSize(maxRequestSize);
		return factory.createMultipartConfig();
	}

	/**
	 * 
	 * @author Administrator
	 * 说明：
	 * 修改说明：
	 */
	public class MyTomcatConnectionCustomizer implements TomcatConnectorCustomizer {

		public MyTomcatConnectionCustomizer() {
		}

		@Override
		public void customize(Connector connector) {
			//connector.setAttribute("acceptorThreadCount", acceptorThreadCount);
			//connector.setAttribute("minSpareThreads", minSpareThreads);
			//connector.setAttribute("maxSpareThreads", maxSpareThreads);
			connector.setAttribute("protocol", protocol);
		}
	}
}
