
package com.valuelinkin.admin.dao;

import java.util.Collection;
import java.util.List;

import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

/**
 * 
 * @author Nail.zhang
 *
 */
@SuppressWarnings("all")
public interface BaseMapper<T> extends Mapper<T>, MySqlMapper<T>  {
	
	/**
	 * 根据多个主键查询
	 * @param ids
	 * @return
	 */
	List selectByIds( Collection ids);
	
	

}
