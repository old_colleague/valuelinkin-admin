package com.valuelinkin.admin.dto;

import com.valuelinkin.admin.dto.BaseDTO;

public class StockFinancialDataDetailDTO extends BaseDTO {
    private static final long serialVersionUID = 3148176768559230877L;
    

	private Long dtlId;
	private Long sfdId;
	private java.lang.String dtlTitle;
	private java.lang.String dtlDataValue;
	private java.lang.String dtlDataRate;
	private java.util.Date createdOn;
	private java.util.Date modifiedOn;

	
	public Long getDtlId() {
		return this.dtlId;
	}
	
	public void setDtlId(Long value) {
		this.dtlId = value;
	}
	
	public Long getSfdId() {
		return this.sfdId;
	}
	
	public void setSfdId(Long value) {
		this.sfdId = value;
	}
	
	public java.lang.String getDtlTitle() {
		return this.dtlTitle;
	}
	
	public void setDtlTitle(java.lang.String value) {
		this.dtlTitle = value;
	}
	
	public java.lang.String getDtlDataValue() {
		return this.dtlDataValue;
	}
	
	public void setDtlDataValue(java.lang.String value) {
		this.dtlDataValue = value;
	}
	
	public java.lang.String getDtlDataRate() {
		return this.dtlDataRate;
	}
	
	public void setDtlDataRate(java.lang.String value) {
		this.dtlDataRate = value;
	}
	
	public java.util.Date getCreatedOn() {
		return this.createdOn;
	}
	
	public void setCreatedOn(java.util.Date value) {
		this.createdOn = value;
	}
	
	public java.util.Date getModifiedOn() {
		return this.modifiedOn;
	}
	
	public void setModifiedOn(java.util.Date value) {
		this.modifiedOn = value;
	}
	
}

