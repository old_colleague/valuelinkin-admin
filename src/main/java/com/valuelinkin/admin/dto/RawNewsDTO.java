package com.valuelinkin.admin.dto;

import com.valuelinkin.admin.dto.BaseDTO;

public class RawNewsDTO extends BaseDTO {
    private static final long serialVersionUID = 3148176768559230877L;
    

	private Long nwsId;
	private java.lang.String refId;
	private java.lang.String nwsTitle;
	private java.lang.String nwsBrief;
	private java.lang.String nwsContent;
	private java.lang.String nwsSource;
	private java.util.Date nwsPublishDte;
	private java.util.Date nwsCreateDate;
	private java.lang.String nwsUrl;
	private Integer nwsStatus;
	private java.lang.String nwsType;

	
	public Long getNwsId() {
		return this.nwsId;
	}
	
	public void setNwsId(Long value) {
		this.nwsId = value;
	}
	
	public java.lang.String getRefId() {
		return this.refId;
	}
	
	public void setRefId(java.lang.String value) {
		this.refId = value;
	}
	
	public java.lang.String getNwsTitle() {
		return this.nwsTitle;
	}
	
	public void setNwsTitle(java.lang.String value) {
		this.nwsTitle = value;
	}
	
	public java.lang.String getNwsBrief() {
		return this.nwsBrief;
	}
	
	public void setNwsBrief(java.lang.String value) {
		this.nwsBrief = value;
	}
	
	public java.lang.String getNwsContent() {
		return this.nwsContent;
	}
	
	public void setNwsContent(java.lang.String value) {
		this.nwsContent = value;
	}
	
	public java.lang.String getNwsSource() {
		return this.nwsSource;
	}
	
	public void setNwsSource(java.lang.String value) {
		this.nwsSource = value;
	}
	
	public java.util.Date getNwsPublishDte() {
		return this.nwsPublishDte;
	}
	
	public void setNwsPublishDte(java.util.Date value) {
		this.nwsPublishDte = value;
	}
	
	public java.util.Date getNwsCreateDate() {
		return this.nwsCreateDate;
	}
	
	public void setNwsCreateDate(java.util.Date value) {
		this.nwsCreateDate = value;
	}
	
	public java.lang.String getNwsUrl() {
		return this.nwsUrl;
	}
	
	public void setNwsUrl(java.lang.String value) {
		this.nwsUrl = value;
	}
	
	public Integer getNwsStatus() {
		return this.nwsStatus;
	}
	
	public void setNwsStatus(Integer value) {
		this.nwsStatus = value;
	}
	
	public java.lang.String getNwsType() {
		return this.nwsType;
	}
	
	public void setNwsType(java.lang.String value) {
		this.nwsType = value;
	}
	
}

