package com.valuelinkin.admin.service.core;

import java.util.List;

import com.jcommon.orm.Page;
import com.jcommon.exception.BusinessException;
import com.jcommon.result.ResponseResult;

import com.valuelinkin.admin.dto.StockDTO;

public interface StockService {

	ResponseResult findPage(StockDTO dto) throws BusinessException;
	
	ResponseResult getById(Long id) throws BusinessException;

	ResponseResult save(StockDTO dto) throws BusinessException;
	
	ResponseResult delete(StockDTO dto) throws BusinessException ;
	
	ResponseResult deleteById(Long id) throws BusinessException ;
	
	ResponseResult update(StockDTO dto) throws BusinessException ;
	
	ResponseResult findByExample(StockDTO dto) throws BusinessException;
	
	ResponseResult getByExample(StockDTO dto) throws BusinessException;
	
	ResponseResult deleteByIds(String ids) throws BusinessException ;
	
	ResponseResult getStockDetails(Long id) throws BusinessException;
	

}
