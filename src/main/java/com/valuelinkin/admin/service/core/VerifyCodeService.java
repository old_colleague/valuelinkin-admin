package com.valuelinkin.admin.service.core;

import java.util.List;

import com.jcommon.orm.Page;
import com.jcommon.exception.BusinessException;
import com.jcommon.result.ResponseResult;

import com.valuelinkin.admin.dto.VerifyCodeDTO;

public interface VerifyCodeService {

	ResponseResult findPage(VerifyCodeDTO dto) throws BusinessException;
	
	ResponseResult getById(Long id) throws BusinessException;

	ResponseResult save(VerifyCodeDTO dto) throws BusinessException;
	
	ResponseResult delete(VerifyCodeDTO dto) throws BusinessException ;
	
	ResponseResult deleteById(Long id) throws BusinessException ;
	
	ResponseResult update(VerifyCodeDTO dto) throws BusinessException ;
	
	ResponseResult findByExample(VerifyCodeDTO dto) throws BusinessException;
	
	ResponseResult getByExample(VerifyCodeDTO dto) throws BusinessException;
	
	ResponseResult deleteByIds(String ids) throws BusinessException ;
	

}
