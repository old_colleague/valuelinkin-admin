package com.valuelinkin.admin.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jcommon.result.ResponseResult;
import com.valuelinkin.admin.dto.GroupDTO;
import com.valuelinkin.admin.dto.StockDTO;
import com.valuelinkin.admin.service.core.GroupService;
import com.valuelinkin.admin.service.core.StockService;

@Controller
@RequestMapping(value="/stock") 
public class StockController{
	
	@Autowired
	private StockService stockService;
	
	@Autowired
	private GroupService groupService;
	
	@RequestMapping(value="/index",method=RequestMethod.GET)
	public String index( Model model) throws Exception {
		
		return "/stock/list";
	}
	
	/** 
	 * 执行搜索 
	 **/
	@RequestMapping(value="",method=RequestMethod.GET)
	@ResponseBody
	public ResponseResult findPage(StockDTO dto) {
		
		ResponseResult result = new ResponseResult();
		result = this.stockService.findPage(dto);
		return result;
	}
	
	
	/** 
	 * 查看对象
	 **/
	@RequestMapping(value="/view/{id}",method=RequestMethod.GET)
	public String getById(@PathVariable Long id, Model model) {
		ResponseResult result = new ResponseResult();
		result = stockService.getById(id);
		
		model.addAttribute("result", result);
		return "/stock/view";
		
	}
	

	/** 
	 * 进入新增页面
	 **/
	@RequestMapping(value="/create", method=RequestMethod.GET)
	public String create(StockDTO dto, Model model) throws Exception {
		model.addAttribute("stock", dto);
		
		GroupDTO groupDTO = new GroupDTO();
		groupDTO.setGrpStatus(0);
		ResponseResult result = groupService.findByExample(groupDTO);
		model.addAttribute("groupList", result.getData());
		return "/stock/create";
	}
	
	/** 
	 * 保存新增对象
	 **/
	@RequestMapping(value="",method=RequestMethod.POST)
	@ResponseBody
	public ResponseResult save(StockDTO dto) {
		ResponseResult result = null;
		result = stockService.save(dto);
		return result;
	}
	
	
	/**
	 * 进入更新页面
	 **/
	@RequestMapping(value="/edit/{id}", method=RequestMethod.GET)
	public String edit(@PathVariable Long id, Model model) throws Exception {

		ResponseResult result  = stockService.getStockDetails(id);
		model.addAttribute("result", result);
		
		return "/stock/edit";
	}
	
	/**
	 * 保存更新对象
	 **/
	@RequestMapping(value="/{id}", method=RequestMethod.PUT)
	@ResponseBody
	public ResponseResult update(@PathVariable Long id,StockDTO dto) throws Exception {
		ResponseResult result = new ResponseResult();
		dto.setStkId(id);
		result = stockService.update(dto);
		return result;
	}
	
	
	/**
	 *删除多个对象
	 **/
	@RequestMapping(value="/{ids}", method=RequestMethod.DELETE)
	@ResponseBody
	public ResponseResult delete(@PathVariable String ids) {
		
		ResponseResult result = null;
		result = stockService.deleteByIds(ids);
		return result;
		
	}
	
	public void setStockService(StockService service) {
		this.stockService = service;
	}
	
}
