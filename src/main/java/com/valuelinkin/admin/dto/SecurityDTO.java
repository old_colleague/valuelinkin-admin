package com.valuelinkin.admin.dto;

import com.valuelinkin.admin.dto.BaseDTO;

public class SecurityDTO extends BaseDTO {
    private static final long serialVersionUID = 3148176768559230877L;
    

	private Long stkId;
	private java.lang.String stkCode;
	private java.lang.String stkName;
	private java.lang.String stkKeys;
	private java.lang.String stkStatus;

	
	public Long getStkId() {
		return this.stkId;
	}
	
	public void setStkId(Long value) {
		this.stkId = value;
	}
	
	public java.lang.String getStkCode() {
		return this.stkCode;
	}
	
	public void setStkCode(java.lang.String value) {
		this.stkCode = value;
	}
	
	public java.lang.String getStkName() {
		return this.stkName;
	}
	
	public void setStkName(java.lang.String value) {
		this.stkName = value;
	}
	
	public java.lang.String getStkKeys() {
		return this.stkKeys;
	}
	
	public void setStkKeys(java.lang.String value) {
		this.stkKeys = value;
	}
	
	public java.lang.String getStkStatus() {
		return this.stkStatus;
	}
	
	public void setStkStatus(java.lang.String value) {
		this.stkStatus = value;
	}
	
}

