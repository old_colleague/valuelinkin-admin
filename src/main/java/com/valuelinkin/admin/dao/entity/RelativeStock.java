package com.valuelinkin.admin.dao.entity;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 〈功能详细描述〉
 *
 * @author Nail.zhang
 * @version [V0.1, Jan 9, 2018]
 */

@Table(name="vln_relative_stock")
public class RelativeStock implements java.io.Serializable{
	private static final long serialVersionUID = 5454155825314635342L;
	

	//columns START
		@Id
		@Column(name = "lnk_id")
		private Long lnkId;
		private Long relateId;
		private java.lang.String stkCode;
	//columns END


	public RelativeStock(){
	}

	public RelativeStock(
		Long lnkId
	){
		this.lnkId = lnkId;
	}

	

	public void setLnkId(Long value) {
		this.lnkId = value;
	}
	
	public Long getLnkId() {
		return this.lnkId;
	}
	
	public Long getRelateId() {
		return this.relateId;
	}
	
	public void setRelateId(Long value) {
		this.relateId = value;
	}
	
	public java.lang.String getStkCode() {
		return this.stkCode;
	}
	
	public void setStkCode(java.lang.String value) {
		this.stkCode = value;
	}
	

}

