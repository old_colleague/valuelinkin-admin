package com.valuelinkin.admin.service.core.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jcommon.orm.Page;
import com.jcommon.exception.BusinessException;
import com.jcommon.utils.BeanUtils;
import com.jcommon.result.ResponseResult;

import com.valuelinkin.admin.dao.entity.RawNews;
import com.valuelinkin.admin.dto.RawNewsDTO;
import com.valuelinkin.admin.dao.mapper.RawNewsMapper;
import com.valuelinkin.admin.service.core.RawNewsService;
import com.valuelinkin.admin.service.UidGenerator;
import com.valuelinkin.admin.common.utils.AppConstants;

import tk.mybatis.mapper.entity.Example;

@Service
public class RawNewsServiceImpl implements RawNewsService{

	@Autowired
	private RawNewsMapper rawNewsMapper;
	
	@Autowired
	private UidGenerator uidGenerator;

	public ResponseResult findPage(RawNewsDTO dto) {
		ResponseResult result = new ResponseResult();
		
		RawNews entity = new RawNews();
		BeanUtils.copyProperties(entity, dto);
		
		Page page = new Page();
		page.setPageNo(dto.getPageNo());
		page.setPageSize(dto.getPageSize());
		page.setTotalCount(rawNewsMapper.selectCount(entity));
		
		Example example = new Example(RawNews.class);
		//example.createCriteria().andEqualTo("refId", rowNews.getRefId());
		//example.setOrderByClause("nws_Publish_Date desc");
		
		List<RawNews> list = rawNewsMapper.selectByExampleAndRowBounds(example, new RowBounds(page.getFirst(), page.getPageSize()));
		
		page.setResult(list);
		result.setData(page);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
		
	}	
	
	public ResponseResult getById(Long id) throws BusinessException{
		ResponseResult result = new ResponseResult();
		RawNews entity = rawNewsMapper.selectByPrimaryKey(id);
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}

	
	/** 插入数据 */
	@Transactional
	public ResponseResult save(RawNewsDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		RawNews entity = new RawNews();
		BeanUtils.copyProperties(entity, dto);
		entity.setNwsId(uidGenerator.nextId());
		
		rawNewsMapper.insertSelective(entity);
		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult deleteById(Long id) throws BusinessException{
		ResponseResult result = new ResponseResult();
		rawNewsMapper.deleteByPrimaryKey(id);
		
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult delete(RawNewsDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		
		RawNews entity = new RawNews();
		BeanUtils.copyProperties(entity, dto);
		
		rawNewsMapper.delete(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult deleteByIds(String ids) throws BusinessException{
		ResponseResult result = new ResponseResult();
		String[] items = ids.split(",");
		for (String str : items) {
			Long id = new Long(str);
			rawNewsMapper.deleteByPrimaryKey(id);
		}
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult update(RawNewsDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		RawNews entity = new RawNews();
		BeanUtils.copyProperties(entity, dto);
		rawNewsMapper.updateByPrimaryKeySelective(entity);
		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	public ResponseResult findByExample(RawNewsDTO dto) throws BusinessException {
		ResponseResult result = new ResponseResult();
		
		RawNews entity = new RawNews();
		BeanUtils.copyProperties(entity, dto);
		
		List list =rawNewsMapper.select(entity);
		
		result.setData(list);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	
	public ResponseResult getByExample(RawNewsDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		
		RawNews entity = new RawNews();
		BeanUtils.copyProperties(entity, dto);
		
		entity =rawNewsMapper.selectOne(entity);
		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
		
	}
	
	public void setRawNewsMapper(RawNewsMapper dao) {
		this.rawNewsMapper = dao;
	}
}
