package com.valuelinkin.admin.service.core.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jcommon.orm.Page;
import com.jcommon.exception.BusinessException;
import com.jcommon.utils.BeanUtils;
import com.jcommon.result.ResponseResult;

import com.valuelinkin.admin.dao.entity.SysRolePermission;
import com.valuelinkin.admin.dto.SysRolePermissionDTO;
import com.valuelinkin.admin.dao.mapper.SysRolePermissionMapper;
import com.valuelinkin.admin.service.core.SysRolePermissionService;
import com.valuelinkin.admin.service.UidGenerator;
import com.valuelinkin.admin.common.utils.AppConstants;

import tk.mybatis.mapper.entity.Example;

@Service
public class SysRolePermissionServiceImpl implements SysRolePermissionService{

	@Autowired
	private SysRolePermissionMapper sysRolePermissionMapper;
	
	@Autowired
	private UidGenerator uidGenerator;

	public ResponseResult findPage(SysRolePermissionDTO dto) {
		ResponseResult result = new ResponseResult();
		
		SysRolePermission entity = new SysRolePermission();
		BeanUtils.copyProperties(entity, dto);
		
		Page page = new Page();
		page.setPageNo(dto.getPageNo());
		page.setPageSize(dto.getPageSize());
		page.setTotalCount(sysRolePermissionMapper.selectCount(entity));
		
		Example example = new Example(SysRolePermission.class);
		//example.createCriteria().andEqualTo("refId", rowNews.getRefId());
		//example.setOrderByClause("nws_Publish_Date desc");
		
		List<SysRolePermission> list = sysRolePermissionMapper.selectByExampleAndRowBounds(example, new RowBounds(page.getFirst(), page.getPageSize()));
		
		page.setResult(list);
		result.setData(page);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
		
	}	
	
	public ResponseResult getById(Long id) throws BusinessException{
		ResponseResult result = new ResponseResult();
		SysRolePermission entity = sysRolePermissionMapper.selectByPrimaryKey(id);
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}

	
	/** 插入数据 */
	@Transactional
	public ResponseResult save(SysRolePermissionDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		SysRolePermission entity = new SysRolePermission();
		BeanUtils.copyProperties(entity, dto);
		entity.setId(uidGenerator.nextId());
		
		sysRolePermissionMapper.insertSelective(entity);
		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult deleteById(Long id) throws BusinessException{
		ResponseResult result = new ResponseResult();
		sysRolePermissionMapper.deleteByPrimaryKey(id);
		
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult delete(SysRolePermissionDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		
		SysRolePermission entity = new SysRolePermission();
		BeanUtils.copyProperties(entity, dto);
		
		sysRolePermissionMapper.delete(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult deleteByIds(String ids) throws BusinessException{
		ResponseResult result = new ResponseResult();
		String[] items = ids.split(",");
		for (String str : items) {
			Long id = new Long(str);
			sysRolePermissionMapper.deleteByPrimaryKey(id);
		}
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult update(SysRolePermissionDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		SysRolePermission entity = new SysRolePermission();
		BeanUtils.copyProperties(entity, dto);
		sysRolePermissionMapper.updateByPrimaryKeySelective(entity);
		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	public ResponseResult findByExample(SysRolePermissionDTO dto) throws BusinessException {
		ResponseResult result = new ResponseResult();
		
		SysRolePermission entity = new SysRolePermission();
		BeanUtils.copyProperties(entity, dto);
		
		List list =sysRolePermissionMapper.select(entity);
		
		result.setData(list);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	
	public ResponseResult getByExample(SysRolePermissionDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		
		SysRolePermission entity = new SysRolePermission();
		BeanUtils.copyProperties(entity, dto);
		
		entity =sysRolePermissionMapper.selectOne(entity);
		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
		
	}
	
	public void setSysRolePermissionMapper(SysRolePermissionMapper dao) {
		this.sysRolePermissionMapper = dao;
	}
}
