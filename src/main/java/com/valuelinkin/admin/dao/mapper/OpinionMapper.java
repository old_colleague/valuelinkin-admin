package com.valuelinkin.admin.dao.mapper;


import com.valuelinkin.admin.dto.OpinionDTO;
import com.valuelinkin.admin.dto.OpinionResultDTO;
import org.apache.ibatis.annotations.Mapper;

import com.valuelinkin.admin.dao.entity.Opinion;
import com.valuelinkin.admin.dao.BaseMapper;

import java.util.List;

@Mapper
public interface OpinionMapper extends BaseMapper<Opinion>{

    List<OpinionResultDTO> findPage(OpinionDTO dto);
	

}
