package com.valuelinkin.admin.dto;

import com.valuelinkin.admin.dto.BaseDTO;

public class UserDTO extends BaseDTO {
    private static final long serialVersionUID = 3148176768559230877L;
    

	private Long usrId;
	private java.lang.String usrName;
	private java.lang.String usrPassword;
	private java.lang.String usrNickName;
	private java.lang.String usrPhone;
	private java.lang.String usrOpenId;
	private java.lang.String usrUnionId;
	private java.lang.String usrHeadImg;
	private java.lang.String usrRole;
	private Integer usrStatus;
	private java.util.Date createdOn;
	private java.util.Date modifiedOn;
	private java.lang.String createBy;
	
	private String keywords;

	
	public Long getUsrId() {
		return this.usrId;
	}
	
	public void setUsrId(Long value) {
		this.usrId = value;
	}
	
	public java.lang.String getUsrName() {
		return this.usrName;
	}
	
	public void setUsrName(java.lang.String value) {
		this.usrName = value;
	}
	
	public java.lang.String getUsrPassword() {
		return this.usrPassword;
	}
	
	public void setUsrPassword(java.lang.String value) {
		this.usrPassword = value;
	}
	
	public java.lang.String getUsrNickName() {
		return this.usrNickName;
	}
	
	public void setUsrNickName(java.lang.String value) {
		this.usrNickName = value;
	}
	
	public java.lang.String getUsrPhone() {
		return this.usrPhone;
	}
	
	public void setUsrPhone(java.lang.String value) {
		this.usrPhone = value;
	}
	
	public java.lang.String getUsrOpenId() {
		return this.usrOpenId;
	}
	
	public void setUsrOpenId(java.lang.String value) {
		this.usrOpenId = value;
	}
	
	public java.lang.String getUsrUnionId() {
		return this.usrUnionId;
	}
	
	public void setUsrUnionId(java.lang.String value) {
		this.usrUnionId = value;
	}
	
	public java.lang.String getUsrHeadImg() {
		return this.usrHeadImg;
	}
	
	public void setUsrHeadImg(java.lang.String value) {
		this.usrHeadImg = value;
	}
	
	public java.lang.String getUsrRole() {
		return this.usrRole;
	}
	
	public void setUsrRole(java.lang.String value) {
		this.usrRole = value;
	}
	
	public Integer getUsrStatus() {
		return this.usrStatus;
	}
	
	public void setUsrStatus(Integer value) {
		this.usrStatus = value;
	}
	
	public java.util.Date getCreatedOn() {
		return this.createdOn;
	}
	
	public void setCreatedOn(java.util.Date value) {
		this.createdOn = value;
	}
	
	public java.util.Date getModifiedOn() {
		return this.modifiedOn;
	}
	
	public void setModifiedOn(java.util.Date value) {
		this.modifiedOn = value;
	}
	
	public java.lang.String getCreateBy() {
		return this.createBy;
	}
	
	public void setCreateBy(java.lang.String value) {
		this.createBy = value;
	}

	/**
	 * @return the keywords
	 */
	public String getKeywords() {
		return keywords;
	}

	/**
	 * @param keywords the keywords to set
	 */
	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}
	
}

