package com.valuelinkin.admin.service.core;

import java.util.List;

import com.jcommon.orm.Page;
import com.jcommon.exception.BusinessException;
import com.jcommon.result.ResponseResult;

import com.valuelinkin.admin.dto.NewsLiveDTO;

public interface NewsLiveService {

	ResponseResult findPage(NewsLiveDTO dto) throws BusinessException;
	
	ResponseResult getById(Long id) throws BusinessException;

	ResponseResult save(NewsLiveDTO dto) throws BusinessException;
	
	ResponseResult delete(NewsLiveDTO dto) throws BusinessException ;
	
	ResponseResult deleteById(Long id) throws BusinessException ;
	
	ResponseResult update(NewsLiveDTO dto) throws BusinessException ;
	
	ResponseResult findByExample(NewsLiveDTO dto) throws BusinessException;
	
	ResponseResult getByExample(NewsLiveDTO dto) throws BusinessException;
	
	ResponseResult deleteByIds(String ids) throws BusinessException ;
	

}
