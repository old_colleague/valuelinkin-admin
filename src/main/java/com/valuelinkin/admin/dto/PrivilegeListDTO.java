package com.valuelinkin.admin.dto;

import java.util.List;

public class PrivilegeListDTO extends BaseDTO {
    private static final long serialVersionUID = 3148176768559230877L;


    private Long pvlId;
    private Long parentId;
    private String privilegeName;
    private String privilegeCode;
    private String privilegeDesc;
    private Integer privilegeType;
    private Integer pvlStatus;

    private String pvlUrl;

    List<PrivilegeListDTO> childs;

    public String getPvlUrl() {
        return pvlUrl;
    }

    public void setPvlUrl(String pvlUrl) {
        this.pvlUrl = pvlUrl;
    }



    public String getChecked() {
        return checked;
    }

    public void setChecked(String checked) {
        this.checked = checked;
    }

    private String checked="";

    public List<PrivilegeListDTO> getChilds() {
        return childs;
    }

    public void setChilds(List<PrivilegeListDTO> childs) {
        this.childs = childs;
    }

    public Long getPvlId() {
        return this.pvlId;
    }

    public void setPvlId(Long value) {
        this.pvlId = value;
    }

    public Long getParentId() {
        return this.parentId;
    }

    public void setParentId(Long value) {
        this.parentId = value;
    }

    public String getPrivilegeName() {
        return this.privilegeName;
    }

    public void setPrivilegeName(String value) {
        this.privilegeName = value;
    }

    public String getPrivilegeCode() {
        return this.privilegeCode;
    }

    public void setPrivilegeCode(String value) {
        this.privilegeCode = value;
    }

    public String getPrivilegeDesc() {
        return this.privilegeDesc;
    }

    public void setPrivilegeDesc(String value) {
        this.privilegeDesc = value;
    }

    public Integer getPrivilegeType() {
        return this.privilegeType;
    }

    public void setPrivilegeType(Integer value) {
        this.privilegeType = value;
    }

    public Integer getPvlStatus() {
        return this.pvlStatus;
    }

    public void setPvlStatus(Integer value) {
        this.pvlStatus = value;
    }



}

