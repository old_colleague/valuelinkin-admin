package com.valuelinkin.admin.service.core;

import java.util.List;

import com.jcommon.orm.Page;
import com.jcommon.exception.BusinessException;
import com.jcommon.result.ResponseResult;

import com.valuelinkin.admin.dto.StockFinancialDataDTO;

public interface StockFinancialDataService {

	ResponseResult findPage(StockFinancialDataDTO dto) throws BusinessException;
	
	ResponseResult getById(Long id) throws BusinessException;

	ResponseResult save(StockFinancialDataDTO dto) throws BusinessException;
	
	ResponseResult delete(StockFinancialDataDTO dto) throws BusinessException ;
	
	ResponseResult deleteById(Long id) throws BusinessException ;
	
	ResponseResult update(StockFinancialDataDTO dto) throws BusinessException ;
	
	ResponseResult findByExample(StockFinancialDataDTO dto) throws BusinessException;
	
	ResponseResult getByExample(StockFinancialDataDTO dto) throws BusinessException;
	
	ResponseResult deleteByIds(String ids) throws BusinessException ;
	

}
