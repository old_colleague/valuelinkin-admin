package com.valuelinkin.admin.service.core.impl;

import com.jcommon.exception.BusinessException;
import com.jcommon.orm.Page;
import com.jcommon.result.ResponseResult;
import com.jcommon.utils.BeanUtils;
import com.jcommon.utils.RamdonString;
import com.jcommon.utils.StringUtils;
import com.valuelinkin.admin.common.utils.AppConstants;
import com.valuelinkin.admin.common.utils.StockUtils;
import com.valuelinkin.admin.dao.entity.NewsColumn;
import com.valuelinkin.admin.dao.entity.NewsLive;
import com.valuelinkin.admin.dao.entity.RelativeStock;
import com.valuelinkin.admin.dao.mapper.NewsColumnMapper;
import com.valuelinkin.admin.dao.mapper.NewsLiveMapper;
import com.valuelinkin.admin.dao.mapper.RelativeStockMapper;
import com.valuelinkin.admin.dto.NewsLiveDTO;
import com.valuelinkin.admin.dto.NewsLiveResultDTO;
import com.valuelinkin.admin.dto.StockInfoDTO;
import com.valuelinkin.admin.service.UidGenerator;
import com.valuelinkin.admin.service.core.NewsLiveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.Date;
import java.util.List;

@Service
public class NewsLiveServiceImpl implements NewsLiveService {

    @Autowired
    private NewsLiveMapper newsLiveMapper;

    @Autowired
    private UidGenerator uidGenerator;

    @Autowired
    private NewsColumnMapper newsColumnMapper;

    @Autowired
    private RelativeStockMapper relativeStockMapper;


    public ResponseResult findPage(NewsLiveDTO dto) {
        ResponseResult result = new ResponseResult();

        NewsLive entity = new NewsLive();
        BeanUtils.copyProperties(entity, dto);

        Page page = new Page();
        page.setPageNo(dto.getPageNo());
        page.setPageSize(dto.getPageSize());

        Example example = new Example(NewsLive.class);

        String keywords = StringUtils.trimToNull(dto.getKeywords());
        dto.setKeywords(keywords);
        if (StringUtils.isNotBlank(keywords)) {
            keywords = "%" + keywords + "%";

            dto.setKeywords(keywords);

            example.createCriteria().orLike("nwsTitle", keywords).orLike("nwsStockCode", keywords)
                    .orLike("nwsKeywords", keywords).orLike("nwsBrief", keywords)
                    .orLike("nwsContent", keywords);
        }

        page.setTotalCount(newsLiveMapper.selectCountByExample(example));
        example.setOrderByClause("nws_publish_date desc");

        //List<NewsLive> list = newsLiveMapper.selectByExampleAndRowBounds(example, new RowBounds(page.getFirst(), page.getPageSize()));
        List<NewsLiveResultDTO> list = newsLiveMapper.findPage(dto);


        page.setResult(list);
        result.setData(page);
        result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
        return result;

    }

    public ResponseResult getById(Long id) throws BusinessException {
        ResponseResult result = new ResponseResult();
        NewsLive entity = newsLiveMapper.selectByPrimaryKey(id);
        result.setData(entity);
        result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
        return result;
    }


    /**
     * 插入数据
     */
    @Transactional
    public ResponseResult save(NewsLiveDTO dto) throws BusinessException {
        ResponseResult result = new ResponseResult();
        NewsLive entity = new NewsLive();
        BeanUtils.copyProperties(entity, dto);
        entity.setNwsId(uidGenerator.nextId());
        NewsColumn newsColumn = newsColumnMapper.selectByPrimaryKey(dto.getClmId());
        if (newsColumn != null && newsColumn.getClmCode().equals("JCHG")) {
            entity.setNwsStatus(3);
            entity.setNwsPublishDate(new Date());
        }

        if (newsColumn == null) {
            result.setErrorMessage("参数错误，栏目代码不存在");
            return result;
        }

        if(entity.getNwsPublishDate()==null){
            result.setErrorMessage("开始日期不可为空");
            return result;
        }

        if (newsColumn != null && !newsColumn.getClmCode().equals("TZHD")){
            StockInfoDTO stockInfoDTO = StockUtils.getStockInfo(dto.getNwsStockCode());

            if (stockInfoDTO == null || StringUtils.isBlank(stockInfoDTO.getStkCode())) {
                result.setErrorMessage("股票代码不存在");
                return result;
            }

            String keywords = "【" + stockInfoDTO.getStkCode() + " " + stockInfoDTO.getStkName() + "】";
            entity.setNwsKeywords(keywords);
            entity.setNwsStockCode(stockInfoDTO.getStkCode());

            RelativeStock relativeStock = new RelativeStock();
            relativeStock.setLnkId(uidGenerator.nextId());
            relativeStock.setStkCode(stockInfoDTO.getStkCode());
            relativeStock.setRelateId(entity.getNwsId());
            relativeStockMapper.insertSelective(relativeStock);
        }

        entity.setNwsViewCount(RamdonString.getRamdonInt(100, 2000));
        Date now = new Date();
        entity.setCreatedOn(now);
        entity.setModifiedOn(now);

        newsLiveMapper.insertSelective(entity);

        result.setData(entity);
        result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
        return result;
    }

    @Transactional
    public ResponseResult deleteById(Long id) throws BusinessException {
        ResponseResult result = new ResponseResult();
        newsLiveMapper.deleteByPrimaryKey(id);

        result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
        return result;
    }

    @Transactional
    public ResponseResult delete(NewsLiveDTO dto) throws BusinessException {
        ResponseResult result = new ResponseResult();

        NewsLive entity = new NewsLive();
        BeanUtils.copyProperties(entity, dto);

        newsLiveMapper.delete(entity);
        result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
        return result;
    }

    @Transactional
    public ResponseResult deleteByIds(String ids) throws BusinessException {
        ResponseResult result = new ResponseResult();
        String[] items = ids.split(",");
        for (String str : items) {
            Long id = new Long(str);
            newsLiveMapper.deleteByPrimaryKey(id);
        }
        result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
        return result;
    }

    @Transactional
    public ResponseResult update(NewsLiveDTO dto) throws BusinessException {
        ResponseResult result = new ResponseResult();
        NewsLive entity = new NewsLive();
        BeanUtils.copyProperties(entity, dto);

        NewsColumn newsColumn = newsColumnMapper.selectByPrimaryKey(dto.getClmId());
        if (newsColumn != null && newsColumn.getClmCode().equals("JCHG")) {
            entity.setNwsStatus(3);
            //entity.setc
        }

        entity.setModifiedOn(new Date());
        newsLiveMapper.updateByPrimaryKeySelective(entity);

        result.setData(entity);
        result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
        return result;
    }

    public ResponseResult findByExample(NewsLiveDTO dto) throws BusinessException {
        ResponseResult result = new ResponseResult();

        NewsLive entity = new NewsLive();
        BeanUtils.copyProperties(entity, dto);

        List list = newsLiveMapper.select(entity);

        result.setData(list);
        result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
        return result;
    }


    public ResponseResult getByExample(NewsLiveDTO dto) throws BusinessException {
        ResponseResult result = new ResponseResult();

        NewsLive entity = new NewsLive();
        BeanUtils.copyProperties(entity, dto);

        entity = newsLiveMapper.selectOne(entity);

        result.setData(entity);
        result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
        return result;

    }

    public void setNewsLiveMapper(NewsLiveMapper dao) {
        this.newsLiveMapper = dao;
    }
}
