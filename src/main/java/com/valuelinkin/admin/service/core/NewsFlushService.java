package com.valuelinkin.admin.service.core;

import java.util.List;

import com.jcommon.orm.Page;
import com.jcommon.exception.BusinessException;
import com.jcommon.result.ResponseResult;

import com.valuelinkin.admin.dto.NewsFlushDTO;

public interface NewsFlushService {

	ResponseResult findPage(NewsFlushDTO dto) throws BusinessException;
	
	ResponseResult getById(Long id) throws BusinessException;

	ResponseResult save(NewsFlushDTO dto) throws BusinessException;
	
	ResponseResult delete(NewsFlushDTO dto) throws BusinessException ;
	
	ResponseResult deleteById(Long id) throws BusinessException ;
	
	ResponseResult update(NewsFlushDTO dto) throws BusinessException ;
	
	ResponseResult findByExample(NewsFlushDTO dto) throws BusinessException;
	
	ResponseResult getByExample(NewsFlushDTO dto) throws BusinessException;
	
	ResponseResult deleteByIds(String ids) throws BusinessException ;
	

}
