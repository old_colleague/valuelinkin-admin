package com.valuelinkin.admin.web.controller;

import java.util.List;

import com.jcommon.orm.Page;

import com.valuelinkin.admin.common.utils.AppConstants;
import com.valuelinkin.admin.dto.SysAdminDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.ui.Model;

import com.valuelinkin.admin.dto.SuggestionDTO;
import com.valuelinkin.admin.service.core.SuggestionService;

import com.jcommon.result.ResponseResult;
import com.jcommon.utils.BeanUtils;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping(value="/suggestion") 
public class SuggestionController{
	
	@Autowired
	private SuggestionService suggestionService;
	
	@RequestMapping(value="/index",method=RequestMethod.GET)
	public String index( Model model) throws Exception {
		
		return "/suggestion/list";
	}
	
	/** 
	 * 执行搜索 
	 **/
	@RequestMapping(value="",method=RequestMethod.GET)
	@ResponseBody
	public ResponseResult findPage(SuggestionDTO dto) {
		
		ResponseResult result = new ResponseResult();
		result = this.suggestionService.findPage(dto);
		return result;
	}
	
	
	/** 
	 * 查看对象
	 **/
	@RequestMapping(value="/view/{id}",method=RequestMethod.GET)
	public String getById(@PathVariable Long id, Model model) {
		ResponseResult result = new ResponseResult();
		result = suggestionService.getById(id);
		
		model.addAttribute("result", result);
		return "/suggestion/view";
		
	}
	

	/** 
	 * 进入新增页面
	 **/
	@RequestMapping(value="/create", method=RequestMethod.GET)
	public String create(SuggestionDTO dto, Model model) throws Exception {
		model.addAttribute("suggestion", dto);
		return "/suggestion/create";
	}
	
	/** 
	 * 保存新增对象
	 **/
	@RequestMapping(value="",method=RequestMethod.POST)
	@ResponseBody
	public ResponseResult save(SuggestionDTO dto) {
		ResponseResult result = null;


		result = suggestionService.save(dto);
		return result;
	}
	
	
	/**
	 * 进入更新页面
	 **/
	@RequestMapping(value="/edit/{id}", method=RequestMethod.GET)
	public String edit(@PathVariable Long id, Model model) throws Exception {

		ResponseResult result  = suggestionService.getById(id);
		model.addAttribute("result", result);
		
		return "/suggestion/edit";
	}
	
	/**
	 * 保存更新对象
	 **/
	@RequestMapping(value="/{id}", method=RequestMethod.PUT)
	@ResponseBody
	public ResponseResult update(@PathVariable Long id,SuggestionDTO dto, HttpSession session) throws Exception {
		ResponseResult result = new ResponseResult();
		dto.setSgsId(id);
		SysAdminDTO adminDTO = (SysAdminDTO)session.getAttribute(AppConstants.LOGIN_SESSION);
		dto.setSgsReplyBy(adminDTO.getAdmLoginName());

		result = suggestionService.update(dto);
		return result;
	}
	
	
	/**
	 *删除多个对象
	 **/
	@RequestMapping(value="/{ids}", method=RequestMethod.DELETE)
	@ResponseBody
	public ResponseResult delete(@PathVariable String ids) {
		
		ResponseResult result = null;
		result = suggestionService.deleteByIds(ids);
		return result;
		
	}
	
	public void setSuggestionService(SuggestionService service) {
		this.suggestionService = service;
	}
	
}
