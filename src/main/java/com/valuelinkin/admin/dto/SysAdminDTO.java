package com.valuelinkin.admin.dto;

public class SysAdminDTO extends BaseDTO {
    private static final long serialVersionUID = 3148176768559230877L;
    

	private Long admId;
	private java.lang.String admLoginName;
	private java.lang.String admDisplayName;
	private java.lang.String admPassword;
	private Integer admIsPwdExpired;
	private Integer admStatus;
	private java.lang.String admRemark;
	private java.util.Date createdOn;
	private java.util.Date modifiedOn;
	
	private Long rleId;
	
	private String newPassword;

	
	public Long getAdmId() {
		return this.admId;
	}
	
	public void setAdmId(Long value) {
		this.admId = value;
	}
	
	public java.lang.String getAdmLoginName() {
		return this.admLoginName;
	}
	
	public void setAdmLoginName(java.lang.String value) {
		this.admLoginName = value;
	}
	
	public java.lang.String getAdmDisplayName() {
		return this.admDisplayName;
	}
	
	public void setAdmDisplayName(java.lang.String value) {
		this.admDisplayName = value;
	}
	
	public java.lang.String getAdmPassword() {
		return this.admPassword;
	}
	
	public void setAdmPassword(java.lang.String value) {
		this.admPassword = value;
	}
	
	public Integer getAdmIsPwdExpired() {
		return this.admIsPwdExpired;
	}
	
	public void setAdmIsPwdExpired(Integer value) {
		this.admIsPwdExpired = value;
	}
	
	public Integer getAdmStatus() {
		return this.admStatus;
	}
	
	public void setAdmStatus(Integer value) {
		this.admStatus = value;
	}
	
	public java.lang.String getAdmRemark() {
		return this.admRemark;
	}
	
	public void setAdmRemark(java.lang.String value) {
		this.admRemark = value;
	}
	
	public java.util.Date getCreatedOn() {
		return this.createdOn;
	}
	
	public void setCreatedOn(java.util.Date value) {
		this.createdOn = value;
	}
	
	public java.util.Date getModifiedOn() {
		return this.modifiedOn;
	}
	
	public void setModifiedOn(java.util.Date value) {
		this.modifiedOn = value;
	}

	/**
	 * @return the newPassword
	 */
	public String getNewPassword() {
		return newPassword;
	}

	/**
	 * @param newPassword the newPassword to set
	 */
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	/**
	 * @return the rleId
	 */
	public Long getRleId() {
		return rleId;
	}

	/**
	 * @param rleId the rleId to set
	 */
	public void setRleId(Long rleId) {
		this.rleId = rleId;
	}
	
}

