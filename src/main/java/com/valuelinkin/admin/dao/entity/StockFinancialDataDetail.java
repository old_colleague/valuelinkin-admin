package com.valuelinkin.admin.dao.entity;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 〈功能详细描述〉
 *
 * @author Nail.zhang
 * @version [V0.1, Jan 9, 2018]
 */

@Table(name="vln_stock_financial_data_detail")
public class StockFinancialDataDetail implements java.io.Serializable{
	private static final long serialVersionUID = 5454155825314635342L;
	

	//columns START
		@Id
		@Column(name = "dtl_id")
		private Long dtlId;
		private Long sfdId;
		private java.lang.String dtlTitle;
		private java.lang.String dtlDataValue;
		private java.lang.String dtlDataRate;
		private java.util.Date createdOn;
		private java.util.Date modifiedOn;
	//columns END


	public StockFinancialDataDetail(){
	}

	public StockFinancialDataDetail(
		Long dtlId
	){
		this.dtlId = dtlId;
	}

	

	public void setDtlId(Long value) {
		this.dtlId = value;
	}
	
	public Long getDtlId() {
		return this.dtlId;
	}
	
	public Long getSfdId() {
		return this.sfdId;
	}
	
	public void setSfdId(Long value) {
		this.sfdId = value;
	}
	
	public java.lang.String getDtlTitle() {
		return this.dtlTitle;
	}
	
	public void setDtlTitle(java.lang.String value) {
		this.dtlTitle = value;
	}
	
	public java.lang.String getDtlDataValue() {
		return this.dtlDataValue;
	}
	
	public void setDtlDataValue(java.lang.String value) {
		this.dtlDataValue = value;
	}
	
	public java.lang.String getDtlDataRate() {
		return this.dtlDataRate;
	}
	
	public void setDtlDataRate(java.lang.String value) {
		this.dtlDataRate = value;
	}
	
		
	public java.util.Date getCreatedOn() {
		return this.createdOn;
	}
	
	public void setCreatedOn(java.util.Date value) {
		this.createdOn = value;
	}
	
		
	public java.util.Date getModifiedOn() {
		return this.modifiedOn;
	}
	
	public void setModifiedOn(java.util.Date value) {
		this.modifiedOn = value;
	}
	

}

