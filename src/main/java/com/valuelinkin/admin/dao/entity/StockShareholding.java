package com.valuelinkin.admin.dao.entity;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 〈功能详细描述〉
 *
 * @author Nail.zhang
 * @version [V0.1, Jan 9, 2018]
 */

@Table(name="vln_stock_shareholding")
public class StockShareholding implements java.io.Serializable{
	private static final long serialVersionUID = 5454155825314635342L;
	

	//columns START
		@Id
		@Column(name = "id")
		private Long id;
		private java.lang.String stkCode;
		private java.lang.String sshRealName;
		private Double sshWeight;
		private java.util.Date createdOn;
		private java.util.Date modifiedOn;
	//columns END


	public StockShareholding(){
	}

	public StockShareholding(
		Long id
	){
		this.id = id;
	}

	

	public void setId(Long value) {
		this.id = value;
	}
	
	public Long getId() {
		return this.id;
	}
	
	public java.lang.String getStkCode() {
		return this.stkCode;
	}
	
	public void setStkCode(java.lang.String value) {
		this.stkCode = value;
	}
	
	public java.lang.String getSshRealName() {
		return this.sshRealName;
	}
	
	public void setSshRealName(java.lang.String value) {
		this.sshRealName = value;
	}
	
	public Double getSshWeight() {
		return this.sshWeight;
	}
	
	public void setSshWeight(Double value) {
		this.sshWeight = value;
	}
	
		
	public java.util.Date getCreatedOn() {
		return this.createdOn;
	}
	
	public void setCreatedOn(java.util.Date value) {
		this.createdOn = value;
	}
	
		
	public java.util.Date getModifiedOn() {
		return this.modifiedOn;
	}
	
	public void setModifiedOn(java.util.Date value) {
		this.modifiedOn = value;
	}
	

}

