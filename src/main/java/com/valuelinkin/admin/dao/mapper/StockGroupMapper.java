package com.valuelinkin.admin.dao.mapper;


import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.valuelinkin.admin.dao.BaseMapper;
import com.valuelinkin.admin.dao.entity.StockGroup;
import com.valuelinkin.admin.dto.GroupStockDTO;

@Mapper
public interface StockGroupMapper extends BaseMapper<StockGroup>{
	
	List<GroupStockDTO> findGroupStock(Long grpId);
	
	/**
	 * 	获取分组中最大的排序索引
	 * @param grpId
	 * @return
	 */
	Integer selectMaxSortIndex(Long grpId);

}
