package com.valuelinkin.admin.common.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jcommon.httpclient.HttpResponseResult;
import com.jcommon.httpclient.HttpUtils;
import com.jcommon.json.JSONUtils;
import com.jcommon.utils.StringUtils;
import com.valuelinkin.admin.dto.StockInfoDTO;

/**
 * 
 * @atuhor: nail.zhang
 * @description:
 * @date: Created in  2019年2月3日
 */
public class StockUtils {
	
	
	private static final Logger logger = LoggerFactory.getLogger(StockUtils.class);

	
	public static String GET_STOCK_INFO_URL_SINA="http://hq.sinajs.cn/list=";
	
	public static StockInfoDTO getStockInfo(String stkCode) {
		
		StockInfoDTO stockInfoDTO = new StockInfoDTO();
		String url = GET_STOCK_INFO_URL_SINA+addStockPrefix(stkCode);
		try {
			HttpResponseResult result = HttpUtils.doGet(url, null, null,"gbk");
			String str = result.getContent();
			//logger.debug(str);

			String[] stkInfoArray = StringUtils.split(str, "\"");
			stkInfoArray = StringUtils.split(stkInfoArray[1],",");
			for (int i = 0; i < stkInfoArray.length; i++) {
				stockInfoDTO.setStkCode(stkCode);
				stockInfoDTO.setStkName(stkInfoArray[0]);
				
			}
			
		} catch (Exception e) {
			logger.error("获取股票信息错误", e);

		}
		
		return stockInfoDTO;
		
	}
	
	public static String addStockPrefix(String stkCode) {
		
		return stkCode.startsWith("6")?"sh"+stkCode:"sz"+stkCode;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		StockInfoDTO stockInfoDTO = getStockInfo("601006");
		
		System.out.println(JSONUtils.toJSONString(stockInfoDTO));

	}

}
