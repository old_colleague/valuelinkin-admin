package com.valuelinkin.admin.service.core;

import java.util.List;

import com.jcommon.orm.Page;
import com.jcommon.exception.BusinessException;
import com.jcommon.result.ResponseResult;

import com.valuelinkin.admin.dto.AdsDTO;

public interface AdsService {

	ResponseResult findPage(AdsDTO dto) throws BusinessException;
	
	ResponseResult getById(Long id) throws BusinessException;

	ResponseResult save(AdsDTO dto) throws BusinessException;
	
	ResponseResult delete(AdsDTO dto) throws BusinessException ;
	
	ResponseResult deleteById(Long id) throws BusinessException ;
	
	ResponseResult update(AdsDTO dto) throws BusinessException ;
	
	ResponseResult findByExample(AdsDTO dto) throws BusinessException;
	
	ResponseResult getByExample(AdsDTO dto) throws BusinessException;
	
	ResponseResult deleteByIds(String ids) throws BusinessException ;
	

}
