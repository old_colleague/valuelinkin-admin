package com.valuelinkin.admin.dto;

import com.valuelinkin.admin.dto.BaseDTO;

public class StockManagerDTO extends BaseDTO {
    private static final long serialVersionUID = 3148176768559230877L;
    

	private Long id;
	private java.lang.String stkCode;
	private java.lang.String mgrRealName;
	private java.lang.String mgrPosition;
	private java.lang.String mgrHeadPic;
	private java.lang.String mgrResume;
	private Integer mgrSortIndex;
	private java.util.Date createdOn;
	private java.util.Date modifiedOn;

	public Integer getMgrSortIndex() {
		return mgrSortIndex;
	}

	public void setMgrSortIndex(Integer mgrSortIndex) {
		this.mgrSortIndex = mgrSortIndex;
	}

	public Long getId() {
		return this.id;
	}
	
	public void setId(Long value) {
		this.id = value;
	}
	
	public java.lang.String getStkCode() {
		return this.stkCode;
	}
	
	public void setStkCode(java.lang.String value) {
		this.stkCode = value;
	}
	
	public java.lang.String getMgrRealName() {
		return this.mgrRealName;
	}
	
	public void setMgrRealName(java.lang.String value) {
		this.mgrRealName = value;
	}
	
	public java.lang.String getMgrPosition() {
		return this.mgrPosition;
	}
	
	public void setMgrPosition(java.lang.String value) {
		this.mgrPosition = value;
	}
	
	public java.lang.String getMgrHeadPic() {
		return this.mgrHeadPic;
	}
	
	public void setMgrHeadPic(java.lang.String value) {
		this.mgrHeadPic = value;
	}
	
	public java.lang.String getMgrResume() {
		return this.mgrResume;
	}
	
	public void setMgrResume(java.lang.String value) {
		this.mgrResume = value;
	}
	
	public java.util.Date getCreatedOn() {
		return this.createdOn;
	}
	
	public void setCreatedOn(java.util.Date value) {
		this.createdOn = value;
	}
	
	public java.util.Date getModifiedOn() {
		return this.modifiedOn;
	}
	
	public void setModifiedOn(java.util.Date value) {
		this.modifiedOn = value;
	}
	
}

