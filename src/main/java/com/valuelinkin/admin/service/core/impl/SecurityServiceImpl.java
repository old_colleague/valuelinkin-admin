package com.valuelinkin.admin.service.core.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jcommon.orm.Page;
import com.jcommon.exception.BusinessException;
import com.jcommon.utils.BeanUtils;
import com.jcommon.result.ResponseResult;

import com.valuelinkin.admin.dao.entity.Security;
import com.valuelinkin.admin.dto.SecurityDTO;
import com.valuelinkin.admin.dao.mapper.SecurityMapper;
import com.valuelinkin.admin.service.core.SecurityService;
import com.valuelinkin.admin.service.UidGenerator;
import com.valuelinkin.admin.common.utils.AppConstants;

import tk.mybatis.mapper.entity.Example;

@Service
public class SecurityServiceImpl implements SecurityService{

	@Autowired
	private SecurityMapper securityMapper;
	
	@Autowired
	private UidGenerator uidGenerator;

	public ResponseResult findPage(SecurityDTO dto) {
		ResponseResult result = new ResponseResult();
		
		Security entity = new Security();
		BeanUtils.copyProperties(entity, dto);
		
		Page page = new Page();
		page.setPageNo(dto.getPageNo());
		page.setPageSize(dto.getPageSize());
		page.setTotalCount(securityMapper.selectCount(entity));
		
		Example example = new Example(Security.class);
		//example.createCriteria().andEqualTo("refId", rowNews.getRefId());
		//example.setOrderByClause("nws_Publish_Date desc");
		
		List<Security> list = securityMapper.selectByExampleAndRowBounds(example, new RowBounds(page.getFirst(), page.getPageSize()));
		
		page.setResult(list);
		result.setData(page);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
		
	}	
	
	public ResponseResult getById(Long id) throws BusinessException{
		ResponseResult result = new ResponseResult();
		Security entity = securityMapper.selectByPrimaryKey(id);
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}

	
	/** 插入数据 */
	@Transactional
	public ResponseResult save(SecurityDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		Security entity = new Security();
		BeanUtils.copyProperties(entity, dto);
		entity.setStkId(uidGenerator.nextId());
		Date now = new Date();
		entity.setCreatedOn(now);
		entity.setModifiedOn(now);
		securityMapper.insertSelective(entity);
		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult deleteById(Long id) throws BusinessException{
		ResponseResult result = new ResponseResult();
		securityMapper.deleteByPrimaryKey(id);
		
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult delete(SecurityDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		
		Security entity = new Security();
		BeanUtils.copyProperties(entity, dto);
		
		securityMapper.delete(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult deleteByIds(String ids) throws BusinessException{
		ResponseResult result = new ResponseResult();
		String[] items = ids.split(",");
		for (String str : items) {
			Long id = new Long(str);
			securityMapper.deleteByPrimaryKey(id);
		}
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult update(SecurityDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		Security entity = new Security();
		BeanUtils.copyProperties(entity, dto);
		Date now = new Date();
		entity.setModifiedOn(now);
		
		securityMapper.updateByPrimaryKeySelective(entity);
		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	public ResponseResult findByExample(SecurityDTO dto) throws BusinessException {
		ResponseResult result = new ResponseResult();
		
		Security entity = new Security();
		BeanUtils.copyProperties(entity, dto);
		
		List list =securityMapper.select(entity);
		
		result.setData(list);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	
	public ResponseResult getByExample(SecurityDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		
		Security entity = new Security();
		BeanUtils.copyProperties(entity, dto);
		
		entity =securityMapper.selectOne(entity);
		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
		
	}
	
	public void setSecurityMapper(SecurityMapper dao) {
		this.securityMapper = dao;
	}
}
