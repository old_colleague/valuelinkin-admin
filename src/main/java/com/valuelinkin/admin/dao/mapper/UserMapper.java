package com.valuelinkin.admin.dao.mapper;


import com.valuelinkin.admin.dto.AppMessageDTO;
import org.apache.ibatis.annotations.Mapper;

import com.valuelinkin.admin.dao.entity.User;
import com.valuelinkin.admin.dao.BaseMapper;

import java.util.List;

@Mapper
public interface UserMapper extends BaseMapper<User>{

    List<Long> findAllUserIdList(AppMessageDTO dto);
	

}
