package com.valuelinkin.admin.dao.entity;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 〈功能详细描述〉
 *
 * @author Nail.zhang
 * @version [V0.1, Jan 9, 2018]
 */

@Table(name="vln_group")
public class Group implements java.io.Serializable{
	private static final long serialVersionUID = 5454155825314635342L;
	

	//columns START
		@Id
		@Column(name = "grp_id")
		private Long grpId;
		private java.lang.String grpName;
		private java.lang.String grpDesc;
		private Integer grpStatus;
		private Integer grpSortIndex;
		private java.util.Date createdOn;
		private java.util.Date modifiedOn;
	//columns END


	public Group(){
	}

	public Group(
		Long grpId
	){
		this.grpId = grpId;
	}

	

	public void setGrpId(Long value) {
		this.grpId = value;
	}
	
	public Long getGrpId() {
		return this.grpId;
	}
	
	public java.lang.String getGrpName() {
		return this.grpName;
	}
	
	public void setGrpName(java.lang.String value) {
		this.grpName = value;
	}
	
	public java.lang.String getGrpDesc() {
		return this.grpDesc;
	}
	
	public void setGrpDesc(java.lang.String value) {
		this.grpDesc = value;
	}
	
	public Integer getGrpStatus() {
		return this.grpStatus;
	}
	
	public void setGrpStatus(Integer value) {
		this.grpStatus = value;
	}
	
	public Integer getGrpSortIndex() {
		return this.grpSortIndex;
	}
	
	public void setGrpSortIndex(Integer value) {
		this.grpSortIndex = value;
	}
	
		
	public java.util.Date getCreatedOn() {
		return this.createdOn;
	}
	
	public void setCreatedOn(java.util.Date value) {
		this.createdOn = value;
	}
	
		
	public java.util.Date getModifiedOn() {
		return this.modifiedOn;
	}
	
	public void setModifiedOn(java.util.Date value) {
		this.modifiedOn = value;
	}
	

}

