package com.valuelinkin.admin.dao.entity;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 〈功能详细描述〉
 *
 * @author Nail.zhang
 * @version [V0.1, Jan 9, 2018]
 */

@Table(name="vln_news_flush")
public class NewsFlush implements java.io.Serializable{
	private static final long serialVersionUID = 5454155825314635342L;
	

	//columns START
		@Id
		@Column(name = "nws_id")
		private Long nwsId;
		private Long clmId;
		private java.lang.String nwsTitle;
		private java.lang.String nwsKeywords;
		private java.lang.String nwsBrief;
		private java.lang.String nwsContent;
		private java.lang.String nwsSource;
		private java.util.Date nwsPublishDate;
		private Integer nwsStatus;
		private Integer nwsViewCount;
		private Integer nwsStickTop;
		private java.lang.String nwsColor;
		private java.util.Date createdOn;
		private java.util.Date modifiedOn;
	//columns END


	public NewsFlush(){
	}

	public NewsFlush(
		Long nwsId
	){
		this.nwsId = nwsId;
	}

	

	public void setNwsId(Long value) {
		this.nwsId = value;
	}
	
	public Long getNwsId() {
		return this.nwsId;
	}
	
	public Long getClmId() {
		return this.clmId;
	}
	
	public void setClmId(Long value) {
		this.clmId = value;
	}
	
	public java.lang.String getNwsTitle() {
		return this.nwsTitle;
	}
	
	public void setNwsTitle(java.lang.String value) {
		this.nwsTitle = value;
	}
	
	public java.lang.String getNwsKeywords() {
		return this.nwsKeywords;
	}
	
	public void setNwsKeywords(java.lang.String value) {
		this.nwsKeywords = value;
	}
	
	public java.lang.String getNwsBrief() {
		return this.nwsBrief;
	}
	
	public void setNwsBrief(java.lang.String value) {
		this.nwsBrief = value;
	}
	
	public java.lang.String getNwsContent() {
		return this.nwsContent;
	}
	
	public void setNwsContent(java.lang.String value) {
		this.nwsContent = value;
	}
	
	public java.lang.String getNwsSource() {
		return this.nwsSource;
	}
	
	public void setNwsSource(java.lang.String value) {
		this.nwsSource = value;
	}
	
	public Integer getNwsStatus() {
		return this.nwsStatus;
	}
	
	public void setNwsStatus(Integer value) {
		this.nwsStatus = value;
	}
	
	public Integer getNwsViewCount() {
		return this.nwsViewCount;
	}
	
	public void setNwsViewCount(Integer value) {
		this.nwsViewCount = value;
	}
	
	public Integer getNwsStickTop() {
		return this.nwsStickTop;
	}
	
	public void setNwsStickTop(Integer value) {
		this.nwsStickTop = value;
	}
	
	public java.lang.String getNwsColor() {
		return this.nwsColor;
	}
	
	public void setNwsColor(java.lang.String value) {
		this.nwsColor = value;
	}
	
		
	public java.util.Date getCreatedOn() {
		return this.createdOn;
	}
	
	public void setCreatedOn(java.util.Date value) {
		this.createdOn = value;
	}
	
		
	public java.util.Date getModifiedOn() {
		return this.modifiedOn;
	}
	
	public void setModifiedOn(java.util.Date value) {
		this.modifiedOn = value;
	}

	/**
	 * @return the nwsPublishDate
	 */
	public java.util.Date getNwsPublishDate() {
		return nwsPublishDate;
	}

	/**
	 * @param nwsPublishDate the nwsPublishDate to set
	 */
	public void setNwsPublishDate(java.util.Date nwsPublishDate) {
		this.nwsPublishDate = nwsPublishDate;
	}
	

}

