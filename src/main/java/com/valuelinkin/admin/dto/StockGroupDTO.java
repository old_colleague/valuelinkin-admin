package com.valuelinkin.admin.dto;

import java.util.List;

public class StockGroupDTO extends BaseDTO {
    private static final long serialVersionUID = 3148176768559230877L;
    

	private Long id;
	private Long stkId;
	private Long grpId;
	private Integer sortIndex;
	
	private List<Long> itmId;

	
	public Long getId() {
		return this.id;
	}
	
	public void setId(Long value) {
		this.id = value;
	}
	
	public Long getStkId() {
		return this.stkId;
	}
	
	public void setStkId(Long value) {
		this.stkId = value;
	}
	
	public Long getGrpId() {
		return this.grpId;
	}
	
	public void setGrpId(Long value) {
		this.grpId = value;
	}
	
	public Integer getSortIndex() {
		return this.sortIndex;
	}
	
	public void setSortIndex(Integer value) {
		this.sortIndex = value;
	}


	/**
	 * @return the itmId
	 */
	public List<Long> getItmId() {
		return itmId;
	}

	/**
	 * @param itmId the itmId to set
	 */
	public void setItmId(List<Long> itmId) {
		this.itmId = itmId;
	}

	
	
}

