package com.valuelinkin.admin.dto;

import com.valuelinkin.admin.dto.BaseDTO;

public class AnswerDTO extends BaseDTO {
    private static final long serialVersionUID = 3148176768559230877L;
    

	private Long ansId;
	private Long qsnId;
	private Long usrId;
	private java.lang.String ansContent;
	private Integer ansStatus;
	private Long ansReplier;

	
	public Long getAnsId() {
		return this.ansId;
	}
	
	public void setAnsId(Long value) {
		this.ansId = value;
	}
	
	public Long getQsnId() {
		return this.qsnId;
	}
	
	public void setQsnId(Long value) {
		this.qsnId = value;
	}
	
	public Long getUsrId() {
		return this.usrId;
	}
	
	public void setUsrId(Long value) {
		this.usrId = value;
	}
	
	public java.lang.String getAnsContent() {
		return this.ansContent;
	}
	
	public void setAnsContent(java.lang.String value) {
		this.ansContent = value;
	}
	
	public Integer getAnsStatus() {
		return this.ansStatus;
	}
	
	public void setAnsStatus(Integer value) {
		this.ansStatus = value;
	}
	
	public Long getAnsReplier() {
		return this.ansReplier;
	}
	
	public void setAnsReplier(Long value) {
		this.ansReplier = value;
	}
	
}

