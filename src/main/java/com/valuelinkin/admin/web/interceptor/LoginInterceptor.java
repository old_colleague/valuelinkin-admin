/**
 * 
 */
package com.valuelinkin.admin.web.interceptor;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.jcommon.json.JSONUtils;
import com.jcommon.result.ResponseResult;
import com.jcommon.utils.StringUtils;
import com.valuelinkin.admin.common.utils.AppConstants;

/**
 * 
 * @author nail
 *
 */
@Component
public class LoginInterceptor implements HandlerInterceptor {


	private static Logger logger = LoggerFactory.getLogger(LoginInterceptor.class);

	@Override
	public void afterCompletion(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, Exception arg3)
			throws Exception {

	}

	@Override
	public void postHandle(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, ModelAndView arg3)
			throws Exception {

	}

	@Override
	public boolean preHandle(HttpServletRequest req, HttpServletResponse res, Object arg2) throws Exception {
		HttpSession session = req.getSession(true);
		// 从session 里面获取用户名的信息
		Object obj = session.getAttribute(AppConstants.LOGIN_SESSION);
		
		String contextPath = req.getContextPath();
		String servletPath = req.getServletPath();
		logger.info("contextPath="+contextPath +" servletPath="+servletPath);
		
		//logger.info("contextPath=" + contextPath + " servletPath=" + servletPath);
		// System.out.println("========================================contextPath="+contextPath
		// +" servletPath="+servletPath);
		// 判断如果没有取到用户信息，就跳转到登陆页面，提示用户进行登陆
		if (obj == null) {
			if(isAjax(req)){
				ResponseResult result = new ResponseResult();
				result.setErrorCode(AppConstants.RETURN_SESSION_EXPIRED_CODE);
				result.setErrorMessage(getMessage(req, "info.session_expired", null));
				String json = JSONUtils.toJSONString(result);

				String jsoncallback = req.getParameter("jsoncallback");
				if (StringUtils.trimToNull(jsoncallback) != null) {
					json = jsoncallback + "(" + json + ")";
				}
				try {
					logger.info("session过期失效  "+JSONUtils.toJSONString(obj));
					res.setCharacterEncoding("UTF-8");
					res.setContentType("text/html; charset=utf-8");
					res.getWriter().write(json);
				} catch (IOException e) {
					logger.error("返回Json结果错误 " + e.getMessage(), e);
				}
			}else{
				res.sendRedirect("/login");
			}
			
			return false;

			
		}
		return true;
	}
	
	boolean isAjax(HttpServletRequest request){
		logger.info(request.getHeader("accept"));
		String xRequestedWith=request.getHeader("x-requested-with");
		xRequestedWith = xRequestedWith==null?request.getHeader("X-Requested-With"):xRequestedWith;
		
	    return  xRequestedWith != null  &&  "XMLHttpRequest".equalsIgnoreCase(xRequestedWith);
	}
	

	public String getMessage(HttpServletRequest request, String key, Object[] args) {
		WebApplicationContext ac = RequestContextUtils.findWebApplicationContext(request);
		return ac.getMessage(key, args, Locale.CHINA);
	}


}
