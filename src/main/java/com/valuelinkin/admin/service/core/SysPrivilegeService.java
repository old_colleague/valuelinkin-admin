package com.valuelinkin.admin.service.core;

import java.util.Collection;
import java.util.List;

import com.jcommon.orm.Page;
import com.jcommon.exception.BusinessException;
import com.jcommon.result.ResponseResult;

import com.valuelinkin.admin.dto.PrivilegeListDTO;
import com.valuelinkin.admin.dto.SysPrivilegeDTO;

public interface SysPrivilegeService {

	ResponseResult findPage(SysPrivilegeDTO dto) throws BusinessException;
	
	ResponseResult getById(Long id) throws BusinessException;

	ResponseResult save(SysPrivilegeDTO dto) throws BusinessException;
	
	ResponseResult delete(SysPrivilegeDTO dto) throws BusinessException ;
	
	ResponseResult deleteById(Long id) throws BusinessException ;
	
	ResponseResult update(SysPrivilegeDTO dto) throws BusinessException ;
	
	ResponseResult findByExample(SysPrivilegeDTO dto) throws BusinessException;
	
	ResponseResult getByExample(SysPrivilegeDTO dto) throws BusinessException;
	
	ResponseResult deleteByIds(String ids) throws BusinessException ;

	ResponseResult<Collection<PrivilegeListDTO>> findAllPvlList( ) throws BusinessException;

	ResponseResult<Collection<PrivilegeListDTO>> findRolePvlToEdit(Long rleId) throws BusinessException;

	ResponseResult<Collection<PrivilegeListDTO>> findRolePvlList(Long rleId) throws BusinessException;

	

}
