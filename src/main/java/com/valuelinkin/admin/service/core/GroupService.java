package com.valuelinkin.admin.service.core;

import java.util.List;

import com.jcommon.orm.Page;
import com.jcommon.exception.BusinessException;
import com.jcommon.result.ResponseResult;

import com.valuelinkin.admin.dto.GroupDTO;

public interface GroupService {

	ResponseResult findPage(GroupDTO dto) throws BusinessException;
	
	ResponseResult getById(Long id) throws BusinessException;

	ResponseResult save(GroupDTO dto) throws BusinessException;
	
	ResponseResult delete(GroupDTO dto) throws BusinessException ;
	
	ResponseResult deleteById(Long id) throws BusinessException ;
	
	ResponseResult update(GroupDTO dto) throws BusinessException ;
	
	ResponseResult findByExample(GroupDTO dto) throws BusinessException;
	
	ResponseResult getByExample(GroupDTO dto) throws BusinessException;
	
	ResponseResult deleteByIds(String ids) throws BusinessException ;
	

}
