package com.valuelinkin.admin.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.valuelinkin.admin.service.UidGenerator;
import com.jcommon.snowflake.SnowFlakeGenerator;

@Service
public class UidGeneratorImpl implements UidGenerator {

	
	@Value("${snowflake.worker.id}")
	private long workerId;
	
	@Value("${snowflake.datacenter.id}")
	private long datacenterId;
	
	@Override
	public Long nextId() {
		return SnowFlakeGenerator.getInstance(workerId, datacenterId).nextId();
	}

	public long getWorkerId() {
		return workerId;
	}

	public void setWorkerId(long workerId) {
		this.workerId = workerId;
	}

	public long getDatacenterId() {
		return datacenterId;
	}

	public void setDatacenterId(long datacenterId) {
		this.datacenterId = datacenterId;
	}

}
