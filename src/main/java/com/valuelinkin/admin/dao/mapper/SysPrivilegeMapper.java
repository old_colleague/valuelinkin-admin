package com.valuelinkin.admin.dao.mapper;


import com.valuelinkin.admin.dto.PrivilegeListDTO;
import org.apache.ibatis.annotations.Mapper;

import com.valuelinkin.admin.dao.entity.SysPrivilege;
import com.valuelinkin.admin.dao.BaseMapper;

import java.util.List;

@Mapper
public interface SysPrivilegeMapper extends BaseMapper<SysPrivilege>{

    List<PrivilegeListDTO> findRolePvlList(Long rleId);

}
