package com.valuelinkin.admin.service.core.impl;

import java.util.Date;
import java.util.List;

import com.valuelinkin.admin.common.utils.StockUtils;
import com.valuelinkin.admin.dao.entity.*;
import com.valuelinkin.admin.dao.mapper.*;
import com.valuelinkin.admin.dto.StockInfoDTO;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jcommon.exception.BusinessException;
import com.jcommon.orm.Page;
import com.jcommon.result.ResponseResult;
import com.jcommon.utils.BeanUtils;
import com.jcommon.utils.StringUtils;
import com.valuelinkin.admin.common.utils.AppConstants;
import com.valuelinkin.admin.dto.StockDTO;
import com.valuelinkin.admin.dto.StockDetailDTO;
import com.valuelinkin.admin.service.UidGenerator;
import com.valuelinkin.admin.service.core.StockService;

import tk.mybatis.mapper.entity.Example;

@Service
public class StockServiceImpl implements StockService{

	@Autowired
	private StockMapper stockMapper;
	
	@Autowired
	private UidGenerator uidGenerator;
	
	@Autowired
	private StockShareholdingMapper stockShareholdingMapper;
	
	@Autowired
	private StockFinancialDataMapper stockFinancialDataMapper;
	
	@Autowired
	private StockFinancialDataDetailMapper stockFinancialDataDetailMapper;
	
	@Autowired
	private GroupMapper groupMapper;

	@Autowired
	private SecurityMapper securityMapper;
	
	@Autowired
	private StockGroupMapper stockGroupMapper;

	public ResponseResult findPage(StockDTO dto) {
		ResponseResult result = new ResponseResult();
		
		Stock entity = new Stock();
		BeanUtils.copyProperties(entity, dto);
		
		Page page = new Page();
		page.setPageNo(dto.getPageNo());
		page.setPageSize(dto.getPageSize());
		page.setTotalCount(stockMapper.selectCount(entity));
		
		Example example = new Example(Stock.class);
		String keywords = StringUtils.trimToNull(dto.getKeywords());
		
		if(StringUtils.isNotBlank(keywords)) {
			keywords = "%"+keywords+"%";
			example.createCriteria().orLike("stkCode", keywords).orLike("stkName", keywords);
		}
		
		List<Stock> list = stockMapper.selectByExampleAndRowBounds(example, new RowBounds(page.getFirst(), page.getPageSize()));
		
		page.setResult(list);
		result.setData(page);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
		
	}	
	
	public ResponseResult getById(Long id) throws BusinessException{
		ResponseResult result = new ResponseResult();
		Stock entity = stockMapper.selectByPrimaryKey(id);
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	public ResponseResult getStockDetails(Long id) throws BusinessException{
		ResponseResult result = new ResponseResult();
		Stock entity = stockMapper.selectByPrimaryKey(id);
		StockDetailDTO stockDetails = new StockDetailDTO();
		BeanUtils.copyProperties(stockDetails, entity);
		
		StockFinancialData stockFinancialData = new StockFinancialData();
		Example example = new Example(StockFinancialData.class);
		example.createCriteria().andEqualTo("stkCode", entity.getStkCode());
		example.orderBy("created_On desc ");
		//stockFinancialData.setStkCode(entity.getStkCode());
		List<StockFinancialData> stockFDList = stockFinancialDataMapper.selectByExample(example);
		if(stockFDList!=null && stockFDList.size()>0) {
			stockFinancialData = stockFDList.get(0);
		}
		
		stockDetails.setSdfDataTitle(stockFinancialData.getSdfDataTitle());
		StockFinancialDataDetail stockFinancialDataDetail = new StockFinancialDataDetail();
		stockFinancialDataDetail.setSfdId(stockFinancialData.getSfdId());
		List<StockFinancialDataDetail> stkFinDataDetails = stockFinancialDataDetailMapper.select(stockFinancialDataDetail);
		stockDetails.setStkFinDataDetails(stkFinDataDetails);
		
		StockShareholding stockShareholding = new StockShareholding();
		stockShareholding.setStkCode(entity.getStkCode());
		List<StockShareholding> stkShareholdings = stockShareholdingMapper.select(stockShareholding);
		stockDetails.setStkShareholdings(stkShareholdings);
		
		StockGroup stockGroup = new StockGroup();
		stockGroup.setStkId(entity.getStkId());
		List<StockGroup> groups = stockGroupMapper.select(stockGroup);
		
		stockDetails.setGroups(groups);
		
		stockDetails.setGroupList(groupMapper.selectAll());
		
		result.setData(stockDetails);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}

	
	/** 插入数据 */
	@Transactional
	public ResponseResult save(StockDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		Stock entity = new Stock();
		Date now = new Date();
		if(StringUtils.isBlank(dto.getStkCode())) {
			result.setErrorCode(AppConstants.RETURN_FAIL_CODE);
			result.setErrorMessage("股票代码不能为空。");
			return result; 
		}
		
		if(StringUtils.isBlank(dto.getStkLogo())) {
			result.setErrorCode(AppConstants.RETURN_FAIL_CODE);
			result.setErrorMessage("股票logo不可为空。");
			return result; 
		}
		
		if(StringUtils.isBlank(dto.getSdfDataTitle())) {
			result.setErrorCode(AppConstants.RETURN_FAIL_CODE);
			result.setErrorMessage("个股财务报表标题不可为空。");
			return result; 
		}
		entity.setStkCode(dto.getStkCode());
		entity = stockMapper.selectOne(entity);
		if(entity!=null) {
			result.setErrorCode(AppConstants.RETURN_FAIL_CODE);
			result.setErrorMessage("股票已经存在，请不要重复添加");
			return result; 
		}else {
			entity = new Stock();
		}

		StockInfoDTO stockInfoDTO = StockUtils.getStockInfo(dto.getStkCode());
		if(stockInfoDTO==null){
			result.setErrorCode(AppConstants.RETURN_FAIL_CODE);
			result.setErrorMessage("股票代码不存在");
			return result;
		}
		Security security = new Security();
		security.setStkCode(stockInfoDTO.getStkCode());
		security = securityMapper.selectOne(security);
		if(security ==null){
			security = new Security();
			security.setStkCode(stockInfoDTO.getStkCode());
			security.setStkName(stockInfoDTO.getStkName());
			security.setCreatedOn(now);
			security.setModifiedOn(now);
			security.setStkStatus("ACTIVE");
			securityMapper.insertSelective(security);
		}
		
		BeanUtils.copyProperties(entity, dto);
		entity.setStkId(uidGenerator.nextId());
		entity.setStkName(stockInfoDTO.getStkName());
		
		Integer sortIndex = stockGroupMapper.selectMaxSortIndex(dto.getGrpId());
		sortIndex = sortIndex==null?1:sortIndex+1;
		

		entity.setCreatedOn(now);
		entity.setModifiedOn(now);
		stockMapper.insertSelective(entity);
		
		List<String> dataRateList = dto.getDtlDataRate();
		List<String> dataValueList = dto.getDtlDataValue();
		List<String> dataTitleList = dto.getDtlTitle();
		
		List<String> sshRealNameList = dto.getSshRealName();
		List<Double> sshWeightList = dto.getSshWeight();
		for (int i = 0; i < sshRealNameList.size(); i++) {
			
			StockShareholding stockShareholding = new StockShareholding();
			stockShareholding.setId(uidGenerator.nextId());
			stockShareholding.setStkCode(dto.getStkCode());
			if(StringUtils.isBlank(sshRealNameList.get(i))) {
				continue;
			}
			stockShareholding.setSshRealName(sshRealNameList.get(i));
			stockShareholding.setSshWeight(sshWeightList.get(i));
			stockShareholding.setCreatedOn(now);
			stockShareholding.setModifiedOn(now);
			stockShareholdingMapper.insertSelective(stockShareholding);
		}
		
		StockFinancialData stockFinancialData = new StockFinancialData();
		stockFinancialData.setSfdId(uidGenerator.nextId());
		stockFinancialData.setSdfDataTitle(dto.getSdfDataTitle());
		stockFinancialData.setStkCode(dto.getStkCode());
		stockFinancialData.setCreatedOn(now);
		stockFinancialData.setModifiedOn(now);
		stockFinancialDataMapper.insertSelective(stockFinancialData);
		
		for (int i = 0; i < dataRateList.size(); i++) {
			StockFinancialDataDetail stockFinancialDataDetail = new StockFinancialDataDetail();
			if(StringUtils.isBlank(dataTitleList.get(i)) || StringUtils.isBlank(dataValueList.get(i)) ||
					 StringUtils.isBlank(dataRateList.get(i))) {
				continue;
			}
			stockFinancialDataDetail.setDtlId(uidGenerator.nextId());
			stockFinancialDataDetail.setSfdId(stockFinancialData.getSfdId());
			
			stockFinancialDataDetail.setDtlTitle(dataTitleList.get(i));
			stockFinancialDataDetail.setDtlDataValue(dataValueList.get(i));
			stockFinancialDataDetail.setDtlDataRate(dataRateList.get(i));
			stockFinancialDataDetail.setCreatedOn(now);
			stockFinancialDataDetail.setModifiedOn(now);
			
			stockFinancialDataDetailMapper.insertSelective(stockFinancialDataDetail);
		}
		
		if(dto.getGrpId()!=null) {
			StockGroup stockGroup = new StockGroup();
			stockGroup.setId(uidGenerator.nextId());
			stockGroup.setStkId(entity.getStkId());
			stockGroup.setGrpId(dto.getGrpId());
			stockGroup.setSortIndex(sortIndex);
			stockGroupMapper.insertSelective(stockGroup);
		}
		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult deleteById(Long id) throws BusinessException{
		ResponseResult result = new ResponseResult();
		stockMapper.deleteByPrimaryKey(id);
		
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult delete(StockDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		
		Stock entity = new Stock();
		BeanUtils.copyProperties(entity, dto);
		
		stockMapper.delete(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult deleteByIds(String ids) throws BusinessException{
		ResponseResult result = new ResponseResult();
		String[] items = ids.split(",");
		for (String str : items) {
			Long id = new Long(str);
			stockMapper.deleteByPrimaryKey(id);
		}
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult update(StockDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		Stock entity = new Stock();
		BeanUtils.copyProperties(entity, dto);
		
		
		Date now = new Date();
		entity.setModifiedOn(now);
		stockMapper.updateByPrimaryKeySelective(entity);
		
		List<String> dataRateList = dto.getDtlDataRate();
		List<String> dataValueList = dto.getDtlDataValue();
		List<String> dataTitleList = dto.getDtlTitle();
		
		List<String> sshRealNameList = dto.getSshRealName();
		List<Double> sshWeightList = dto.getSshWeight();
		StockShareholding stockShareholding = new StockShareholding();
		stockShareholding.setStkCode(dto.getStkCode());
		if(sshRealNameList!=null && sshRealNameList.size()>0) {
			stockShareholdingMapper.delete(stockShareholding);
		}
		
		for (int i = 0; i < sshRealNameList.size(); i++) {
			
			stockShareholding = new StockShareholding();
			stockShareholding.setId(uidGenerator.nextId());
			stockShareholding.setStkCode(dto.getStkCode());
			if(StringUtils.isBlank(sshRealNameList.get(i))) {
				continue;
			}
			stockShareholding.setSshRealName(sshRealNameList.get(i));
			stockShareholding.setSshWeight(sshWeightList.get(i));
			stockShareholding.setCreatedOn(now);
			stockShareholding.setModifiedOn(now);
			stockShareholdingMapper.insertSelective(stockShareholding);
		}
		
		StockFinancialData stockFinancialData = new StockFinancialData();
		//stockFinancialData.setSdfDataTitle(dto.getSdfDataTitle());
		stockFinancialData.setStkCode(dto.getStkCode());
		
		stockFinancialData = stockFinancialDataMapper.selectOne(stockFinancialData);
		
		if(stockFinancialData==null) {
			stockFinancialData = new StockFinancialData();
		}
		
		
		stockFinancialData.setSdfDataTitle(dto.getSdfDataTitle());
		stockFinancialData.setStkCode(dto.getStkCode());
		stockFinancialData.setCreatedOn(now);
		stockFinancialData.setModifiedOn(now);
		if(stockFinancialData.getSfdId()==null) {
			stockFinancialData.setSfdId(uidGenerator.nextId());
			stockFinancialDataMapper.insertSelective(stockFinancialData);
		}else {
			StockFinancialDataDetail stockFinancialDataDetail = new StockFinancialDataDetail();
			stockFinancialDataDetail.setSfdId(stockFinancialData.getSfdId());
			stockFinancialDataDetailMapper.delete(stockFinancialDataDetail);
			stockFinancialDataMapper.updateByPrimaryKeySelective(stockFinancialData);
		}
		
		for (int i = 0; i < dataRateList.size(); i++) {
			StockFinancialDataDetail stockFinancialDataDetail = new StockFinancialDataDetail();
			if(StringUtils.isBlank(dataTitleList.get(i)) || StringUtils.isBlank(dataValueList.get(i)) ||
					 StringUtils.isBlank(dataRateList.get(i))) {
				continue;
			}
			stockFinancialDataDetail.setDtlId(uidGenerator.nextId());
			stockFinancialDataDetail.setSfdId(stockFinancialData.getSfdId());
			
			stockFinancialDataDetail.setDtlTitle(dataTitleList.get(i));
			stockFinancialDataDetail.setDtlDataValue(dataValueList.get(i));
			stockFinancialDataDetail.setDtlDataRate(dataRateList.get(i));
			stockFinancialDataDetail.setCreatedOn(now);
			stockFinancialDataDetail.setModifiedOn(now);
			
			stockFinancialDataDetailMapper.insertSelective(stockFinancialDataDetail);
		}
		
		if(dto.getGrpId()!=null) {
			StockGroup stockGroup = new StockGroup();
			stockGroup.setStkId(entity.getStkId());
			
			stockGroup = stockGroupMapper.selectOne(stockGroup);
			
			if(stockGroup!=null) {
				
				if(!stockGroup.getGrpId().equals(dto.getGrpId())) {
					stockGroup.setGrpId(dto.getGrpId());
					stockGroupMapper.updateByPrimaryKeySelective(stockGroup);
				}
				
			}else {
				stockGroup = new StockGroup();
				stockGroup.setId(uidGenerator.nextId());
				stockGroup.setStkId(entity.getStkId());
				stockGroup.setGrpId(dto.getGrpId());
				stockGroup.setSortIndex(1);
				stockGroupMapper.insertSelective(stockGroup);
			}
			
			
		}
		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	public ResponseResult findByExample(StockDTO dto) throws BusinessException {
		ResponseResult result = new ResponseResult();
		
		Stock entity = new Stock();
		BeanUtils.copyProperties(entity, dto);
		
		List list =stockMapper.select(entity);
		
		result.setData(list);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	
	public ResponseResult getByExample(StockDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		
		Stock entity = new Stock();
		BeanUtils.copyProperties(entity, dto);
		
		entity =stockMapper.selectOne(entity);
		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
		
	}
	
	public void setStockMapper(StockMapper dao) {
		this.stockMapper = dao;
	}
}
