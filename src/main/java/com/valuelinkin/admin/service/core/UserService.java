package com.valuelinkin.admin.service.core;

import java.util.List;

import com.jcommon.orm.Page;
import com.jcommon.exception.BusinessException;
import com.jcommon.result.ResponseResult;

import com.valuelinkin.admin.dto.UserDTO;

public interface UserService {

	ResponseResult findPage(UserDTO dto) throws BusinessException;
	
	ResponseResult getById(Long id) throws BusinessException;

	ResponseResult save(UserDTO dto) throws BusinessException;
	
	ResponseResult delete(UserDTO dto) throws BusinessException ;
	
	ResponseResult deleteById(Long id) throws BusinessException ;
	
	ResponseResult update(UserDTO dto) throws BusinessException ;
	
	ResponseResult findByExample(UserDTO dto) throws BusinessException;
	
	ResponseResult getByExample(UserDTO dto) throws BusinessException;
	
	ResponseResult deleteByIds(String ids) throws BusinessException ;
	
	
	ResponseResult resetPwd(UserDTO dto) throws BusinessException;
	

}
