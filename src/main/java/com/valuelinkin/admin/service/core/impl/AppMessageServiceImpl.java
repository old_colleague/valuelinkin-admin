package com.valuelinkin.admin.service.core.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.valuelinkin.admin.dao.mapper.UserMapper;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jcommon.orm.Page;
import com.jcommon.exception.BusinessException;
import com.jcommon.utils.BeanUtils;
import com.jcommon.result.ResponseResult;

import com.valuelinkin.admin.dao.entity.AppMessage;
import com.valuelinkin.admin.dto.AppMessageDTO;
import com.valuelinkin.admin.dao.mapper.AppMessageMapper;
import com.valuelinkin.admin.service.core.AppMessageService;
import com.valuelinkin.admin.service.UidGenerator;
import com.valuelinkin.admin.common.utils.AppConstants;

import tk.mybatis.mapper.entity.Example;

/**
 *
 */
@Service
public class AppMessageServiceImpl implements AppMessageService{

	@Autowired
	private AppMessageMapper appMessageMapper;
	
	@Autowired
	private UidGenerator uidGenerator;

	@Autowired
	private UserMapper userMapper;

	@Autowired
	private BatchSaveAppMessageService batchSaveAppMessageService;

	public ResponseResult findPage(AppMessageDTO dto) {
		ResponseResult result = new ResponseResult();
		
		AppMessage entity = new AppMessage();
		BeanUtils.copyProperties(entity, dto);
		
		Page page = new Page();
		page.setPageNo(dto.getPageNo());
		page.setPageSize(dto.getPageSize());
		page.setTotalCount(appMessageMapper.selectCount(entity));
		
		Example example = new Example(AppMessage.class);
		//example.createCriteria().andEqualTo("refId", rowNews.getRefId());
		example.setOrderByClause("msg_date desc");
		
		List<AppMessage> list = appMessageMapper.selectByExampleAndRowBounds(example, new RowBounds(page.getFirst(), page.getPageSize()));
		
		page.setResult(list);
		result.setData(page);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
		
	}

	/**
	 *
	 * @param id
	 * @return
	 * @throws BusinessException
	 */
	public ResponseResult getById(Long id) throws BusinessException{
		ResponseResult result = new ResponseResult();
		AppMessage entity = appMessageMapper.selectByPrimaryKey(id);
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}

	
	/** 插入数据 */
	@Transactional
	public ResponseResult save(AppMessageDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();


		List<Long> userIdList = userMapper.findAllUserIdList(dto);

        int startIndex =0;
        int endIndex =0;
        int maxIndex = userIdList.size();
        int batchSize = 100;
        ExecutorService executorService = Executors.newFixedThreadPool(8);
        while(endIndex<maxIndex){
            endIndex += batchSize;
            endIndex = endIndex>maxIndex?maxIndex:endIndex;
            List<Long> tempList = userIdList.subList(startIndex, endIndex);

            BatchSaveAppMsg calculatePlanMarketValueThread = new BatchSaveAppMsg(tempList, batchSaveAppMessageService ,dto);
            executorService.execute(calculatePlanMarketValueThread);

            startIndex = endIndex;
        }

        executorService.shutdown();

		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}

    @Transactional
    public ResponseResult batchSave( List<Long> userIdList,AppMessageDTO dto) throws BusinessException{
        ResponseResult result = new ResponseResult();

        for (Long usrId : userIdList) {

            AppMessage entity = new AppMessage();
            entity.setUsrId(usrId);
            entity.setMsgTitle(dto.getMsgTitle());
            entity.setMsgContent(dto.getMsgContent());
            entity.setMsgActionLink("");
            entity.setMsgActionType("");
            entity.setMsgIsRead(0);
            entity.setMsgType("1");
            entity.setMsgDate(new Date());

            entity.setMsgId(uidGenerator.nextId());

            appMessageMapper.insertSelective(entity);
        }

        result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
        return result;
    }


	
	@Transactional
	public ResponseResult deleteById(Long id) throws BusinessException{
		ResponseResult result = new ResponseResult();
		appMessageMapper.deleteByPrimaryKey(id);
		
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult delete(AppMessageDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		
		AppMessage entity = new AppMessage();
		BeanUtils.copyProperties(entity, dto);
		
		appMessageMapper.delete(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult deleteByIds(String ids) throws BusinessException{
		ResponseResult result = new ResponseResult();
		String[] items = ids.split(",");
		for (String str : items) {
			Long id = new Long(str);
			appMessageMapper.deleteByPrimaryKey(id);
		}
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult update(AppMessageDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		AppMessage entity = new AppMessage();
		BeanUtils.copyProperties(entity, dto);
		appMessageMapper.updateByPrimaryKeySelective(entity);
		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	public ResponseResult findByExample(AppMessageDTO dto) throws BusinessException {
		ResponseResult result = new ResponseResult();
		
		AppMessage entity = new AppMessage();
		BeanUtils.copyProperties(entity, dto);
		
		List list =appMessageMapper.select(entity);
		
		result.setData(list);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	
	public ResponseResult getByExample(AppMessageDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		
		AppMessage entity = new AppMessage();
		BeanUtils.copyProperties(entity, dto);
		
		entity =appMessageMapper.selectOne(entity);
		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
		
	}
	
	public void setAppMessageMapper(AppMessageMapper dao) {
		this.appMessageMapper = dao;
	}

	class BatchSaveAppMsg implements Runnable{

		private List<Long> userIds;

		private BatchSaveAppMessageService batchSaveAppMessageService;

		private AppMessageDTO dto;

		public BatchSaveAppMsg(List<Long> userIds, BatchSaveAppMessageService batchSaveAppMessageService ,AppMessageDTO dto){

		    this.userIds= userIds;
		    this.batchSaveAppMessageService= batchSaveAppMessageService;
		    this.dto = dto;

        }

		@Override
		public void run() {

            batchSaveAppMessageService.batchSave(userIds,dto);

		}
	}
}
