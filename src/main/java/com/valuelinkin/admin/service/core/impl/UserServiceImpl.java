package com.valuelinkin.admin.service.core.impl;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jcommon.exception.BusinessException;
import com.jcommon.orm.Page;
import com.jcommon.result.ResponseResult;
import com.jcommon.utils.BeanUtils;
import com.jcommon.utils.Cipher;
import com.jcommon.utils.StringUtils;
import com.valuelinkin.admin.common.utils.AppConstants;
import com.valuelinkin.admin.dao.entity.User;
import com.valuelinkin.admin.dao.mapper.UserMapper;
import com.valuelinkin.admin.dto.UserDTO;
import com.valuelinkin.admin.service.UidGenerator;
import com.valuelinkin.admin.service.core.UserService;

import tk.mybatis.mapper.entity.Example;

@Service
public class UserServiceImpl implements UserService{

	@Autowired
	private UserMapper userMapper;
	
	@Autowired
	private UidGenerator uidGenerator;

	public ResponseResult findPage(UserDTO dto) {
		ResponseResult result = new ResponseResult();
		
		User entity = new User();
		BeanUtils.copyProperties(entity, dto);
		
		Page page = new Page();
		page.setPageNo(dto.getPageNo());
		page.setPageSize(dto.getPageSize());

		
		Example example = new Example(User.class);
		String keywords = StringUtils.trimToNull(dto.getKeywords());
		Example.Criteria criteria = example.createCriteria();

		if(StringUtils.isNotBlank(keywords)) {
			keywords = "%"+keywords+"%";
			criteria.orLike("usrName", keywords).orLike("usrPhone", keywords).orLike("usrNickName", keywords);
			//example.or(criteria);
		}

		if(StringUtils.isNotBlank(dto.getUsrRole())){
			Example.Criteria criteria2 = example.createCriteria();
			criteria2.andEqualTo("usrRole",dto.getUsrRole());
			example.and(criteria2);
		}

		Example.Criteria criteria3 = example.createCriteria();
		criteria3.andIsNotNull("usrHeadImg").andIsNotNull("usrNickName");
		example.and(criteria3);

		page.setTotalCount(userMapper.selectCountByExample(example));

		example.setOrderByClause("created_on desc");
		
		List<User> list = userMapper.selectByExampleAndRowBounds(example, new RowBounds(page.getFirst(), page.getPageSize()));
		
		page.setResult(list);
		result.setData(page);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
		
	}	
	
	public ResponseResult getById(Long id) throws BusinessException{
		ResponseResult result = new ResponseResult();
		User entity = userMapper.selectByPrimaryKey(id);
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}

	
	/** 插入数据 */
	@Transactional
	public ResponseResult save(UserDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		User entity = new User();
		entity.setUsrPhone(dto.getUsrPhone());
		entity = userMapper.selectOne(entity);

		if(entity!=null) {
			result.setErrorCode(AppConstants.RETURN_FAIL_CODE);
			result.setErrorMessage("手机号在系统中已经存在。");
			return result;
		}else {
			entity = new User();
		}
		
		BeanUtils.copyProperties(entity, dto);
		entity.setUsrId(uidGenerator.nextId());
		
		entity.setUsrPassword(Cipher.MD5("888888"));
		//entity.
		Date now = new Date();
		entity.setCreatedOn(now);
		entity.setModifiedOn(now);
		entity.setCreateBy("system");
		userMapper.insertSelective(entity);
		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult deleteById(Long id) throws BusinessException{
		ResponseResult result = new ResponseResult();
		userMapper.deleteByPrimaryKey(id);
		
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult delete(UserDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		
		User entity = new User();
		BeanUtils.copyProperties(entity, dto);
		
		userMapper.delete(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult deleteByIds(String ids) throws BusinessException{
		ResponseResult result = new ResponseResult();
		String[] items = ids.split(",");
		for (String str : items) {
			Long id = new Long(str);
			userMapper.deleteByPrimaryKey(id);
		}
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult update(UserDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		
		User entity = userMapper.selectByPrimaryKey(dto.getUsrId());
		if(entity==null) {
			result.setErrorCode(AppConstants.RETURN_FAIL_CODE);
			result.setErrorMessage("用户不存在");
			return result;
		}
		
		BeanUtils.copyProperties(entity, dto);
		
		Date now = new Date();
		entity.setModifiedOn(now);
		
		userMapper.updateByPrimaryKeySelective(entity);
		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	public ResponseResult findByExample(UserDTO dto) throws BusinessException {
		ResponseResult result = new ResponseResult();
		
		User entity = new User();
		BeanUtils.copyProperties(entity, dto);
		
		List list =userMapper.select(entity);
		
		result.setData(list);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	
	public ResponseResult getByExample(UserDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		
		User entity = new User();
		BeanUtils.copyProperties(entity, dto);
		
		entity =userMapper.selectOne(entity);
		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
		
	}
	
	/**
	 * @return
	 * @throws BusinessException
	 */
	@Override
	@Transactional
	public ResponseResult resetPwd(UserDTO dto) throws BusinessException {
		ResponseResult result = new ResponseResult();
		User entity = userMapper.selectByPrimaryKey(dto.getUsrId());
		if(entity==null) {
			result.setErrorCode(AppConstants.RETURN_FAIL_CODE);
			result.setErrorMessage("用户不存在");
			return result;
		}
		
		Date now = new Date();
		entity.setModifiedOn(now);
		
		entity.setUsrPassword(Cipher.MD5(dto.getUsrPassword()));
		
		userMapper.updateByPrimaryKeySelective(entity);
		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	public void setUserMapper(UserMapper dao) {
		this.userMapper = dao;
	}

	
}
