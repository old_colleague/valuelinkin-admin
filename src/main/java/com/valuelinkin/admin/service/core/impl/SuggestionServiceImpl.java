package com.valuelinkin.admin.service.core.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.valuelinkin.admin.dto.SuggestionResultDTO;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jcommon.orm.Page;
import com.jcommon.exception.BusinessException;
import com.jcommon.utils.BeanUtils;
import com.jcommon.result.ResponseResult;

import com.valuelinkin.admin.dao.entity.Suggestion;
import com.valuelinkin.admin.dto.SuggestionDTO;
import com.valuelinkin.admin.dao.mapper.SuggestionMapper;
import com.valuelinkin.admin.service.core.SuggestionService;
import com.valuelinkin.admin.service.UidGenerator;
import com.valuelinkin.admin.common.utils.AppConstants;

import tk.mybatis.mapper.entity.Example;

@Service
public class SuggestionServiceImpl implements SuggestionService{

	@Autowired
	private SuggestionMapper suggestionMapper;
	
	@Autowired
	private UidGenerator uidGenerator;

	public ResponseResult findPage(SuggestionDTO dto) {
		ResponseResult result = new ResponseResult();
		
		Suggestion entity = new Suggestion();
		BeanUtils.copyProperties(entity, dto);
		
		Page page = new Page();
		page.setPageNo(dto.getPageNo());
		page.setPageSize(dto.getPageSize());
		page.setTotalCount(suggestionMapper.selectCount(entity));
		
		//Example example = new Example(Suggestion.class);
		//example.createCriteria().andEqualTo("refId", rowNews.getRefId());
		//example.setOrderByClause("nws_Publish_Date desc");
		
		List<SuggestionResultDTO> list = suggestionMapper.findPage(dto);
		
		page.setResult(list);
		result.setData(page);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
		
	}	
	
	public ResponseResult getById(Long id) throws BusinessException{
		ResponseResult result = new ResponseResult();
		Suggestion entity = suggestionMapper.selectByPrimaryKey(id);
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}

	
	/** 插入数据 */
	@Transactional
	public ResponseResult save(SuggestionDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		Suggestion entity = new Suggestion();
		BeanUtils.copyProperties(entity, dto);
		entity.setSgsId(uidGenerator.nextId());
		
		suggestionMapper.insertSelective(entity);
		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult deleteById(Long id) throws BusinessException{
		ResponseResult result = new ResponseResult();
		suggestionMapper.deleteByPrimaryKey(id);
		
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult delete(SuggestionDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		
		Suggestion entity = new Suggestion();
		BeanUtils.copyProperties(entity, dto);
		
		suggestionMapper.delete(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult deleteByIds(String ids) throws BusinessException{
		ResponseResult result = new ResponseResult();
		String[] items = ids.split(",");
		for (String str : items) {
			Long id = new Long(str);
			suggestionMapper.deleteByPrimaryKey(id);
		}
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult update(SuggestionDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		Suggestion entity = new Suggestion();

		BeanUtils.copyProperties(entity, dto);
		Date now = new Date();
		entity.setModifiedOn(now);
		entity.setSgsReplyOn(now);
		entity.setSgsStatus("CLOSE");
		suggestionMapper.updateByPrimaryKeySelective(entity);
		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	public ResponseResult findByExample(SuggestionDTO dto) throws BusinessException {
		ResponseResult result = new ResponseResult();
		
		Suggestion entity = new Suggestion();
		BeanUtils.copyProperties(entity, dto);
		
		List list =suggestionMapper.select(entity);
		
		result.setData(list);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	
	public ResponseResult getByExample(SuggestionDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		
		Suggestion entity = new Suggestion();
		BeanUtils.copyProperties(entity, dto);
		
		entity =suggestionMapper.selectOne(entity);
		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
		
	}
	
	public void setSuggestionMapper(SuggestionMapper dao) {
		this.suggestionMapper = dao;
	}
}
