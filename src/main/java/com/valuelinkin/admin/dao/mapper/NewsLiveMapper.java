package com.valuelinkin.admin.dao.mapper;


import com.valuelinkin.admin.dto.NewsLiveDTO;
import com.valuelinkin.admin.dto.NewsLiveResultDTO;
import org.apache.ibatis.annotations.Mapper;

import com.valuelinkin.admin.dao.entity.NewsLive;
import com.valuelinkin.admin.dao.BaseMapper;

import java.util.List;

@Mapper
public interface NewsLiveMapper extends BaseMapper<NewsLive>{

    List<NewsLiveResultDTO> findPage(NewsLiveDTO dto);
	

}
