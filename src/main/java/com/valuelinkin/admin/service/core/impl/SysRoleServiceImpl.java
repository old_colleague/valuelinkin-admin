package com.valuelinkin.admin.service.core.impl;

import java.util.Date;
import java.util.List;

import com.valuelinkin.admin.dao.entity.SysRolePermission;
import com.valuelinkin.admin.dao.mapper.SysRolePermissionMapper;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jcommon.exception.BusinessException;
import com.jcommon.orm.Page;
import com.jcommon.result.ResponseResult;
import com.jcommon.utils.BeanUtils;
import com.valuelinkin.admin.common.utils.AppConstants;
import com.valuelinkin.admin.dao.entity.SysRole;
import com.valuelinkin.admin.dao.mapper.SysRoleMapper;
import com.valuelinkin.admin.dto.SysRoleDTO;
import com.valuelinkin.admin.service.UidGenerator;
import com.valuelinkin.admin.service.core.SysRoleService;

import tk.mybatis.mapper.entity.Example;

@Service
public class SysRoleServiceImpl implements SysRoleService{

	@Autowired
	private SysRoleMapper sysRoleMapper;
	
	@Autowired
	private UidGenerator uidGenerator;

	@Autowired
	private SysRolePermissionMapper sysRolePermissionMapper;

	public ResponseResult findPage(SysRoleDTO dto) {
		ResponseResult result = new ResponseResult();
		
		SysRole entity = new SysRole();
		BeanUtils.copyProperties(entity, dto);
		
		Page page = new Page();
		page.setPageNo(dto.getPageNo());
		page.setPageSize(dto.getPageSize());
		page.setTotalCount(sysRoleMapper.selectCount(entity));
		
		Example example = new Example(SysRole.class);
		//example.createCriteria().andEqualTo("refId", rowNews.getRefId());
		//example.setOrderByClause("nws_Publish_Date desc");
		
		List<SysRole> list = sysRoleMapper.selectByExampleAndRowBounds(example, new RowBounds(page.getFirst(), page.getPageSize()));
		
		page.setResult(list);
		result.setData(page);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
		
	}	
	
	public ResponseResult getById(Long id) throws BusinessException{
		ResponseResult result = new ResponseResult();
		SysRole entity = sysRoleMapper.selectByPrimaryKey(id);
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}

	
	/** 插入数据 */
	@Transactional
	public ResponseResult save(SysRoleDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		SysRole entity = new SysRole();
		BeanUtils.copyProperties(entity, dto);
		entity.setRleId(uidGenerator.nextId());
		
		entity.setCreatedOn(new Date());
		entity.setModifiedOn(new Date());
		entity.setRleStatus(0);
		
		sysRoleMapper.insertSelective(entity);

		List<Long> pvlIdList = dto.getPvlId();
		for (Long aLong : pvlIdList) {
			SysRolePermission sysRolePermission = new SysRolePermission();
			sysRolePermission.setId(uidGenerator.nextId());
			sysRolePermission.setPvlId(aLong);
			sysRolePermission.setRleId(entity.getRleId());
			sysRolePermissionMapper.insertSelective(sysRolePermission);
		}

		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult deleteById(Long id) throws BusinessException{
		ResponseResult result = new ResponseResult();
		sysRoleMapper.deleteByPrimaryKey(id);
		
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult delete(SysRoleDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		
		SysRole entity = new SysRole();
		BeanUtils.copyProperties(entity, dto);
		
		sysRoleMapper.delete(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult deleteByIds(String ids) throws BusinessException{
		ResponseResult result = new ResponseResult();
		String[] items = ids.split(",");
		for (String str : items) {
			Long id = new Long(str);
			sysRoleMapper.deleteByPrimaryKey(id);
		}
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult update(SysRoleDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		SysRole entity = new SysRole();
		BeanUtils.copyProperties(entity, dto);
		sysRoleMapper.updateByPrimaryKeySelective(entity);

		SysRolePermission sysRolePermission = new SysRolePermission();
		sysRolePermission.setRleId(dto.getRleId());
		sysRolePermissionMapper.delete(sysRolePermission);
		List<Long> pvlIdList = dto.getPvlId();
		for (Long aLong : pvlIdList) {
			sysRolePermission = new SysRolePermission();
			sysRolePermission.setId(uidGenerator.nextId());
			sysRolePermission.setPvlId(aLong);
			sysRolePermission.setRleId(entity.getRleId());
			sysRolePermissionMapper.insertSelective(sysRolePermission);
		}
		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	public ResponseResult findByExample(SysRoleDTO dto) throws BusinessException {
		ResponseResult result = new ResponseResult();
		
		SysRole entity = new SysRole();
		BeanUtils.copyProperties(entity, dto);
		
		List list =sysRoleMapper.select(entity);
		
		result.setData(list);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	
	public ResponseResult getByExample(SysRoleDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		
		SysRole entity = new SysRole();
		BeanUtils.copyProperties(entity, dto);
		
		entity =sysRoleMapper.selectOne(entity);
		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
		
	}
	
	public void setSysRoleMapper(SysRoleMapper dao) {
		this.sysRoleMapper = dao;
	}
}
