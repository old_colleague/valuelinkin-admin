package com.valuelinkin.admin.dao.mapper;


import com.valuelinkin.admin.dto.NewsFlushDTO;
import org.apache.ibatis.annotations.Mapper;

import com.valuelinkin.admin.dao.entity.NewsFlush;
import com.valuelinkin.admin.dao.BaseMapper;

import java.util.List;

@Mapper
public interface NewsFlushMapper extends BaseMapper<NewsFlush>{

    List<NewsFlush> findPage(NewsFlushDTO dto);
	

}
