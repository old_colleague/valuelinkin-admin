package com.valuelinkin.admin.dto;

/**
 * 
 * @atuhor: nail.zhang
 * @description:
 * @date: Created in  2019年2月3日
 */
public class StockInfoDTO {
	
	private String stkCode;
	private String stkName;
	/**
	 * @return the stkCode
	 */
	public String getStkCode() {
		return stkCode;
	}
	/**
	 * @param stkCode the stkCode to set
	 */
	public void setStkCode(String stkCode) {
		this.stkCode = stkCode;
	}
	/**
	 * @return the stkName
	 */
	public String getStkName() {
		return stkName;
	}
	/**
	 * @param stkName the stkName to set
	 */
	public void setStkName(String stkName) {
		this.stkName = stkName;
	}
	

}
