package com.valuelinkin.admin.dao.mapper;


import org.apache.ibatis.annotations.Mapper;

import com.valuelinkin.admin.dao.entity.Parameter;
import com.valuelinkin.admin.dao.BaseMapper;

@Mapper
public interface ParameterMapper extends BaseMapper<Parameter>{
	

}
