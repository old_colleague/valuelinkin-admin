package com.valuelinkin.admin.web.controller;

import java.util.List;

import com.jcommon.orm.Page;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.ui.Model;

import com.valuelinkin.admin.dto.SysRolePermissionDTO;
import com.valuelinkin.admin.service.core.SysRolePermissionService;

import com.jcommon.result.ResponseResult;
import com.jcommon.utils.BeanUtils;

@Controller
@RequestMapping(value="/sysRolePermission") 
public class SysRolePermissionController{
	
	@Autowired
	private SysRolePermissionService sysRolePermissionService;
	
	@RequestMapping(value="/index",method=RequestMethod.GET)
	public String index( Model model) throws Exception {
		
		return "/sysRolePermission/list";
	}
	
	/** 
	 * 执行搜索 
	 **/
	@RequestMapping(value="",method=RequestMethod.GET)
	@ResponseBody
	public ResponseResult findPage(SysRolePermissionDTO dto) {
		
		ResponseResult result = new ResponseResult();
		result = this.sysRolePermissionService.findPage(dto);
		return result;
	}
	
	
	/** 
	 * 查看对象
	 **/
	@RequestMapping(value="/view/{id}",method=RequestMethod.GET)
	public String getById(@PathVariable Long id, Model model) {
		ResponseResult result = new ResponseResult();
		result = sysRolePermissionService.getById(id);
		
		model.addAttribute("result", result);
		return "/sysRolePermission/view";
		
	}
	

	/** 
	 * 进入新增页面
	 **/
	@RequestMapping(value="/create", method=RequestMethod.GET)
	public String create(SysRolePermissionDTO dto, Model model) throws Exception {
		model.addAttribute("sysRolePermission", dto);
		return "/sysRolePermission/create";
	}
	
	/** 
	 * 保存新增对象
	 **/
	@RequestMapping(value="",method=RequestMethod.POST)
	@ResponseBody
	public ResponseResult save(SysRolePermissionDTO dto) {
		ResponseResult result = null;
		result = sysRolePermissionService.save(dto);
		return result;
	}
	
	
	/**
	 * 进入更新页面
	 **/
	@RequestMapping(value="/edit/{id}", method=RequestMethod.GET)
	public String edit(@PathVariable Long id, Model model) throws Exception {

		ResponseResult result  = sysRolePermissionService.getById(id);
		model.addAttribute("result", result);
		
		return "/sysRolePermission/edit";
	}
	
	/**
	 * 保存更新对象
	 **/
	@RequestMapping(value="/{id}", method=RequestMethod.PUT)
	@ResponseBody
	public ResponseResult update(@PathVariable Long id,SysRolePermissionDTO dto) throws Exception {
		ResponseResult result = new ResponseResult();
		dto.setId(id);
		result = sysRolePermissionService.update(dto);
		return result;
	}
	
	
	/**
	 *删除多个对象
	 **/
	@RequestMapping(value="/{ids}", method=RequestMethod.DELETE)
	@ResponseBody
	public ResponseResult delete(@PathVariable String ids) {
		
		ResponseResult result = null;
		result = sysRolePermissionService.deleteByIds(ids);
		return result;
		
	}
	
	public void setSysRolePermissionService(SysRolePermissionService service) {
		this.sysRolePermissionService = service;
	}
	
}
