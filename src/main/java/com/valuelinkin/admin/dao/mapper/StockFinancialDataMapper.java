package com.valuelinkin.admin.dao.mapper;


import org.apache.ibatis.annotations.Mapper;

import com.valuelinkin.admin.dao.entity.StockFinancialData;
import com.valuelinkin.admin.dao.BaseMapper;

@Mapper
public interface StockFinancialDataMapper extends BaseMapper<StockFinancialData>{
	

}
