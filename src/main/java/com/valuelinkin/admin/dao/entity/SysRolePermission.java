package com.valuelinkin.admin.dao.entity;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 〈功能详细描述〉
 *
 * @author Nail.zhang
 * @version [V0.1, Jan 9, 2018]
 */

@Table(name="sys_role_permission")
public class SysRolePermission implements java.io.Serializable{
	private static final long serialVersionUID = 5454155825314635342L;
	

	//columns START
		@Id
		@Column(name = "id")
		private Long id;
		private Long pvlId;
		private Long rleId;
	//columns END


	public SysRolePermission(){
	}

	public SysRolePermission(
		Long id
	){
		this.id = id;
	}

	

	public void setId(Long value) {
		this.id = value;
	}
	
	public Long getId() {
		return this.id;
	}
	
	public Long getPvlId() {
		return this.pvlId;
	}
	
	public void setPvlId(Long value) {
		this.pvlId = value;
	}
	
	public Long getRleId() {
		return this.rleId;
	}
	
	public void setRleId(Long value) {
		this.rleId = value;
	}
	

}

