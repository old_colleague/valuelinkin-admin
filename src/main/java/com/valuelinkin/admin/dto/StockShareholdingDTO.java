package com.valuelinkin.admin.dto;

import com.valuelinkin.admin.dto.BaseDTO;

public class StockShareholdingDTO extends BaseDTO {
    private static final long serialVersionUID = 3148176768559230877L;
    

	private Long id;
	private java.lang.String stkCode;
	private java.lang.String sshRealName;
	private Double sshWeight;
	private java.util.Date createdOn;
	private java.util.Date modifiedOn;

	
	public Long getId() {
		return this.id;
	}
	
	public void setId(Long value) {
		this.id = value;
	}
	
	public java.lang.String getStkCode() {
		return this.stkCode;
	}
	
	public void setStkCode(java.lang.String value) {
		this.stkCode = value;
	}
	
	public java.lang.String getSshRealName() {
		return this.sshRealName;
	}
	
	public void setSshRealName(java.lang.String value) {
		this.sshRealName = value;
	}
	
	public Double getSshWeight() {
		return this.sshWeight;
	}
	
	public void setSshWeight(Double value) {
		this.sshWeight = value;
	}
	
	public java.util.Date getCreatedOn() {
		return this.createdOn;
	}
	
	public void setCreatedOn(java.util.Date value) {
		this.createdOn = value;
	}
	
	public java.util.Date getModifiedOn() {
		return this.modifiedOn;
	}
	
	public void setModifiedOn(java.util.Date value) {
		this.modifiedOn = value;
	}
	
}

