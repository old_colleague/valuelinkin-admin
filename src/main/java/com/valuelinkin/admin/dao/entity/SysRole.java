package com.valuelinkin.admin.dao.entity;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 〈功能详细描述〉
 *
 * @author Nail.zhang
 * @version [V0.1, Jan 9, 2018]
 */

@Table(name="sys_role")
public class SysRole implements java.io.Serializable{
	private static final long serialVersionUID = 5454155825314635342L;
	

	//columns START
		@Id
		@Column(name = "rle_id")
		private Long rleId;
		private java.lang.String rleName;
		private Integer rleStatus;
		private java.lang.String rleRemark;
		private java.util.Date createdOn;
		private java.util.Date modifiedOn;
	//columns END


	public SysRole(){
	}

	public SysRole(
		Long rleId
	){
		this.rleId = rleId;
	}

	

	public void setRleId(Long value) {
		this.rleId = value;
	}
	
	public Long getRleId() {
		return this.rleId;
	}
	
	public java.lang.String getRleName() {
		return this.rleName;
	}
	
	public void setRleName(java.lang.String value) {
		this.rleName = value;
	}
	
	public Integer getRleStatus() {
		return this.rleStatus;
	}
	
	public void setRleStatus(Integer value) {
		this.rleStatus = value;
	}
	
	public java.lang.String getRleRemark() {
		return this.rleRemark;
	}
	
	public void setRleRemark(java.lang.String value) {
		this.rleRemark = value;
	}
	
		
	public java.util.Date getCreatedOn() {
		return this.createdOn;
	}
	
	public void setCreatedOn(java.util.Date value) {
		this.createdOn = value;
	}
	
		
	public java.util.Date getModifiedOn() {
		return this.modifiedOn;
	}
	
	public void setModifiedOn(java.util.Date value) {
		this.modifiedOn = value;
	}
	

}

