package com.valuelinkin.admin.dto;

import com.valuelinkin.admin.dto.BaseDTO;

public class StockReportDTO extends BaseDTO {
    private static final long serialVersionUID = 3148176768559230877L;
    

	private Long id;
	private java.lang.String stkCode;
	private java.lang.String rptTitle;
	private java.lang.String rptSource;
	private java.lang.String rptContent;
	private java.util.Date rptPublishDate;
	private java.util.Date createdOn;
	private java.util.Date modifiedOn;

	
	public Long getId() {
		return this.id;
	}
	
	public void setId(Long value) {
		this.id = value;
	}
	
	public java.lang.String getStkCode() {
		return this.stkCode;
	}
	
	public void setStkCode(java.lang.String value) {
		this.stkCode = value;
	}
	
	public java.lang.String getRptTitle() {
		return this.rptTitle;
	}
	
	public void setRptTitle(java.lang.String value) {
		this.rptTitle = value;
	}
	
	public java.lang.String getRptSource() {
		return this.rptSource;
	}
	
	public void setRptSource(java.lang.String value) {
		this.rptSource = value;
	}
	
	public java.lang.String getRptContent() {
		return this.rptContent;
	}
	
	public void setRptContent(java.lang.String value) {
		this.rptContent = value;
	}
	
	public java.util.Date getRptPublishDate() {
		return this.rptPublishDate;
	}
	
	public void setRptPublishDate(java.util.Date value) {
		this.rptPublishDate = value;
	}
	
	public java.util.Date getCreatedOn() {
		return this.createdOn;
	}
	
	public void setCreatedOn(java.util.Date value) {
		this.createdOn = value;
	}
	
	public java.util.Date getModifiedOn() {
		return this.modifiedOn;
	}
	
	public void setModifiedOn(java.util.Date value) {
		this.modifiedOn = value;
	}
	
}

