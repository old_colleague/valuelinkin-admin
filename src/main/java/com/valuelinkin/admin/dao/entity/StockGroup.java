package com.valuelinkin.admin.dao.entity;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 〈功能详细描述〉
 *
 * @author Nail.zhang
 * @version [V0.1, Jan 9, 2018]
 */

@Table(name="vln_stock_group")
public class StockGroup implements java.io.Serializable{
	private static final long serialVersionUID = 5454155825314635342L;
	

	//columns START
		@Id
		@Column(name = "id")
		private Long id;
		private Long stkId;
		private Long grpId;
		private Integer sortIndex;
	//columns END


	public StockGroup(){
	}

	public StockGroup(
		Long id
	){
		this.id = id;
	}

	

	public void setId(Long value) {
		this.id = value;
	}
	
	public Long getId() {
		return this.id;
	}
	
	public Long getStkId() {
		return this.stkId;
	}
	
	public void setStkId(Long value) {
		this.stkId = value;
	}
	
	public Long getGrpId() {
		return this.grpId;
	}
	
	public void setGrpId(Long value) {
		this.grpId = value;
	}
	
	public Integer getSortIndex() {
		return this.sortIndex;
	}
	
	public void setSortIndex(Integer value) {
		this.sortIndex = value;
	}
	

}

