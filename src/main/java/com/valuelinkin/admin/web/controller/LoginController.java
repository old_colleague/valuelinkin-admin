package com.valuelinkin.admin.web.controller;

import javax.servlet.http.HttpSession;

import com.valuelinkin.admin.service.core.SysPrivilegeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jcommon.result.ResponseResult;
import com.valuelinkin.admin.common.utils.AppConstants;
import com.valuelinkin.admin.dto.SysAdminDTO;
import com.valuelinkin.admin.service.core.SysAdminService;

@Controller
@RequestMapping(value="/")
public class LoginController {
	
	@Autowired
	private SysAdminService adminService;

	@Autowired
	private SysPrivilegeService sysPrivilegeService;
	
	@RequestMapping(value="/",method=RequestMethod.GET)
	public String index( Model model) throws Exception {
		
		return "/index";
	}
	
	@RequestMapping(value="/welcome",method=RequestMethod.GET)
	public String welcome( Model model) throws Exception {
		
		return "/welcome";
	}
	
	@RequestMapping(value="/login",method=RequestMethod.GET)
	public String login( Model model, HttpSession session) throws Exception {
		session.removeAttribute(AppConstants.LOGIN_SESSION);
		return "/login";
	}
	
	@RequestMapping(value="/authrize",method=RequestMethod.POST)
	@ResponseBody
	public ResponseResult authrize( SysAdminDTO dto, HttpSession session) throws Exception {
		ResponseResult result = new ResponseResult();
		SysAdminDTO adminDTO = adminService.authrize(dto);
		
		session.setAttribute(AppConstants.LOGIN_SESSION, adminDTO);

		session.setAttribute(AppConstants.LOGIN_USER_PERMISSION, sysPrivilegeService.findRolePvlList(adminDTO.getRleId()));

		result.setData(adminDTO);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}

}
