package com.valuelinkin.admin.web.controller;

import java.util.List;

import com.jcommon.orm.Page;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.ui.Model;

import com.valuelinkin.admin.dto.QuestionDTO;
import com.valuelinkin.admin.service.core.QuestionService;

import com.jcommon.result.ResponseResult;
import com.jcommon.utils.BeanUtils;

@Controller
@RequestMapping(value="/question") 
public class QuestionController{
	
	@Autowired
	private QuestionService questionService;
	
	@RequestMapping(value="/index",method=RequestMethod.GET)
	public String index( Model model) throws Exception {
		
		return "/question/list";
	}
	
	/** 
	 * 执行搜索 
	 **/
	@RequestMapping(value="",method=RequestMethod.GET)
	@ResponseBody
	public ResponseResult findPage(QuestionDTO dto) {
		
		ResponseResult result = new ResponseResult();
		result = this.questionService.findPage(dto);
		return result;
	}
	
	
	/** 
	 * 查看对象
	 **/
	@RequestMapping(value="/view/{id}",method=RequestMethod.GET)
	public String getById(@PathVariable Long id, Model model) {
		ResponseResult result = new ResponseResult();
		result = questionService.getById(id);
		
		model.addAttribute("result", result);
		return "/question/view";
		
	}
	

	/** 
	 * 进入新增页面
	 **/
	@RequestMapping(value="/create", method=RequestMethod.GET)
	public String create(QuestionDTO dto, Model model) throws Exception {
		model.addAttribute("question", dto);
		return "/question/create";
	}
	
	/** 
	 * 保存新增对象
	 **/
	@RequestMapping(value="",method=RequestMethod.POST)
	@ResponseBody
	public ResponseResult save(QuestionDTO dto) {
		ResponseResult result = null;
		result = questionService.save(dto);
		return result;
	}
	
	
	/**
	 * 进入更新页面
	 **/
	@RequestMapping(value="/edit/{id}", method=RequestMethod.GET)
	public String edit(@PathVariable Long id, Model model) throws Exception {

		ResponseResult result  = questionService.getById(id);
		model.addAttribute("result", result);
		
		return "/question/edit";
	}
	
	/**
	 * 保存更新对象
	 **/
	@RequestMapping(value="/{id}", method=RequestMethod.PUT)
	@ResponseBody
	public ResponseResult update(@PathVariable Long id,QuestionDTO dto) throws Exception {
		ResponseResult result = new ResponseResult();
		dto.setQsnId(id);
		result = questionService.update(dto);
		return result;
	}
	
	
	/**
	 *删除多个对象
	 **/
	@RequestMapping(value="/{ids}", method=RequestMethod.DELETE)
	@ResponseBody
	public ResponseResult delete(@PathVariable String ids) {
		
		ResponseResult result = null;
		result = questionService.deleteByIds(ids);
		return result;
		
	}
	
	public void setQuestionService(QuestionService service) {
		this.questionService = service;
	}
	
}
