package com.valuelinkin.admin.dto;

import com.valuelinkin.admin.dto.BaseDTO;

public class SuggestionDTO extends BaseDTO {
    private static final long serialVersionUID = 3148176768559230877L;
    

	private Long sgsId;
	private Long usrId;
	private java.lang.String sgsContent;
	private java.lang.String sgsStatus;
	private java.lang.String sgsReply;
	private java.util.Date sgsReplyOn;
	private java.lang.String sgsReplyBy;
	private java.util.Date createdOn;
	private java.util.Date modifiedOn;

	
	public Long getSgsId() {
		return this.sgsId;
	}
	
	public void setSgsId(Long value) {
		this.sgsId = value;
	}
	
	public Long getUsrId() {
		return this.usrId;
	}
	
	public void setUsrId(Long value) {
		this.usrId = value;
	}
	
	public java.lang.String getSgsContent() {
		return this.sgsContent;
	}
	
	public void setSgsContent(java.lang.String value) {
		this.sgsContent = value;
	}
	
	public java.lang.String getSgsStatus() {
		return this.sgsStatus;
	}
	
	public void setSgsStatus(java.lang.String value) {
		this.sgsStatus = value;
	}
	
	public java.lang.String getSgsReply() {
		return this.sgsReply;
	}
	
	public void setSgsReply(java.lang.String value) {
		this.sgsReply = value;
	}
	
	public java.util.Date getSgsReplyOn() {
		return this.sgsReplyOn;
	}
	
	public void setSgsReplyOn(java.util.Date value) {
		this.sgsReplyOn = value;
	}
	
	public java.lang.String getSgsReplyBy() {
		return this.sgsReplyBy;
	}
	
	public void setSgsReplyBy(java.lang.String value) {
		this.sgsReplyBy = value;
	}
	
	public java.util.Date getCreatedOn() {
		return this.createdOn;
	}
	
	public void setCreatedOn(java.util.Date value) {
		this.createdOn = value;
	}
	
	public java.util.Date getModifiedOn() {
		return this.modifiedOn;
	}
	
	public void setModifiedOn(java.util.Date value) {
		this.modifiedOn = value;
	}
	
}

