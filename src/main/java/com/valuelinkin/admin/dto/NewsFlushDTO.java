package com.valuelinkin.admin.dto;

import com.valuelinkin.admin.dto.BaseDTO;

public class NewsFlushDTO extends BaseDTO {
    private static final long serialVersionUID = 3148176768559230877L;
    

	private Long nwsId;
	private Long clmId;
	private java.lang.String nwsTitle;
	private java.lang.String nwsKeywords;
	private java.lang.String nwsBrief;
	private java.lang.String nwsContent;
	private java.lang.String nwsSource;
	private java.util.Date nwsPublishDte;
	private Integer nwsStatus;
	private java.lang.String nwsViewCount;
	private Integer nwsStickTop;
	private java.lang.String nwsColor;
	private java.util.Date createdOn;
	private java.util.Date modifiedOn;
	private String keywords;


	public String getKeywords() {
		return keywords;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}


	
	public Long getNwsId() {
		return this.nwsId;
	}
	
	public void setNwsId(Long value) {
		this.nwsId = value;
	}
	
	public Long getClmId() {
		return this.clmId;
	}
	
	public void setClmId(Long value) {
		this.clmId = value;
	}
	
	public java.lang.String getNwsTitle() {
		return this.nwsTitle;
	}
	
	public void setNwsTitle(java.lang.String value) {
		this.nwsTitle = value;
	}
	
	public java.lang.String getNwsKeywords() {
		return this.nwsKeywords;
	}
	
	public void setNwsKeywords(java.lang.String value) {
		this.nwsKeywords = value;
	}
	
	public java.lang.String getNwsBrief() {
		return this.nwsBrief;
	}
	
	public void setNwsBrief(java.lang.String value) {
		this.nwsBrief = value;
	}
	
	public java.lang.String getNwsContent() {
		return this.nwsContent;
	}
	
	public void setNwsContent(java.lang.String value) {
		this.nwsContent = value;
	}
	
	public java.lang.String getNwsSource() {
		return this.nwsSource;
	}
	
	public void setNwsSource(java.lang.String value) {
		this.nwsSource = value;
	}
	
	public java.util.Date getNwsPublishDte() {
		return this.nwsPublishDte;
	}
	
	public void setNwsPublishDte(java.util.Date value) {
		this.nwsPublishDte = value;
	}
	
	public Integer getNwsStatus() {
		return this.nwsStatus;
	}
	
	public void setNwsStatus(Integer value) {
		this.nwsStatus = value;
	}
	
	public java.lang.String getNwsViewCount() {
		return this.nwsViewCount;
	}
	
	public void setNwsViewCount(java.lang.String value) {
		this.nwsViewCount = value;
	}
	
	public Integer getNwsStickTop() {
		return this.nwsStickTop;
	}
	
	public void setNwsStickTop(Integer value) {
		this.nwsStickTop = value;
	}
	
	public java.lang.String getNwsColor() {
		return this.nwsColor;
	}
	
	public void setNwsColor(java.lang.String value) {
		this.nwsColor = value;
	}
	
	public java.util.Date getCreatedOn() {
		return this.createdOn;
	}
	
	public void setCreatedOn(java.util.Date value) {
		this.createdOn = value;
	}
	
	public java.util.Date getModifiedOn() {
		return this.modifiedOn;
	}
	
	public void setModifiedOn(java.util.Date value) {
		this.modifiedOn = value;
	}
	
}

