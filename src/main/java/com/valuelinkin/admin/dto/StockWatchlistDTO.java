package com.valuelinkin.admin.dto;

import com.valuelinkin.admin.dto.BaseDTO;

public class StockWatchlistDTO extends BaseDTO {
    private static final long serialVersionUID = 3148176768559230877L;
    

	private Long lst;
	private Long usrId;
	private java.lang.String stkCode;
	private Integer sortIndex;
	private java.util.Date createdOn;
	private java.util.Date modifiedOn;

	
	public Long getLst() {
		return this.lst;
	}
	
	public void setLst(Long value) {
		this.lst = value;
	}
	
	public Long getUsrId() {
		return this.usrId;
	}
	
	public void setUsrId(Long value) {
		this.usrId = value;
	}
	
	public java.lang.String getStkCode() {
		return this.stkCode;
	}
	
	public void setStkCode(java.lang.String value) {
		this.stkCode = value;
	}
	
	public Integer getSortIndex() {
		return this.sortIndex;
	}
	
	public void setSortIndex(Integer value) {
		this.sortIndex = value;
	}
	
	public java.util.Date getCreatedOn() {
		return this.createdOn;
	}
	
	public void setCreatedOn(java.util.Date value) {
		this.createdOn = value;
	}
	
	public java.util.Date getModifiedOn() {
		return this.modifiedOn;
	}
	
	public void setModifiedOn(java.util.Date value) {
		this.modifiedOn = value;
	}
	
}

