package com.valuelinkin.admin.service.core.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jcommon.orm.Page;
import com.jcommon.exception.BusinessException;
import com.jcommon.utils.BeanUtils;
import com.jcommon.result.ResponseResult;

import com.valuelinkin.admin.dao.entity.StockWatchlist;
import com.valuelinkin.admin.dto.StockWatchlistDTO;
import com.valuelinkin.admin.dao.mapper.StockWatchlistMapper;
import com.valuelinkin.admin.service.core.StockWatchlistService;
import com.valuelinkin.admin.service.UidGenerator;
import com.valuelinkin.admin.common.utils.AppConstants;

import tk.mybatis.mapper.entity.Example;

@Service
public class StockWatchlistServiceImpl implements StockWatchlistService{

	@Autowired
	private StockWatchlistMapper stockWatchlistMapper;
	
	@Autowired
	private UidGenerator uidGenerator;

	public ResponseResult findPage(StockWatchlistDTO dto) {
		ResponseResult result = new ResponseResult();
		
		StockWatchlist entity = new StockWatchlist();
		BeanUtils.copyProperties(entity, dto);
		
		Page page = new Page();
		page.setPageNo(dto.getPageNo());
		page.setPageSize(dto.getPageSize());
		page.setTotalCount(stockWatchlistMapper.selectCount(entity));
		
		Example example = new Example(StockWatchlist.class);
		//example.createCriteria().andEqualTo("refId", rowNews.getRefId());
		//example.setOrderByClause("nws_Publish_Date desc");
		
		List<StockWatchlist> list = stockWatchlistMapper.selectByExampleAndRowBounds(example, new RowBounds(page.getFirst(), page.getPageSize()));
		
		page.setResult(list);
		result.setData(page);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
		
	}	
	
	public ResponseResult getById(Long id) throws BusinessException{
		ResponseResult result = new ResponseResult();
		StockWatchlist entity = stockWatchlistMapper.selectByPrimaryKey(id);
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}

	
	/** 插入数据 */
	@Transactional
	public ResponseResult save(StockWatchlistDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		StockWatchlist entity = new StockWatchlist();
		BeanUtils.copyProperties(entity, dto);
		entity.setLst(uidGenerator.nextId());
		
		stockWatchlistMapper.insertSelective(entity);
		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult deleteById(Long id) throws BusinessException{
		ResponseResult result = new ResponseResult();
		stockWatchlistMapper.deleteByPrimaryKey(id);
		
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult delete(StockWatchlistDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		
		StockWatchlist entity = new StockWatchlist();
		BeanUtils.copyProperties(entity, dto);
		
		stockWatchlistMapper.delete(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult deleteByIds(String ids) throws BusinessException{
		ResponseResult result = new ResponseResult();
		String[] items = ids.split(",");
		for (String str : items) {
			Long id = new Long(str);
			stockWatchlistMapper.deleteByPrimaryKey(id);
		}
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult update(StockWatchlistDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		StockWatchlist entity = new StockWatchlist();
		BeanUtils.copyProperties(entity, dto);
		stockWatchlistMapper.updateByPrimaryKeySelective(entity);
		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	public ResponseResult findByExample(StockWatchlistDTO dto) throws BusinessException {
		ResponseResult result = new ResponseResult();
		
		StockWatchlist entity = new StockWatchlist();
		BeanUtils.copyProperties(entity, dto);
		
		List list =stockWatchlistMapper.select(entity);
		
		result.setData(list);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	
	public ResponseResult getByExample(StockWatchlistDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		
		StockWatchlist entity = new StockWatchlist();
		BeanUtils.copyProperties(entity, dto);
		
		entity =stockWatchlistMapper.selectOne(entity);
		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
		
	}
	
	public void setStockWatchlistMapper(StockWatchlistMapper dao) {
		this.stockWatchlistMapper = dao;
	}
}
