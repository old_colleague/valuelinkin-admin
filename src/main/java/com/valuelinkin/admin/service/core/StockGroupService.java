package com.valuelinkin.admin.service.core;

import java.util.List;

import com.jcommon.orm.Page;
import com.jcommon.exception.BusinessException;
import com.jcommon.result.ResponseResult;

import com.valuelinkin.admin.dto.StockGroupDTO;

public interface StockGroupService {

	ResponseResult findPage(StockGroupDTO dto) throws BusinessException;
	
	ResponseResult getById(Long id) throws BusinessException;

	ResponseResult save(StockGroupDTO dto) throws BusinessException;
	
	ResponseResult delete(StockGroupDTO dto) throws BusinessException ;
	
	ResponseResult deleteById(Long id) throws BusinessException ;
	
	ResponseResult update(StockGroupDTO dto) throws BusinessException ;
	
	ResponseResult findByExample(StockGroupDTO dto) throws BusinessException;
	
	ResponseResult getByExample(StockGroupDTO dto) throws BusinessException;
	
	ResponseResult deleteByIds(String ids) throws BusinessException ;
	
	ResponseResult findGroupStock(Long id) throws BusinessException;
	

}
