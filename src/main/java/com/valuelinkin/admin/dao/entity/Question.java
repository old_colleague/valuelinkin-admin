package com.valuelinkin.admin.dao.entity;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * 〈功能详细描述〉
 *
 * @author Nail.zhang
 * @version [V0.1, Jan 9, 2018]
 */

@Table(name = "vln_question")
public class Question implements java.io.Serializable {
    private static final long serialVersionUID = 5454155825314635342L;


    //columns START
    @Id
    @Column(name = "qsn_id")
    private Long qsnId;
    private Long nwsId;
    private Long usrId;
    private java.lang.String qsnContent;
    private Integer qsnStatus;
    private java.util.Date createdOn;
    private java.util.Date modifiedOn;
    //columns END


    public Question() {
    }

    public Question(
            Long qsnId
    ) {
        this.qsnId = qsnId;
    }


    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Date getModifiedOn() {
        return modifiedOn;
    }

    public void setModifiedOn(Date modifiedOn) {
        this.modifiedOn = modifiedOn;
    }


    public void setQsnId(Long value) {
        this.qsnId = value;
    }

    public Long getQsnId() {
        return this.qsnId;
    }

    public Long getNwsId() {
        return this.nwsId;
    }

    public void setNwsId(Long value) {
        this.nwsId = value;
    }

    public Long getUsrId() {
        return this.usrId;
    }

    public void setUsrId(Long value) {
        this.usrId = value;
    }

    public java.lang.String getQsnContent() {
        return this.qsnContent;
    }

    public void setQsnContent(java.lang.String value) {
        this.qsnContent = value;
    }

    public Integer getQsnStatus() {
        return this.qsnStatus;
    }

    public void setQsnStatus(Integer value) {
        this.qsnStatus = value;
    }


}

