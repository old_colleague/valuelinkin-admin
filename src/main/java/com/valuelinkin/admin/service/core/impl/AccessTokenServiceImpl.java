package com.valuelinkin.admin.service.core.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jcommon.orm.Page;
import com.jcommon.exception.BusinessException;
import com.jcommon.utils.BeanUtils;
import com.jcommon.result.ResponseResult;

import com.valuelinkin.admin.dao.entity.AccessToken;
import com.valuelinkin.admin.dto.AccessTokenDTO;
import com.valuelinkin.admin.dao.mapper.AccessTokenMapper;
import com.valuelinkin.admin.service.core.AccessTokenService;
import com.valuelinkin.admin.service.UidGenerator;
import com.valuelinkin.admin.common.utils.AppConstants;

import tk.mybatis.mapper.entity.Example;

@Service
public class AccessTokenServiceImpl implements AccessTokenService{

	@Autowired
	private AccessTokenMapper accessTokenMapper;
	
	@Autowired
	private UidGenerator uidGenerator;

	public ResponseResult findPage(AccessTokenDTO dto) {
		ResponseResult result = new ResponseResult();
		
		AccessToken entity = new AccessToken();
		BeanUtils.copyProperties(entity, dto);
		
		Page page = new Page();
		page.setPageNo(dto.getPageNo());
		page.setPageSize(dto.getPageSize());
		page.setTotalCount(accessTokenMapper.selectCount(entity));
		
		Example example = new Example(AccessToken.class);
		//example.createCriteria().andEqualTo("refId", rowNews.getRefId());
		//example.setOrderByClause("nws_Publish_Date desc");
		
		List<AccessToken> list = accessTokenMapper.selectByExampleAndRowBounds(example, new RowBounds(page.getFirst(), page.getPageSize()));
		
		page.setResult(list);
		result.setData(page);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
		
	}	
	
	public ResponseResult getById(Long id) throws BusinessException{
		ResponseResult result = new ResponseResult();
		AccessToken entity = accessTokenMapper.selectByPrimaryKey(id);
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}

	
	/** 插入数据 */
	@Transactional
	public ResponseResult save(AccessTokenDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		AccessToken entity = new AccessToken();
		BeanUtils.copyProperties(entity, dto);
		entity.setTknId(uidGenerator.nextId());
		
		accessTokenMapper.insertSelective(entity);
		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult deleteById(Long id) throws BusinessException{
		ResponseResult result = new ResponseResult();
		accessTokenMapper.deleteByPrimaryKey(id);
		
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult delete(AccessTokenDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		
		AccessToken entity = new AccessToken();
		BeanUtils.copyProperties(entity, dto);
		
		accessTokenMapper.delete(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult deleteByIds(String ids) throws BusinessException{
		ResponseResult result = new ResponseResult();
		String[] items = ids.split(",");
		for (String str : items) {
			Long id = new Long(str);
			accessTokenMapper.deleteByPrimaryKey(id);
		}
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult update(AccessTokenDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		AccessToken entity = new AccessToken();
		BeanUtils.copyProperties(entity, dto);
		accessTokenMapper.updateByPrimaryKeySelective(entity);
		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	public ResponseResult findByExample(AccessTokenDTO dto) throws BusinessException {
		ResponseResult result = new ResponseResult();
		
		AccessToken entity = new AccessToken();
		BeanUtils.copyProperties(entity, dto);
		
		List list =accessTokenMapper.select(entity);
		
		result.setData(list);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	
	public ResponseResult getByExample(AccessTokenDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		
		AccessToken entity = new AccessToken();
		BeanUtils.copyProperties(entity, dto);
		
		entity =accessTokenMapper.selectOne(entity);
		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
		
	}
	
	public void setAccessTokenMapper(AccessTokenMapper dao) {
		this.accessTokenMapper = dao;
	}
}
