package com.valuelinkin.admin.dto;

import com.valuelinkin.admin.dto.BaseDTO;

public class StockFinancialDataDTO extends BaseDTO {
    private static final long serialVersionUID = 3148176768559230877L;
    

	private Long sfdId;
	private java.lang.String stkCode;
	private java.lang.String sdfDataTitle;
	private java.util.Date createdOn;
	private java.util.Date modifiedOn;

	
	public Long getSfdId() {
		return this.sfdId;
	}
	
	public void setSfdId(Long value) {
		this.sfdId = value;
	}
	
	public java.lang.String getStkCode() {
		return this.stkCode;
	}
	
	public void setStkCode(java.lang.String value) {
		this.stkCode = value;
	}
	
	public java.lang.String getSdfDataTitle() {
		return this.sdfDataTitle;
	}
	
	public void setSdfDataTitle(java.lang.String value) {
		this.sdfDataTitle = value;
	}
	
	public java.util.Date getCreatedOn() {
		return this.createdOn;
	}
	
	public void setCreatedOn(java.util.Date value) {
		this.createdOn = value;
	}
	
	public java.util.Date getModifiedOn() {
		return this.modifiedOn;
	}
	
	public void setModifiedOn(java.util.Date value) {
		this.modifiedOn = value;
	}
	
}

