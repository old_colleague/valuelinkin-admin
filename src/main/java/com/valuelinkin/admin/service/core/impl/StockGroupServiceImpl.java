package com.valuelinkin.admin.service.core.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jcommon.orm.Page;
import com.jcommon.exception.BusinessException;
import com.jcommon.utils.BeanUtils;
import com.jcommon.result.ResponseResult;

import com.valuelinkin.admin.dao.entity.StockGroup;
import com.valuelinkin.admin.dto.GroupStockDTO;
import com.valuelinkin.admin.dto.StockGroupDTO;
import com.valuelinkin.admin.dao.mapper.StockGroupMapper;
import com.valuelinkin.admin.service.core.StockGroupService;
import com.valuelinkin.admin.service.UidGenerator;
import com.valuelinkin.admin.common.utils.AppConstants;

import tk.mybatis.mapper.entity.Example;

@Service
public class StockGroupServiceImpl implements StockGroupService{

	@Autowired
	private StockGroupMapper stockGroupMapper;
	
	@Autowired
	private UidGenerator uidGenerator;

	public ResponseResult findPage(StockGroupDTO dto) {
		ResponseResult result = new ResponseResult();
		
		StockGroup entity = new StockGroup();
		BeanUtils.copyProperties(entity, dto);
		
		Page page = new Page();
		page.setPageNo(dto.getPageNo());
		page.setPageSize(200);
		page.setTotalCount(stockGroupMapper.selectCount(entity));
		
		Example example = new Example(StockGroup.class);
		//example.createCriteria().andEqualTo("refId", rowNews.getRefId());
		example.setOrderByClause("sort_Index desc");
		
		List<StockGroup> list = stockGroupMapper.selectByExampleAndRowBounds(example, new RowBounds(page.getFirst(), page.getPageSize()));
		
		page.setResult(list);
		result.setData(page);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
		
	}	
	
	public ResponseResult getById(Long id) throws BusinessException{
		ResponseResult result = new ResponseResult();
		StockGroup entity = stockGroupMapper.selectByPrimaryKey(id);
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	public ResponseResult findGroupStock(Long id) throws BusinessException{
		ResponseResult result = new ResponseResult();
		List<GroupStockDTO>  list = stockGroupMapper.findGroupStock(id);
		result.setData(list);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}

	
	/** 插入数据 */
	@Transactional
	public ResponseResult save(StockGroupDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		StockGroup entity = new StockGroup();
		BeanUtils.copyProperties(entity, dto);
		entity.setId(uidGenerator.nextId());
		
		stockGroupMapper.insertSelective(entity);
		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult deleteById(Long id) throws BusinessException{
		ResponseResult result = new ResponseResult();
		stockGroupMapper.deleteByPrimaryKey(id);
		
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult delete(StockGroupDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		
		StockGroup entity = new StockGroup();
		BeanUtils.copyProperties(entity, dto);
		
		stockGroupMapper.delete(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult deleteByIds(String ids) throws BusinessException{
		ResponseResult result = new ResponseResult();
		String[] items = ids.split(",");
		for (String str : items) {
			Long id = new Long(str);
			stockGroupMapper.deleteByPrimaryKey(id);
		}
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult update(StockGroupDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		
		List<Long> idList = dto.getItmId();
		int index = idList.size();
		for (Long id : idList) { 
			StockGroup entity = new StockGroup();
			entity.setId(id);
			entity.setSortIndex(index--);
			stockGroupMapper.updateByPrimaryKeySelective(entity);
		}
		
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	public ResponseResult findByExample(StockGroupDTO dto) throws BusinessException {
		ResponseResult result = new ResponseResult();
		
		StockGroup entity = new StockGroup();
		BeanUtils.copyProperties(entity, dto);
		
		List list =stockGroupMapper.select(entity);
		
		result.setData(list);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	
	public ResponseResult getByExample(StockGroupDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		
		StockGroup entity = new StockGroup();
		BeanUtils.copyProperties(entity, dto);
		
		entity =stockGroupMapper.selectOne(entity);
		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
		
	}
	
	public void setStockGroupMapper(StockGroupMapper dao) {
		this.stockGroupMapper = dao;
	}
}
