package com.valuelinkin.admin.dao.mapper;


import org.apache.ibatis.annotations.Mapper;

import com.valuelinkin.admin.dao.entity.Group;
import com.valuelinkin.admin.dao.BaseMapper;

@Mapper
public interface GroupMapper extends BaseMapper<Group>{
	

}
