package com.valuelinkin.admin.dao.mapper;


import org.apache.ibatis.annotations.Mapper;

import com.valuelinkin.admin.dao.entity.Stock;
import com.valuelinkin.admin.dao.BaseMapper;

@Mapper
public interface StockMapper extends BaseMapper<Stock>{
	

}
