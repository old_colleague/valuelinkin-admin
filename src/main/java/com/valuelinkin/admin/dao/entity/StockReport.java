package com.valuelinkin.admin.dao.entity;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 〈功能详细描述〉
 *
 * @author Nail.zhang
 * @version [V0.1, Jan 9, 2018]
 */

@Table(name="vln_stock_report")
public class StockReport implements java.io.Serializable{
	private static final long serialVersionUID = 5454155825314635342L;
	

	//columns START
		@Id
		@Column(name = "id")
		private Long id;
		private java.lang.String stkCode;
		private java.lang.String rptTitle;
		private java.lang.String rptSource;
		private java.lang.String rptContent;
		private java.util.Date rptPublishDate;
		private java.util.Date createdOn;
		private java.util.Date modifiedOn;
	//columns END


	public StockReport(){
	}

	public StockReport(
		Long id
	){
		this.id = id;
	}

	

	public void setId(Long value) {
		this.id = value;
	}
	
	public Long getId() {
		return this.id;
	}
	
	public java.lang.String getStkCode() {
		return this.stkCode;
	}
	
	public void setStkCode(java.lang.String value) {
		this.stkCode = value;
	}
	
	public java.lang.String getRptTitle() {
		return this.rptTitle;
	}
	
	public void setRptTitle(java.lang.String value) {
		this.rptTitle = value;
	}
	
	public java.lang.String getRptSource() {
		return this.rptSource;
	}
	
	public void setRptSource(java.lang.String value) {
		this.rptSource = value;
	}
	
	public java.lang.String getRptContent() {
		return this.rptContent;
	}
	
	public void setRptContent(java.lang.String value) {
		this.rptContent = value;
	}
	
		
	public java.util.Date getRptPublishDate() {
		return this.rptPublishDate;
	}
	
	public void setRptPublishDate(java.util.Date value) {
		this.rptPublishDate = value;
	}
	
		
	public java.util.Date getCreatedOn() {
		return this.createdOn;
	}
	
	public void setCreatedOn(java.util.Date value) {
		this.createdOn = value;
	}
	
		
	public java.util.Date getModifiedOn() {
		return this.modifiedOn;
	}
	
	public void setModifiedOn(java.util.Date value) {
		this.modifiedOn = value;
	}
	

}

