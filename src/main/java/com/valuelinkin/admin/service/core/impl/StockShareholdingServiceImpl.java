package com.valuelinkin.admin.service.core.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jcommon.orm.Page;
import com.jcommon.exception.BusinessException;
import com.jcommon.utils.BeanUtils;
import com.jcommon.result.ResponseResult;

import com.valuelinkin.admin.dao.entity.StockShareholding;
import com.valuelinkin.admin.dto.StockShareholdingDTO;
import com.valuelinkin.admin.dao.mapper.StockShareholdingMapper;
import com.valuelinkin.admin.service.core.StockShareholdingService;
import com.valuelinkin.admin.service.UidGenerator;
import com.valuelinkin.admin.common.utils.AppConstants;

import tk.mybatis.mapper.entity.Example;

@Service
public class StockShareholdingServiceImpl implements StockShareholdingService{

	@Autowired
	private StockShareholdingMapper stockShareholdingMapper;
	
	@Autowired
	private UidGenerator uidGenerator;

	public ResponseResult findPage(StockShareholdingDTO dto) {
		ResponseResult result = new ResponseResult();
		
		StockShareholding entity = new StockShareholding();
		BeanUtils.copyProperties(entity, dto);
		
		Page page = new Page();
		page.setPageNo(dto.getPageNo());
		page.setPageSize(dto.getPageSize());
		page.setTotalCount(stockShareholdingMapper.selectCount(entity));
		
		Example example = new Example(StockShareholding.class);
		//example.createCriteria().andEqualTo("refId", rowNews.getRefId());
		//example.setOrderByClause("nws_Publish_Date desc");
		
		List<StockShareholding> list = stockShareholdingMapper.selectByExampleAndRowBounds(example, new RowBounds(page.getFirst(), page.getPageSize()));
		
		page.setResult(list);
		result.setData(page);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
		
	}	
	
	public ResponseResult getById(Long id) throws BusinessException{
		ResponseResult result = new ResponseResult();
		StockShareholding entity = stockShareholdingMapper.selectByPrimaryKey(id);
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}

	
	/** 插入数据 */
	@Transactional
	public ResponseResult save(StockShareholdingDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		StockShareholding entity = new StockShareholding();
		BeanUtils.copyProperties(entity, dto);
		entity.setId(uidGenerator.nextId());
		
		stockShareholdingMapper.insertSelective(entity);
		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult deleteById(Long id) throws BusinessException{
		ResponseResult result = new ResponseResult();
		stockShareholdingMapper.deleteByPrimaryKey(id);
		
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult delete(StockShareholdingDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		
		StockShareholding entity = new StockShareholding();
		BeanUtils.copyProperties(entity, dto);
		
		stockShareholdingMapper.delete(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult deleteByIds(String ids) throws BusinessException{
		ResponseResult result = new ResponseResult();
		String[] items = ids.split(",");
		for (String str : items) {
			Long id = new Long(str);
			stockShareholdingMapper.deleteByPrimaryKey(id);
		}
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult update(StockShareholdingDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		StockShareholding entity = new StockShareholding();
		BeanUtils.copyProperties(entity, dto);
		stockShareholdingMapper.updateByPrimaryKeySelective(entity);
		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	public ResponseResult findByExample(StockShareholdingDTO dto) throws BusinessException {
		ResponseResult result = new ResponseResult();
		
		StockShareholding entity = new StockShareholding();
		BeanUtils.copyProperties(entity, dto);
		
		List list =stockShareholdingMapper.select(entity);
		
		result.setData(list);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	
	public ResponseResult getByExample(StockShareholdingDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		
		StockShareholding entity = new StockShareholding();
		BeanUtils.copyProperties(entity, dto);
		
		entity =stockShareholdingMapper.selectOne(entity);
		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
		
	}
	
	public void setStockShareholdingMapper(StockShareholdingMapper dao) {
		this.stockShareholdingMapper = dao;
	}
}
