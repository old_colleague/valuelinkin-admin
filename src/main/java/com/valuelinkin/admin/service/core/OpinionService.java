package com.valuelinkin.admin.service.core;

import java.util.List;

import com.jcommon.orm.Page;
import com.jcommon.exception.BusinessException;
import com.jcommon.result.ResponseResult;

import com.valuelinkin.admin.dto.OpinionDTO;

public interface OpinionService {

	ResponseResult findPage(OpinionDTO dto) throws BusinessException;
	
	ResponseResult getById(Long id) throws BusinessException;

	ResponseResult save(OpinionDTO dto) throws BusinessException;
	
	ResponseResult delete(OpinionDTO dto) throws BusinessException ;
	
	ResponseResult deleteById(Long id) throws BusinessException ;
	
	ResponseResult update(OpinionDTO dto) throws BusinessException ;
	
	ResponseResult findByExample(OpinionDTO dto) throws BusinessException;
	
	ResponseResult getByExample(OpinionDTO dto) throws BusinessException;
	
	ResponseResult deleteByIds(String ids) throws BusinessException ;
	

}
