package com.valuelinkin.admin;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.jcommon.exception.BusinessException;
import com.jcommon.result.ResponseResult;

/**
 * 
 * 
 * @atuhor: nail.zhang
 * @description:
 * @date: Created in  2018年11月27日
 */
@ControllerAdvice
public class GlobalExceptionResolver {
	
	private static Logger logger = LoggerFactory.getLogger(GlobalExceptionResolver.class);

    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public ResponseResult<String> jsonErrorHandler(HttpServletRequest request, Exception ex) throws Exception {
    	ResponseResult<String> result = new ResponseResult<>();
    	result.setErrorCode(-1);
    	
    	String errorMessage = "";
    	String errorKey = "";
		if(ex instanceof BusinessException){
			logger.error("异常：",ex);
			BusinessException e = (BusinessException)ex;
			errorMessage = getMessage(request,e.getMessage(), e.getPara());
			errorKey = e.getMessage();
		}else if(ex instanceof HttpRequestMethodNotSupportedException){
			logger.error(request.getServletPath());
			errorMessage = request.getMethod()+"方法不支持";
			errorKey = "info.system_error";
		}else{
			logger.error("异常：",ex);
			errorMessage = getMessage(request,"info.system_error",null);
			errorKey = "info.system_error";
		}
		
    	result.setErrorMessage(errorMessage);
    	result.setErrorKey(errorKey);
        return result;
    }
    
    public String getMessage(HttpServletRequest request, String key, List<Object> params) {
		WebApplicationContext ac = RequestContextUtils.findWebApplicationContext(request);
		Object[] args = {};
		if(params!=null) {
			args = params.toArray();
		}
			
		String msg=null;
		try {
			msg = ac.getMessage(key, args, Locale.CHINA);
		} catch (Exception e) {
			msg= key;
		}
		
		return msg;
		
	}
}
