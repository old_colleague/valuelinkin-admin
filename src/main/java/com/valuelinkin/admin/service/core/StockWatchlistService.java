package com.valuelinkin.admin.service.core;

import java.util.List;

import com.jcommon.orm.Page;
import com.jcommon.exception.BusinessException;
import com.jcommon.result.ResponseResult;

import com.valuelinkin.admin.dto.StockWatchlistDTO;

public interface StockWatchlistService {

	ResponseResult findPage(StockWatchlistDTO dto) throws BusinessException;
	
	ResponseResult getById(Long id) throws BusinessException;

	ResponseResult save(StockWatchlistDTO dto) throws BusinessException;
	
	ResponseResult delete(StockWatchlistDTO dto) throws BusinessException ;
	
	ResponseResult deleteById(Long id) throws BusinessException ;
	
	ResponseResult update(StockWatchlistDTO dto) throws BusinessException ;
	
	ResponseResult findByExample(StockWatchlistDTO dto) throws BusinessException;
	
	ResponseResult getByExample(StockWatchlistDTO dto) throws BusinessException;
	
	ResponseResult deleteByIds(String ids) throws BusinessException ;
	

}
