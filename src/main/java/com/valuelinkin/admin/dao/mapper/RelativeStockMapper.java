package com.valuelinkin.admin.dao.mapper;


import org.apache.ibatis.annotations.Mapper;

import com.valuelinkin.admin.dao.entity.RelativeStock;
import com.valuelinkin.admin.dao.BaseMapper;

@Mapper
public interface RelativeStockMapper extends BaseMapper<RelativeStock>{
	

}
