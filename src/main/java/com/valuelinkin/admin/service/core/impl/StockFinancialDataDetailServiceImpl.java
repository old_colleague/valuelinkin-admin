package com.valuelinkin.admin.service.core.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jcommon.orm.Page;
import com.jcommon.exception.BusinessException;
import com.jcommon.utils.BeanUtils;
import com.jcommon.result.ResponseResult;

import com.valuelinkin.admin.dao.entity.StockFinancialDataDetail;
import com.valuelinkin.admin.dto.StockFinancialDataDetailDTO;
import com.valuelinkin.admin.dao.mapper.StockFinancialDataDetailMapper;
import com.valuelinkin.admin.service.core.StockFinancialDataDetailService;
import com.valuelinkin.admin.service.UidGenerator;
import com.valuelinkin.admin.common.utils.AppConstants;

import tk.mybatis.mapper.entity.Example;

@Service
public class StockFinancialDataDetailServiceImpl implements StockFinancialDataDetailService{

	@Autowired
	private StockFinancialDataDetailMapper stockFinancialDataDetailMapper;
	
	@Autowired
	private UidGenerator uidGenerator;

	public ResponseResult findPage(StockFinancialDataDetailDTO dto) {
		ResponseResult result = new ResponseResult();
		
		StockFinancialDataDetail entity = new StockFinancialDataDetail();
		BeanUtils.copyProperties(entity, dto);
		
		Page page = new Page();
		page.setPageNo(dto.getPageNo());
		page.setPageSize(dto.getPageSize());
		page.setTotalCount(stockFinancialDataDetailMapper.selectCount(entity));
		
		Example example = new Example(StockFinancialDataDetail.class);
		//example.createCriteria().andEqualTo("refId", rowNews.getRefId());
		//example.setOrderByClause("nws_Publish_Date desc");
		
		List<StockFinancialDataDetail> list = stockFinancialDataDetailMapper.selectByExampleAndRowBounds(example, new RowBounds(page.getFirst(), page.getPageSize()));
		
		page.setResult(list);
		result.setData(page);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
		
	}	
	
	public ResponseResult getById(Long id) throws BusinessException{
		ResponseResult result = new ResponseResult();
		StockFinancialDataDetail entity = stockFinancialDataDetailMapper.selectByPrimaryKey(id);
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}

	
	/** 插入数据 */
	@Transactional
	public ResponseResult save(StockFinancialDataDetailDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		StockFinancialDataDetail entity = new StockFinancialDataDetail();
		BeanUtils.copyProperties(entity, dto);
		entity.setDtlId(uidGenerator.nextId());
		
		stockFinancialDataDetailMapper.insertSelective(entity);
		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult deleteById(Long id) throws BusinessException{
		ResponseResult result = new ResponseResult();
		stockFinancialDataDetailMapper.deleteByPrimaryKey(id);
		
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult delete(StockFinancialDataDetailDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		
		StockFinancialDataDetail entity = new StockFinancialDataDetail();
		BeanUtils.copyProperties(entity, dto);
		
		stockFinancialDataDetailMapper.delete(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult deleteByIds(String ids) throws BusinessException{
		ResponseResult result = new ResponseResult();
		String[] items = ids.split(",");
		for (String str : items) {
			Long id = new Long(str);
			stockFinancialDataDetailMapper.deleteByPrimaryKey(id);
		}
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult update(StockFinancialDataDetailDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		StockFinancialDataDetail entity = new StockFinancialDataDetail();
		BeanUtils.copyProperties(entity, dto);
		stockFinancialDataDetailMapper.updateByPrimaryKeySelective(entity);
		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	public ResponseResult findByExample(StockFinancialDataDetailDTO dto) throws BusinessException {
		ResponseResult result = new ResponseResult();
		
		StockFinancialDataDetail entity = new StockFinancialDataDetail();
		BeanUtils.copyProperties(entity, dto);
		
		List list =stockFinancialDataDetailMapper.select(entity);
		
		result.setData(list);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	
	public ResponseResult getByExample(StockFinancialDataDetailDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		
		StockFinancialDataDetail entity = new StockFinancialDataDetail();
		BeanUtils.copyProperties(entity, dto);
		
		entity =stockFinancialDataDetailMapper.selectOne(entity);
		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
		
	}
	
	public void setStockFinancialDataDetailMapper(StockFinancialDataDetailMapper dao) {
		this.stockFinancialDataDetailMapper = dao;
	}
}
