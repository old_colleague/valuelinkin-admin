package com.valuelinkin.admin.dao.mapper;


import org.apache.ibatis.annotations.Mapper;

import com.valuelinkin.admin.dao.entity.StockReport;
import com.valuelinkin.admin.dao.BaseMapper;

@Mapper
public interface StockReportMapper extends BaseMapper<StockReport>{
	

}
