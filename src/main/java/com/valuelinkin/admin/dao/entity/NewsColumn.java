package com.valuelinkin.admin.dao.entity;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 〈功能详细描述〉
 *
 * @author Nail.zhang
 * @version [V0.1, Jan 9, 2018]
 */

@Table(name="vln_news_column")
public class NewsColumn implements java.io.Serializable{
	private static final long serialVersionUID = 5454155825314635342L;
	

	//columns START
		@Id
		@Column(name = "clm_id")
		private Long clmId;
		private Long parentId;
		private java.lang.String clmCode;
		private java.lang.String clmDisplayName;
		private java.lang.String clmGroup;
		private Integer clmStatus;
		private java.lang.String clmRemark;
		private java.util.Date createdOn;
		private java.util.Date modifiedOn;
	//columns END


	public NewsColumn(){
	}

	public NewsColumn(
		Long clmId
	){
		this.clmId = clmId;
	}

	

	public void setClmId(Long value) {
		this.clmId = value;
	}
	
	public Long getClmId() {
		return this.clmId;
	}
	
	public Long getParentId() {
		return this.parentId;
	}
	
	public void setParentId(Long value) {
		this.parentId = value;
	}
	
	public java.lang.String getClmCode() {
		return this.clmCode;
	}
	
	public void setClmCode(java.lang.String value) {
		this.clmCode = value;
	}
	
	public java.lang.String getClmDisplayName() {
		return this.clmDisplayName;
	}
	
	public void setClmDisplayName(java.lang.String value) {
		this.clmDisplayName = value;
	}
	
	public java.lang.String getClmGroup() {
		return this.clmGroup;
	}
	
	public void setClmGroup(java.lang.String value) {
		this.clmGroup = value;
	}
	
	public Integer getClmStatus() {
		return this.clmStatus;
	}
	
	public void setClmStatus(Integer value) {
		this.clmStatus = value;
	}
	
	public java.lang.String getClmRemark() {
		return this.clmRemark;
	}
	
	public void setClmRemark(java.lang.String value) {
		this.clmRemark = value;
	}
	
		
	public java.util.Date getCreatedOn() {
		return this.createdOn;
	}
	
	public void setCreatedOn(java.util.Date value) {
		this.createdOn = value;
	}
	
		
	public java.util.Date getModifiedOn() {
		return this.modifiedOn;
	}
	
	public void setModifiedOn(java.util.Date value) {
		this.modifiedOn = value;
	}
	

}

