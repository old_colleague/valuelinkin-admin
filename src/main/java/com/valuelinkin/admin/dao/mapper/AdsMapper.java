package com.valuelinkin.admin.dao.mapper;


import org.apache.ibatis.annotations.Mapper;

import com.valuelinkin.admin.dao.entity.Ads;
import com.valuelinkin.admin.dao.BaseMapper;

@Mapper
public interface AdsMapper extends BaseMapper<Ads>{
	

}
