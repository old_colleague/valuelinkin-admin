package com.valuelinkin.admin.dao.entity;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 〈功能详细描述〉
 *
 * @author Nail.zhang
 * @version [V0.1, Jan 9, 2018]
 */

@Table(name="vln_stock")
public class Stock implements java.io.Serializable{
	private static final long serialVersionUID = 5454155825314635342L;
	

	//columns START
		@Id
		@Column(name = "stk_id")
		private Long stkId;
		private java.lang.String stkCode;
		private java.lang.String stkName;
		private java.lang.String stkLogo;
		private java.lang.String stkKeys;
		private Integer stkShareholders;
		private Double stkCirculatingSharesRate;
		private java.util.Date createdOn;
		private java.util.Date modifiedOn;
	//columns END


	public Stock(){
	}

	public Stock(
		Long stkId
	){
		this.stkId = stkId;
	}

	

	public void setStkId(Long value) {
		this.stkId = value;
	}
	
	public Long getStkId() {
		return this.stkId;
	}
	
	public java.lang.String getStkCode() {
		return this.stkCode;
	}
	
	public void setStkCode(java.lang.String value) {
		this.stkCode = value;
	}
	
	public java.lang.String getStkName() {
		return this.stkName;
	}
	
	public void setStkName(java.lang.String value) {
		this.stkName = value;
	}
	
	public java.lang.String getStkLogo() {
		return this.stkLogo;
	}
	
	public void setStkLogo(java.lang.String value) {
		this.stkLogo = value;
	}
	
	public java.lang.String getStkKeys() {
		return this.stkKeys;
	}
	
	public void setStkKeys(java.lang.String value) {
		this.stkKeys = value;
	}
	
	public Integer getStkShareholders() {
		return this.stkShareholders;
	}
	
	public void setStkShareholders(Integer value) {
		this.stkShareholders = value;
	}
	
	public Double getStkCirculatingSharesRate() {
		return this.stkCirculatingSharesRate;
	}
	
	public void setStkCirculatingSharesRate(Double value) {
		this.stkCirculatingSharesRate = value;
	}
	
		
	public java.util.Date getCreatedOn() {
		return this.createdOn;
	}
	
	public void setCreatedOn(java.util.Date value) {
		this.createdOn = value;
	}
	
		
	public java.util.Date getModifiedOn() {
		return this.modifiedOn;
	}
	
	public void setModifiedOn(java.util.Date value) {
		this.modifiedOn = value;
	}
	

}

