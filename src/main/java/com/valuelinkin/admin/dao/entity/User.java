package com.valuelinkin.admin.dao.entity;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 〈功能详细描述〉
 *
 * @author Nail.zhang
 * @version [V0.1, Jan 9, 2018]
 */

@Table(name="vln_user")
public class User implements java.io.Serializable{
	private static final long serialVersionUID = 5454155825314635342L;
	

	//columns START
		@Id
		@Column(name = "usr_id")
		private Long usrId;
		private java.lang.String usrName;
		private java.lang.String usrPassword;
		private java.lang.String usrNickName;
		private java.lang.String usrPhone;
		private java.lang.String usrOpenId;
		private java.lang.String usrUnionId;
		private java.lang.String usrHeadImg;
		private java.lang.String usrRole;
		private Integer usrStatus;
		private java.util.Date createdOn;
		private java.util.Date modifiedOn;
		private java.lang.String createBy;
	//columns END


	public User(){
	}

	public User(
		Long usrId
	){
		this.usrId = usrId;
	}

	

	public void setUsrId(Long value) {
		this.usrId = value;
	}
	
	public Long getUsrId() {
		return this.usrId;
	}
	
	public java.lang.String getUsrName() {
		return this.usrName;
	}
	
	public void setUsrName(java.lang.String value) {
		this.usrName = value;
	}
	
	public java.lang.String getUsrPassword() {
		return this.usrPassword;
	}
	
	public void setUsrPassword(java.lang.String value) {
		this.usrPassword = value;
	}
	
	public java.lang.String getUsrNickName() {
		return this.usrNickName;
	}
	
	public void setUsrNickName(java.lang.String value) {
		this.usrNickName = value;
	}
	
	public java.lang.String getUsrPhone() {
		return this.usrPhone;
	}
	
	public void setUsrPhone(java.lang.String value) {
		this.usrPhone = value;
	}
	
	public java.lang.String getUsrOpenId() {
		return this.usrOpenId;
	}
	
	public void setUsrOpenId(java.lang.String value) {
		this.usrOpenId = value;
	}
	
	public java.lang.String getUsrUnionId() {
		return this.usrUnionId;
	}
	
	public void setUsrUnionId(java.lang.String value) {
		this.usrUnionId = value;
	}
	
	public java.lang.String getUsrHeadImg() {
		return this.usrHeadImg;
	}
	
	public void setUsrHeadImg(java.lang.String value) {
		this.usrHeadImg = value;
	}
	
	public java.lang.String getUsrRole() {
		return this.usrRole;
	}
	
	public void setUsrRole(java.lang.String value) {
		this.usrRole = value;
	}
	
	public Integer getUsrStatus() {
		return this.usrStatus;
	}
	
	public void setUsrStatus(Integer value) {
		this.usrStatus = value;
	}
	
		
	public java.util.Date getCreatedOn() {
		return this.createdOn;
	}
	
	public void setCreatedOn(java.util.Date value) {
		this.createdOn = value;
	}
	
		
	public java.util.Date getModifiedOn() {
		return this.modifiedOn;
	}
	
	public void setModifiedOn(java.util.Date value) {
		this.modifiedOn = value;
	}
	
	public java.lang.String getCreateBy() {
		return this.createBy;
	}
	
	public void setCreateBy(java.lang.String value) {
		this.createBy = value;
	}
	

}

