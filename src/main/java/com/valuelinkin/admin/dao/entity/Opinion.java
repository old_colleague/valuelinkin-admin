package com.valuelinkin.admin.dao.entity;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 〈功能详细描述〉
 *
 * @author Nail.zhang
 * @version [V0.1, Jan 9, 2018]
 */

@Table(name = "vln_opinion")
public class Opinion implements java.io.Serializable {
    private static final long serialVersionUID = 5454155825314635342L;


    //columns START
    @Id
    @Column(name = "opn_id")
    private Long opnId;
    private Long clmId;
    private Long usrId;
    private java.lang.String opnTitle;
    private java.lang.String opnBrief;
    private java.lang.String opnContent;
    private java.util.Date opnPublishDate;
    private Integer opnStatus;
    private java.lang.String opnStockCode;
    private Integer opnViewCount;
    private Integer opnStickTop;
    private java.lang.String opnColor;
    private java.lang.String opnThumbnailUrl;
    private java.lang.String opnVideoUrl;
    private java.util.Date createdOn;
    private java.util.Date modifiedOn;
    private Integer opnType;

    //columns END

    public Integer getOpnType() {
        return opnType;
    }

    public void setOpnType(Integer opnType) {
        this.opnType = opnType;
    }

    public Opinion() {
    }

    public Opinion(
            Long opnId
    ) {
        this.opnId = opnId;
    }


    public void setOpnId(Long value) {
        this.opnId = value;
    }

    public Long getOpnId() {
        return this.opnId;
    }

    public Long getClmId() {
        return this.clmId;
    }

    public void setClmId(Long value) {
        this.clmId = value;
    }

    public Long getUsrId() {
        return this.usrId;
    }

    public void setUsrId(Long value) {
        this.usrId = value;
    }

    public java.lang.String getOpnTitle() {
        return this.opnTitle;
    }

    public void setOpnTitle(java.lang.String value) {
        this.opnTitle = value;
    }

    public java.lang.String getOpnBrief() {
        return this.opnBrief;
    }

    public void setOpnBrief(java.lang.String value) {
        this.opnBrief = value;
    }

    public java.lang.String getOpnContent() {
        return this.opnContent;
    }

    public void setOpnContent(java.lang.String value) {
        this.opnContent = value;
    }

    public Integer getOpnStatus() {
        return this.opnStatus;
    }

    public void setOpnStatus(Integer value) {
        this.opnStatus = value;
    }

    public java.lang.String getOpnStockCode() {
        return this.opnStockCode;
    }

    public void setOpnStockCode(java.lang.String value) {
        this.opnStockCode = value;
    }

    public Integer getOpnViewCount() {
        return this.opnViewCount;
    }

    public void setOpnViewCount(Integer value) {
        this.opnViewCount = value;
    }

    public Integer getOpnStickTop() {
        return this.opnStickTop;
    }

    public void setOpnStickTop(Integer value) {
        this.opnStickTop = value;
    }

    public java.lang.String getOpnColor() {
        return this.opnColor;
    }

    public void setOpnColor(java.lang.String value) {
        this.opnColor = value;
    }

    public java.lang.String getOpnThumbnailUrl() {
        return this.opnThumbnailUrl;
    }

    public void setOpnThumbnailUrl(java.lang.String value) {
        this.opnThumbnailUrl = value;
    }

    public java.lang.String getOpnVideoUrl() {
        return this.opnVideoUrl;
    }

    public void setOpnVideoUrl(java.lang.String value) {
        this.opnVideoUrl = value;
    }


    public java.util.Date getCreatedOn() {
        return this.createdOn;
    }

    public void setCreatedOn(java.util.Date value) {
        this.createdOn = value;
    }


    public java.util.Date getModifiedOn() {
        return this.modifiedOn;
    }

    public void setModifiedOn(java.util.Date value) {
        this.modifiedOn = value;
    }

    /**
     * @return the opnPublishDate
     */
    public java.util.Date getOpnPublishDate() {
        return opnPublishDate;
    }

    /**
     * @param opnPublishDate the opnPublishDate to set
     */
    public void setOpnPublishDate(java.util.Date opnPublishDate) {
        this.opnPublishDate = opnPublishDate;
    }


}

