package com.valuelinkin.admin.service.core.impl;

import java.util.*;

import com.valuelinkin.admin.dto.PrivilegeListDTO;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jcommon.exception.BusinessException;
import com.jcommon.orm.Page;
import com.jcommon.result.ResponseResult;
import com.jcommon.utils.BeanUtils;
import com.valuelinkin.admin.common.utils.AppConstants;
import com.valuelinkin.admin.dao.entity.SysPrivilege;
import com.valuelinkin.admin.dao.mapper.SysPrivilegeMapper;
import com.valuelinkin.admin.dto.SysPrivilegeDTO;
import com.valuelinkin.admin.service.UidGenerator;
import com.valuelinkin.admin.service.core.SysPrivilegeService;

import tk.mybatis.mapper.entity.Example;

@Service
public class SysPrivilegeServiceImpl implements SysPrivilegeService {

    @Autowired
    private SysPrivilegeMapper sysPrivilegeMapper;

    @Autowired
    private UidGenerator uidGenerator;

    public ResponseResult findPage(SysPrivilegeDTO dto) {
        ResponseResult result = new ResponseResult();

        SysPrivilege entity = new SysPrivilege();
        BeanUtils.copyProperties(entity, dto);

        Page page = new Page();
        page.setPageNo(dto.getPageNo());
        page.setPageSize(dto.getPageSize());
        page.setTotalCount(sysPrivilegeMapper.selectCount(entity));

        Example example = new Example(SysPrivilege.class);
        //example.createCriteria().andEqualTo("refId", rowNews.getRefId());
        //example.setOrderByClause("nws_Publish_Date desc");

        List<SysPrivilege> list = sysPrivilegeMapper.selectByExampleAndRowBounds(example, new RowBounds(page.getFirst(), page.getPageSize()));

        page.setResult(list);
        result.setData(page);
        result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
        return result;

    }

    public ResponseResult getById(Long id) throws BusinessException {
        ResponseResult result = new ResponseResult();
        SysPrivilege entity = sysPrivilegeMapper.selectByPrimaryKey(id);
        result.setData(entity);
        result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
        return result;
    }


    /**
     * 插入数据
     */
    @Transactional
    public ResponseResult save(SysPrivilegeDTO dto) throws BusinessException {
        ResponseResult result = new ResponseResult();
        SysPrivilege entity = new SysPrivilege();
        BeanUtils.copyProperties(entity, dto);
        Date now = new Date();
        entity.setPvlId(uidGenerator.nextId());
        entity.setPvlStatus(0);
        entity.setCreatedOn(now);
        entity.setModifiedOn(now);
        sysPrivilegeMapper.insertSelective(entity);

        result.setData(entity);
        result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
        return result;
    }

    @Transactional
    public ResponseResult deleteById(Long id) throws BusinessException {
        ResponseResult result = new ResponseResult();
        sysPrivilegeMapper.deleteByPrimaryKey(id);

        result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
        return result;
    }

    @Transactional
    public ResponseResult delete(SysPrivilegeDTO dto) throws BusinessException {
        ResponseResult result = new ResponseResult();

        SysPrivilege entity = new SysPrivilege();
        BeanUtils.copyProperties(entity, dto);

        sysPrivilegeMapper.delete(entity);
        result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
        return result;
    }

    @Transactional
    public ResponseResult deleteByIds(String ids) throws BusinessException {
        ResponseResult result = new ResponseResult();
        String[] items = ids.split(",");
        for (String str : items) {
            Long id = new Long(str);
            sysPrivilegeMapper.deleteByPrimaryKey(id);
        }
        result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
        return result;
    }

    @Transactional
    public ResponseResult update(SysPrivilegeDTO dto) throws BusinessException {
        ResponseResult result = new ResponseResult();
        SysPrivilege entity = new SysPrivilege();
        BeanUtils.copyProperties(entity, dto);
        Date now = new Date();
        entity.setModifiedOn(now);

        sysPrivilegeMapper.updateByPrimaryKeySelective(entity);

        result.setData(entity);
        result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
        return result;
    }

    public ResponseResult<Collection<PrivilegeListDTO>> findRolePvlList(Long rleId) throws BusinessException{
        ResponseResult result = new ResponseResult();

        List<PrivilegeListDTO> pvlList = sysPrivilegeMapper.findRolePvlList(rleId);
        Map<Long, PrivilegeListDTO> pvlMap = new LinkedHashMap();
        for (int i = 0; i < pvlList.size(); i++) {
            PrivilegeListDTO sysPrivilege = pvlList.get(i);
            if (sysPrivilege.getParentId() == 0) {
                List<PrivilegeListDTO> childs = new ArrayList();
                sysPrivilege.setChilds(childs);
                pvlMap.put(sysPrivilege.getPvlId(), sysPrivilege);
            } else {
                PrivilegeListDTO privilegeListDTO = pvlMap.get(sysPrivilege.getParentId());
                List<PrivilegeListDTO> childs = privilegeListDTO.getChilds();
                childs.add(sysPrivilege);
                privilegeListDTO.setChilds(childs);
            }

        }

        result.setData(pvlMap.values());
        result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
        return result;
    }


    public ResponseResult<Collection<PrivilegeListDTO>> findRolePvlToEdit(Long rleId) throws BusinessException {

        ResponseResult<Collection<PrivilegeListDTO>> result =findAllPvlList();
        Collection<PrivilegeListDTO> list = result.getData();

        List<PrivilegeListDTO> rolePvlList = sysPrivilegeMapper.findRolePvlList(rleId);
        List<Long> pvlIdList = new ArrayList<>();
        for (PrivilegeListDTO privilegeListDTO : rolePvlList) {
            pvlIdList.add(privilegeListDTO.getPvlId());
        }

        for (PrivilegeListDTO privilegeListDTO : list) {
            if(pvlIdList.contains(privilegeListDTO.getPvlId())){
                privilegeListDTO.setChecked("checked");
            }
            if(privilegeListDTO.getParentId()==0){
               List<PrivilegeListDTO> childs =  privilegeListDTO.getChilds();
                for (PrivilegeListDTO child : childs) {
                    if(pvlIdList.contains(child.getPvlId())){
                        child.setChecked("checked");
                    }
                }
            }
        }

        result.setData(list);

        result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
        return result;
    }

    public ResponseResult<Collection<PrivilegeListDTO>> findAllPvlList() throws BusinessException {
        ResponseResult result = new ResponseResult();

        SysPrivilege entity = new SysPrivilege();
        //entity.setParentId(0L);
        Example example = new Example(SysPrivilege.class);
        //example.createCriteria().andEqualTo("refId", rowNews.getRefId());
        example.setOrderByClause("parent_id asc");

        List<SysPrivilege> list = sysPrivilegeMapper.selectByExample(example);
        List<PrivilegeListDTO> pvlList = new ArrayList<>();
        Map<Long, PrivilegeListDTO> pvlMap = new LinkedHashMap();
        for (int i = 0; i < list.size(); i++) {
            SysPrivilege sysPrivilege = list.get(i);
            if (sysPrivilege.getParentId() == 0) {
                PrivilegeListDTO privilegeListDTO = new PrivilegeListDTO();
                BeanUtils.copyProperties(privilegeListDTO, sysPrivilege);
                List<PrivilegeListDTO> childs = new ArrayList();
                privilegeListDTO.setChilds(childs);
                pvlMap.put(privilegeListDTO.getPvlId(), privilegeListDTO);
            } else {
                PrivilegeListDTO privilegeListDTO = pvlMap.get(sysPrivilege.getParentId());
                List<PrivilegeListDTO> childs = privilegeListDTO.getChilds();
                PrivilegeListDTO childPvlDTO = new PrivilegeListDTO();
                BeanUtils.copyProperties(childPvlDTO, sysPrivilege);
                childs.add(childPvlDTO);
                privilegeListDTO.setChilds(childs);
            }

            //entity = new SysPrivilege();
            //sysPrivilegeMapper.select(entity);
        }

        result.setData(pvlMap.values());
        result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
        return result;
    }

    public ResponseResult findByExample(SysPrivilegeDTO dto) throws BusinessException {
        ResponseResult result = new ResponseResult();

        SysPrivilege entity = new SysPrivilege();
        BeanUtils.copyProperties(entity, dto);

        List list = sysPrivilegeMapper.select(entity);

        result.setData(list);
        result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
        return result;
    }


    public ResponseResult getByExample(SysPrivilegeDTO dto) throws BusinessException {
        ResponseResult result = new ResponseResult();

        SysPrivilege entity = new SysPrivilege();
        BeanUtils.copyProperties(entity, dto);

        entity = sysPrivilegeMapper.selectOne(entity);

        result.setData(entity);
        result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
        return result;

    }

    public void setSysPrivilegeMapper(SysPrivilegeMapper dao) {
        this.sysPrivilegeMapper = dao;
    }
}
