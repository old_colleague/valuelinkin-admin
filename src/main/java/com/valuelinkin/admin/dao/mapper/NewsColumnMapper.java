package com.valuelinkin.admin.dao.mapper;


import org.apache.ibatis.annotations.Mapper;

import com.valuelinkin.admin.dao.entity.NewsColumn;
import com.valuelinkin.admin.dao.BaseMapper;

@Mapper
public interface NewsColumnMapper extends BaseMapper<NewsColumn>{
	

}
