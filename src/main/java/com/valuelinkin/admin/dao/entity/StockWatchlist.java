package com.valuelinkin.admin.dao.entity;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 〈功能详细描述〉
 *
 * @author Nail.zhang
 * @version [V0.1, Jan 9, 2018]
 */

@Table(name="vln_stock_watchlist")
public class StockWatchlist implements java.io.Serializable{
	private static final long serialVersionUID = 5454155825314635342L;
	

	//columns START
		@Id
		@Column(name = "lst")
		private Long lst;
		private Long usrId;
		private java.lang.String stkCode;
		private Integer sortIndex;
		private java.util.Date createdOn;
		private java.util.Date modifiedOn;
	//columns END


	public StockWatchlist(){
	}

	public StockWatchlist(
		Long lst
	){
		this.lst = lst;
	}

	

	public void setLst(Long value) {
		this.lst = value;
	}
	
	public Long getLst() {
		return this.lst;
	}
	
	public Long getUsrId() {
		return this.usrId;
	}
	
	public void setUsrId(Long value) {
		this.usrId = value;
	}
	
	public java.lang.String getStkCode() {
		return this.stkCode;
	}
	
	public void setStkCode(java.lang.String value) {
		this.stkCode = value;
	}
	
	public Integer getSortIndex() {
		return this.sortIndex;
	}
	
	public void setSortIndex(Integer value) {
		this.sortIndex = value;
	}
	
		
	public java.util.Date getCreatedOn() {
		return this.createdOn;
	}
	
	public void setCreatedOn(java.util.Date value) {
		this.createdOn = value;
	}
	
		
	public java.util.Date getModifiedOn() {
		return this.modifiedOn;
	}
	
	public void setModifiedOn(java.util.Date value) {
		this.modifiedOn = value;
	}
	

}

