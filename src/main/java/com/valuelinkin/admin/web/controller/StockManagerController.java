package com.valuelinkin.admin.web.controller;

import java.util.List;

import com.jcommon.orm.Page;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.ui.Model;

import com.valuelinkin.admin.dto.StockManagerDTO;
import com.valuelinkin.admin.service.core.StockManagerService;

import com.jcommon.result.ResponseResult;
import com.jcommon.utils.BeanUtils;

@Controller
@RequestMapping(value="/stockManager") 
public class StockManagerController{
	
	@Autowired
	private StockManagerService stockManagerService;
	
	@RequestMapping(value="/index/{stkCode}",method=RequestMethod.GET)
	public String index(@PathVariable String stkCode, Model model) throws Exception {
		
		ResponseResult result = new ResponseResult();
		StockManagerDTO stockManagerDTO = new StockManagerDTO();
		stockManagerDTO.setStkCode(stkCode);
		result = stockManagerService.findByExample(stockManagerDTO);
		model.addAttribute("result", result);
		model.addAttribute("stkCode", stkCode);
		
		return "/stockManager/list";
	}
	
	/** 
	 * 查看对象
	 **/
	@RequestMapping(value="/view/{id}",method=RequestMethod.GET)
	public String getById(@PathVariable Long id, Model model) {
		ResponseResult result = new ResponseResult();
		result = stockManagerService.getById(id);
		
		model.addAttribute("result", result);
		return "/stockManager/view";
		
	}
	

	/** 
	 * 进入新增页面
	 **/
	@RequestMapping(value="/create", method=RequestMethod.GET)
	public String create(StockManagerDTO dto, Model model) throws Exception {
		model.addAttribute("stockManager", dto);
		return "/stockManager/create";
	}
	
	/** 
	 * 保存新增对象
	 **/
	@RequestMapping(value="",method=RequestMethod.POST)
	@ResponseBody
	public ResponseResult save(StockManagerDTO dto) {
		ResponseResult result = null;
		result = stockManagerService.save(dto);
		return result;
	}
	
	
	/**
	 * 进入更新页面
	 **/
	@RequestMapping(value="/edit/{id}", method=RequestMethod.GET)
	public String edit(@PathVariable Long id, Model model) throws Exception {

		ResponseResult result  = stockManagerService.getById(id);
		model.addAttribute("result", result);
		
		return "/stockManager/edit";
	}
	
	/**
	 * 保存更新对象
	 **/
	@RequestMapping(value="/{id}", method=RequestMethod.PUT)
	@ResponseBody
	public ResponseResult update(@PathVariable Long id,StockManagerDTO dto) throws Exception {
		ResponseResult result = new ResponseResult();
		dto.setId(id);
		result = stockManagerService.update(dto);
		return result;
	}
	
	
	/**
	 *删除多个对象
	 **/
	@RequestMapping(value="/{ids}", method=RequestMethod.DELETE)
	@ResponseBody
	public ResponseResult delete(@PathVariable String ids) {
		
		ResponseResult result = null;
		result = stockManagerService.deleteByIds(ids);
		return result;
		
	}
	
	public void setStockManagerService(StockManagerService service) {
		this.stockManagerService = service;
	}
	
}
