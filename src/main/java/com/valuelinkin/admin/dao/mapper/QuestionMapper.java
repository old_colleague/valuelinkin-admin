package com.valuelinkin.admin.dao.mapper;


import com.valuelinkin.admin.dto.QuestionDTO;
import com.valuelinkin.admin.dto.QuestionResultDTO;
import org.apache.ibatis.annotations.Mapper;

import com.valuelinkin.admin.dao.entity.Question;
import com.valuelinkin.admin.dao.BaseMapper;

import java.util.List;

@Mapper
public interface QuestionMapper extends BaseMapper<Question>{

    List<QuestionResultDTO> findPage(QuestionDTO dto);
	

}
