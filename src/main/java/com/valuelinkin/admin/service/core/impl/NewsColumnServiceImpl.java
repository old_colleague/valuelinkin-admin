package com.valuelinkin.admin.service.core.impl;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jcommon.exception.BusinessException;
import com.jcommon.orm.Page;
import com.jcommon.result.ResponseResult;
import com.jcommon.utils.BeanUtils;
import com.valuelinkin.admin.common.utils.AppConstants;
import com.valuelinkin.admin.dao.entity.NewsColumn;
import com.valuelinkin.admin.dao.mapper.NewsColumnMapper;
import com.valuelinkin.admin.dto.NewsColumnDTO;
import com.valuelinkin.admin.service.UidGenerator;
import com.valuelinkin.admin.service.core.NewsColumnService;

import tk.mybatis.mapper.entity.Example;

@Service
public class NewsColumnServiceImpl implements NewsColumnService{

	@Autowired
	private NewsColumnMapper newsColumnMapper;
	
	@Autowired
	private UidGenerator uidGenerator;

	public ResponseResult findPage(NewsColumnDTO dto) {
		ResponseResult result = new ResponseResult();
		
		NewsColumn entity = new NewsColumn();
		BeanUtils.copyProperties(entity, dto);
		
		Page page = new Page();
		page.setPageNo(dto.getPageNo());
		page.setPageSize(dto.getPageSize());
		page.setTotalCount(newsColumnMapper.selectCount(entity));
		
		Example example = new Example(NewsColumn.class);
		//example.createCriteria().andEqualTo("refId", rowNews.getRefId());
		//example.setOrderByClause("nws_Publish_Date desc");
		
		List<NewsColumn> list = newsColumnMapper.selectByExampleAndRowBounds(example, new RowBounds(page.getFirst(), page.getPageSize()));
		
		page.setResult(list);
		result.setData(page);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
		
	}	
	
	public ResponseResult getById(Long id) throws BusinessException{
		ResponseResult result = new ResponseResult();
		NewsColumn entity = newsColumnMapper.selectByPrimaryKey(id);
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}

	
	/** 插入数据 */
	@Transactional
	public ResponseResult save(NewsColumnDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		NewsColumn entity = new NewsColumn();
		BeanUtils.copyProperties(entity, dto);
		entity.setClmId(uidGenerator.nextId());
		Date now = new Date();
		entity.setCreatedOn(now);
		entity.setModifiedOn(now);
		newsColumnMapper.insertSelective(entity);
		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult deleteById(Long id) throws BusinessException{
		ResponseResult result = new ResponseResult();
		newsColumnMapper.deleteByPrimaryKey(id);
		
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult delete(NewsColumnDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		
		NewsColumn entity = new NewsColumn();
		BeanUtils.copyProperties(entity, dto);
		
		newsColumnMapper.delete(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult deleteByIds(String ids) throws BusinessException{
		ResponseResult result = new ResponseResult();
		String[] items = ids.split(",");
		for (String str : items) {
			Long id = new Long(str);
			newsColumnMapper.deleteByPrimaryKey(id);
		}
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult update(NewsColumnDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		NewsColumn entity = new NewsColumn();
		BeanUtils.copyProperties(entity, dto);
		newsColumnMapper.updateByPrimaryKeySelective(entity);
		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	public ResponseResult findByExample(NewsColumnDTO dto) throws BusinessException {
		ResponseResult result = new ResponseResult();
		
		NewsColumn entity = new NewsColumn();
		BeanUtils.copyProperties(entity, dto);
		
		List list =newsColumnMapper.select(entity);
		
		result.setData(list);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	
	public ResponseResult getByExample(NewsColumnDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		
		NewsColumn entity = new NewsColumn();
		BeanUtils.copyProperties(entity, dto);
		
		entity =newsColumnMapper.selectOne(entity);
		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
		
	}
	
	@Override
	public ResponseResult findChildColumnByCode(String clmGroup) throws BusinessException {
		ResponseResult result = new ResponseResult();
		
		Example example = new Example(NewsColumn.class);
		example.createCriteria().andEqualTo("clmGroup", clmGroup).andNotEqualTo("parentId", 0);
		
		List list =newsColumnMapper.selectByExample(example);
		
		result.setData(list);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	public void setNewsColumnMapper(NewsColumnMapper dao) {
		this.newsColumnMapper = dao;
	}

	/**
	 * @param clmCode
	 * @return
	 * @throws BusinessException
	 */
	
}
