package com.valuelinkin.admin.dao.mapper;


import org.apache.ibatis.annotations.Mapper;

import com.valuelinkin.admin.dao.entity.Security;
import com.valuelinkin.admin.dao.BaseMapper;

@Mapper
public interface SecurityMapper extends BaseMapper<Security>{
	

}
