package com.valuelinkin.admin.dto;

import com.valuelinkin.admin.dto.BaseDTO;

public class RelativeStockDTO extends BaseDTO {
    private static final long serialVersionUID = 3148176768559230877L;
    

	private Long lnkId;
	private Long relateId;
	private java.lang.String stkCode;

	
	public Long getLnkId() {
		return this.lnkId;
	}
	
	public void setLnkId(Long value) {
		this.lnkId = value;
	}
	
	public Long getRelateId() {
		return this.relateId;
	}
	
	public void setRelateId(Long value) {
		this.relateId = value;
	}
	
	public java.lang.String getStkCode() {
		return this.stkCode;
	}
	
	public void setStkCode(java.lang.String value) {
		this.stkCode = value;
	}
	
}

