package com.valuelinkin.admin.dao.entity;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 〈功能详细描述〉
 *
 * @author Nail.zhang
 * @version [V0.1, Jan 9, 2018]
 */

@Table(name="vln_raw_news")
public class RawNews implements java.io.Serializable{
	private static final long serialVersionUID = 5454155825314635342L;
	

	//columns START
		@Id
		@Column(name = "nws_id")
		private Long nwsId;
		private java.lang.String refId;
		private java.lang.String nwsTitle;
		private java.lang.String nwsBrief;
		private java.lang.String nwsContent;
		private java.lang.String nwsSource;
		private java.util.Date nwsPublishDate;
		private java.util.Date nwsCreateDate;
		private java.lang.String nwsUrl;
		private Integer nwsStatus;
		private java.lang.String nwsType;
	//columns END


	public RawNews(){
	}

	public RawNews(
		Long nwsId
	){
		this.nwsId = nwsId;
	}

	

	public void setNwsId(Long value) {
		this.nwsId = value;
	}
	
	public Long getNwsId() {
		return this.nwsId;
	}
	
	public java.lang.String getRefId() {
		return this.refId;
	}
	
	public void setRefId(java.lang.String value) {
		this.refId = value;
	}
	
	public java.lang.String getNwsTitle() {
		return this.nwsTitle;
	}
	
	public void setNwsTitle(java.lang.String value) {
		this.nwsTitle = value;
	}
	
	public java.lang.String getNwsBrief() {
		return this.nwsBrief;
	}
	
	public void setNwsBrief(java.lang.String value) {
		this.nwsBrief = value;
	}
	
	public java.lang.String getNwsContent() {
		return this.nwsContent;
	}
	
	public void setNwsContent(java.lang.String value) {
		this.nwsContent = value;
	}
	
	public java.lang.String getNwsSource() {
		return this.nwsSource;
	}
	
	public void setNwsSource(java.lang.String value) {
		this.nwsSource = value;
	}
	
		
	public java.util.Date getNwsPublishDate() {
		return this.nwsPublishDate;
	}
	
	public void setNwsPublishDate(java.util.Date value) {
		this.nwsPublishDate = value;
	}
	
		
	public java.util.Date getNwsCreateDate() {
		return this.nwsCreateDate;
	}
	
	public void setNwsCreateDate(java.util.Date value) {
		this.nwsCreateDate = value;
	}
	
	public java.lang.String getNwsUrl() {
		return this.nwsUrl;
	}
	
	public void setNwsUrl(java.lang.String value) {
		this.nwsUrl = value;
	}
	
	public Integer getNwsStatus() {
		return this.nwsStatus;
	}
	
	public void setNwsStatus(Integer value) {
		this.nwsStatus = value;
	}
	
	public java.lang.String getNwsType() {
		return this.nwsType;
	}
	
	public void setNwsType(java.lang.String value) {
		this.nwsType = value;
	}
	

}

