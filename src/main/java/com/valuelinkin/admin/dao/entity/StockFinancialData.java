package com.valuelinkin.admin.dao.entity;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 〈功能详细描述〉
 *
 * @author Nail.zhang
 * @version [V0.1, Jan 9, 2018]
 */

@Table(name="vln_stock_financial_data")
public class StockFinancialData implements java.io.Serializable{
	private static final long serialVersionUID = 5454155825314635342L;
	

	//columns START
		@Id
		@Column(name = "sfd_id")
		private Long sfdId;
		private java.lang.String stkCode;
		private java.lang.String sdfDataTitle;
		private java.util.Date createdOn;
		private java.util.Date modifiedOn;
	//columns END


	public StockFinancialData(){
	}

	public StockFinancialData(
		Long sfdId
	){
		this.sfdId = sfdId;
	}

	

	public void setSfdId(Long value) {
		this.sfdId = value;
	}
	
	public Long getSfdId() {
		return this.sfdId;
	}
	
	public java.lang.String getStkCode() {
		return this.stkCode;
	}
	
	public void setStkCode(java.lang.String value) {
		this.stkCode = value;
	}
	
	public java.lang.String getSdfDataTitle() {
		return this.sdfDataTitle;
	}
	
	public void setSdfDataTitle(java.lang.String value) {
		this.sdfDataTitle = value;
	}
	
		
	public java.util.Date getCreatedOn() {
		return this.createdOn;
	}
	
	public void setCreatedOn(java.util.Date value) {
		this.createdOn = value;
	}
	
		
	public java.util.Date getModifiedOn() {
		return this.modifiedOn;
	}
	
	public void setModifiedOn(java.util.Date value) {
		this.modifiedOn = value;
	}
	

}

