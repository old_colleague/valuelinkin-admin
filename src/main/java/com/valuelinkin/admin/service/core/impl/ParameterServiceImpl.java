package com.valuelinkin.admin.service.core.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jcommon.orm.Page;
import com.jcommon.exception.BusinessException;
import com.jcommon.utils.BeanUtils;
import com.jcommon.result.ResponseResult;

import com.valuelinkin.admin.dao.entity.Parameter;
import com.valuelinkin.admin.dto.ParameterDTO;
import com.valuelinkin.admin.dao.mapper.ParameterMapper;
import com.valuelinkin.admin.service.core.ParameterService;
import com.valuelinkin.admin.service.UidGenerator;
import com.valuelinkin.admin.common.utils.AppConstants;

import tk.mybatis.mapper.entity.Example;

@Service
public class ParameterServiceImpl implements ParameterService{

	@Autowired
	private ParameterMapper parameterMapper;
	
	@Autowired
	private UidGenerator uidGenerator;

	public ResponseResult findPage(ParameterDTO dto) {
		ResponseResult result = new ResponseResult();
		
		Parameter entity = new Parameter();
		BeanUtils.copyProperties(entity, dto);
		
		Page page = new Page();
		page.setPageNo(dto.getPageNo());
		page.setPageSize(dto.getPageSize());
		page.setTotalCount(parameterMapper.selectCount(entity));
		
		Example example = new Example(Parameter.class);
		//example.createCriteria().andEqualTo("refId", rowNews.getRefId());
		//example.setOrderByClause("nws_Publish_Date desc");
		
		List<Parameter> list = parameterMapper.selectByExampleAndRowBounds(example, new RowBounds(page.getFirst(), page.getPageSize()));
		
		page.setResult(list);
		result.setData(page);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
		
	}	
	
	public ResponseResult getById(Long id) throws BusinessException{
		ResponseResult result = new ResponseResult();
		Parameter entity = parameterMapper.selectByPrimaryKey(id);
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}

	
	/** 插入数据 */
	@Transactional
	public ResponseResult save(ParameterDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		Parameter entity = new Parameter();
		BeanUtils.copyProperties(entity, dto);
		entity.setPrmId(uidGenerator.nextId());
		
		parameterMapper.insertSelective(entity);
		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult deleteById(Long id) throws BusinessException{
		ResponseResult result = new ResponseResult();
		parameterMapper.deleteByPrimaryKey(id);
		
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult delete(ParameterDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		
		Parameter entity = new Parameter();
		BeanUtils.copyProperties(entity, dto);
		
		parameterMapper.delete(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult deleteByIds(String ids) throws BusinessException{
		ResponseResult result = new ResponseResult();
		String[] items = ids.split(",");
		for (String str : items) {
			Long id = new Long(str);
			parameterMapper.deleteByPrimaryKey(id);
		}
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	@Transactional
	public ResponseResult update(ParameterDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		Parameter entity = new Parameter();
		BeanUtils.copyProperties(entity, dto);
		parameterMapper.updateByPrimaryKeySelective(entity);
		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	public ResponseResult findByExample(ParameterDTO dto) throws BusinessException {
		ResponseResult result = new ResponseResult();
		
		Parameter entity = new Parameter();
		BeanUtils.copyProperties(entity, dto);
		
		List list =parameterMapper.select(entity);
		
		result.setData(list);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
	}
	
	
	public ResponseResult getByExample(ParameterDTO dto) throws BusinessException{
		ResponseResult result = new ResponseResult();
		
		Parameter entity = new Parameter();
		BeanUtils.copyProperties(entity, dto);
		
		entity =parameterMapper.selectOne(entity);
		
		result.setData(entity);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
		
	}
	
	public void setParameterMapper(ParameterMapper dao) {
		this.parameterMapper = dao;
	}
}
