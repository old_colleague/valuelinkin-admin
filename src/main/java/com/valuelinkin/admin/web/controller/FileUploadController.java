package com.valuelinkin.admin.web.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.jcommon.result.ResponseResult;
import com.valuelinkin.admin.common.utils.AppConstants;
import com.valuelinkin.admin.common.utils.QiNiuFileManager;

@RestController
@RequestMapping(value = "/file")
public class FileUploadController {
	@Value("${static.file.domain}")
	private String staticDomain;

	@Value("${video.file.domain}")
	private String videoDomain;
	
	/**
	 * 图片上传
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/img/upload", method = RequestMethod.POST,produces="application/json;charset=UTF-8")
	public ResponseResult imgFileUpload(@RequestParam(value = "file", required = true) MultipartFile file) throws Exception {
		ResponseResult result = new ResponseResult();
		// 最大文件大小
		long maxSize = 1000000;
		//获得上传的文件名：   
		String fileName = "images/"+file.getName();   
		
		String newImgName = QiNiuFileManager.fileUplaod(file.getBytes(),null,"image");
		String fileUrl = staticDomain +"/"+ newImgName;
		result.setData(fileUrl);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
		
	}
	
	@RequestMapping(value = "/img/upload/keditor", method = RequestMethod.POST,produces="application/json;charset=UTF-8")
	public Map imgFileUploadForKingEditor(@RequestParam(value = "file", required = true) MultipartFile file) throws Exception {
		Map result = new HashMap();
		// 最大文件大小
		long maxSize = 1000000;
		//获得上传的文件名：   
		String fileName = "images/"+file.getName();   
		
		String newImgName = QiNiuFileManager.fileUplaod(file.getBytes(),null,"image");
		String fileUrl = staticDomain +"/"+ newImgName;
		result.put("error", 0);
		result.put("url", fileUrl);
		return result;
		
	}


	@RequestMapping(value = "/video/upload", method = RequestMethod.POST,produces="application/json;charset=UTF-8")
	public ResponseResult videoFileUpload(@RequestParam(value = "videoFile", required = true) MultipartFile file) throws Exception {
		ResponseResult result = new ResponseResult();

		String newImgName = QiNiuFileManager.videoUpdateTransform(file.getBytes(),null,"video");
		String fileUrl = videoDomain +"/"+ newImgName;
		result.setData(fileUrl);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;

	}
	
	
	@RequestMapping(value = "/html/upload", method = RequestMethod.POST)
	public ResponseResult fileUpload(@RequestParam(value = "file", required = true) MultipartFile file) throws Exception {
		ResponseResult result = new ResponseResult();

		// 最大文件大小
		long maxSize = 1000000;
		//获得上传的文件名：   
		String fileName = "html/"+file.getName();   
		
		
		String newImgName = QiNiuFileManager.fileUplaod(file.getBytes(),fileName,"image");
		String fileUrl = staticDomain +"/"+ newImgName;
		result.setData(fileUrl);
		result.setErrorCode(AppConstants.RETURN_SUCCESS_CODE);
		return result;
		
	}

	public String getStaticDomain() {
		return staticDomain;
	}

	public void setStaticDomain(String staticDomain) {
		this.staticDomain = staticDomain;
	}


}
