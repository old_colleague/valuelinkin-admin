package com.valuelinkin.admin.dto;

import com.valuelinkin.admin.dto.BaseDTO;

import java.util.Date;

public class OpinionDTO extends BaseDTO {
    private static final long serialVersionUID = 3148176768559230877L;


    private Long opnId;
    private Long clmId;
    private Long usrId;
    private java.lang.String opnTitle;
    private java.lang.String opnBrief;
    private java.lang.String opnContent;
    private java.util.Date opnPublishDate;
    private Integer opnStatus;
    private java.lang.String opnStockCode;
    private java.lang.String opnViewCount;
    private Integer opnStickTop;
    private java.lang.String opnColor;
    private java.lang.String opnThumbnailUrl;
    private java.lang.String opnVideoUrl;
    private java.util.Date createdOn;
    private java.util.Date modifiedOn;

    private Integer opnType;
    private String keywords;

    private Date startDate;
    private Date endDate;

    private String usrNickName;

    private String clmCode;

    public String getClmCode() {
        return clmCode;
    }

    public void setClmCode(String clmCode) {
        this.clmCode = clmCode;
    }

    public String getUsrNickName() {
        return usrNickName;
    }

    public void setUsrNickName(String usrNickName) {
        this.usrNickName = usrNickName;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public Integer getOpnType() {
        return opnType;
    }

    public void setOpnType(Integer opnType) {
        this.opnType = opnType;
    }

    public Long getOpnId() {
        return this.opnId;
    }

    public void setOpnId(Long value) {
        this.opnId = value;
    }

    public Long getClmId() {
        return this.clmId;
    }

    public void setClmId(Long value) {
        this.clmId = value;
    }

    public Long getUsrId() {
        return this.usrId;
    }

    public void setUsrId(Long value) {
        this.usrId = value;
    }

    public java.lang.String getOpnTitle() {
        return this.opnTitle;
    }

    public void setOpnTitle(java.lang.String value) {
        this.opnTitle = value;
    }

    public java.lang.String getOpnBrief() {
        return this.opnBrief;
    }

    public void setOpnBrief(java.lang.String value) {
        this.opnBrief = value;
    }

    public java.lang.String getOpnContent() {
        return this.opnContent;
    }

    public void setOpnContent(java.lang.String value) {
        this.opnContent = value;
    }

    public java.util.Date getOpnPublishDate() {
        return this.opnPublishDate;
    }

    public void setOpnPublishDate(java.util.Date value) {
        this.opnPublishDate = value;
    }

    public Integer getOpnStatus() {
        return this.opnStatus;
    }

    public void setOpnStatus(Integer value) {
        this.opnStatus = value;
    }

    public java.lang.String getOpnStockCode() {
        return this.opnStockCode;
    }

    public void setOpnStockCode(java.lang.String value) {
        this.opnStockCode = value;
    }

    public java.lang.String getOpnViewCount() {
        return this.opnViewCount;
    }

    public void setOpnViewCount(java.lang.String value) {
        this.opnViewCount = value;
    }

    public Integer getOpnStickTop() {
        return this.opnStickTop;
    }

    public void setOpnStickTop(Integer value) {
        this.opnStickTop = value;
    }

    public java.lang.String getOpnColor() {
        return this.opnColor;
    }

    public void setOpnColor(java.lang.String value) {
        this.opnColor = value;
    }

    public java.lang.String getOpnThumbnailUrl() {
        return this.opnThumbnailUrl;
    }

    public void setOpnThumbnailUrl(java.lang.String value) {
        this.opnThumbnailUrl = value;
    }

    public java.lang.String getOpnVideoUrl() {
        return this.opnVideoUrl;
    }

    public void setOpnVideoUrl(java.lang.String value) {
        this.opnVideoUrl = value;
    }

    public java.util.Date getCreatedOn() {
        return this.createdOn;
    }

    public void setCreatedOn(java.util.Date value) {
        this.createdOn = value;
    }

    public java.util.Date getModifiedOn() {
        return this.modifiedOn;
    }

    public void setModifiedOn(java.util.Date value) {
        this.modifiedOn = value;
    }

}

